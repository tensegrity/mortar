/* This file has to be included AFTER the LoginPanel */
if (org.sample.LoginPanel) {
	Ext.apply(org.sample.LoginPanel.prototype, {
		titleText : 'Acceso',
		userFieldText : 'Usuario',
		passwordFieldText : 'Contraseña',
		waitMessageText : 'Espere un momento...',
		waitTitle : 'Autenticando...',
		loginButtonText : 'Aceptar',
		badLoginText : 'Error al tratar de autenticar al usuario',
		unknownErrorText : 'Ocurrio un error inesperado al tratar de autenticar las credenciales',
		errorTitleText : 'Mensaje del Sistema'
	});
}