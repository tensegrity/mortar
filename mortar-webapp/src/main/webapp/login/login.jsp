<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr"%>

<html>
<head>

<link rel="shortcut icon" href="<c:url value="/favicon.ico"/>" type="image/vnd.microsoft.icon"></link>

<jwr:style src="/lib/all.css" useRandomParam="false" />
<link rel="stylesheet" type="text/css" media="screen" href="login.css" />

<script type="text/javascript" charset="utf-8" src="<c:url value="/dwr/engine.js"/>"></script>
<jwr:script src="/lib/all.js" useRandomParam="false" />

<!-- ExJS requires that we set the constant Ext.BLANK_IMAGE_URL -->
<script type="text/javascript">
	Ext.BLANK_IMAGE_URL = '<jwr:imagePath src="/jar:ext/resources/images/default/s.gif"/>';
</script>

<script type="text/javascript" src="LoginPanel.js" charset="UTF-8"></script>
<script type="text/javascript" src="LoginWindow.js" charset="UTF-8"></script>
<script type="text/javascript" src="login_es.js" charset="UTF-8"></script>

<script type="text/javascript">
	/* Ext.onReady(Mortar.app.Login.init, Mortar.app.Login); */
	Ext.onReady(function() {
		var loginWindow = new org.sample.LoginWindow({
			title : 'Acceso al sistema de administración'
		});
		loginWindow.show();
	});
</script>

</head>

<body>
	<div id="loading-mask"></div>
	<div id="loading">
		<div class="loading-indicator">
			<jwr:img src="/jar:ext/resources/images/default/grid/loading.gif" />
			Cargando...
		</div>
		<%-- style="margin-right:8px; margin-top: 8px;" align="absmiddle" --%>
	</div>

	<div id='loginDialogId'></div>
</body>
</html>