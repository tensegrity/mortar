/*jslint browser:true */
/*global Ext, Mortar*/
Ext.ns('org.sample');
org.sample.LoginWindow = Ext.extend(Ext.Window, {
	buttonAlign : 'right',
	closable : false,
	draggable : false,
	plain : false,
	resizable : false,

	initComponent : function () {
		var me = this, loginPanel, successHandler;

		loginPanel = new org.sample.LoginPanel({
			xtype : 'org.sample.LoginPanel',
			header : false,
			border : false,
			itemId : 'contentPanel'
		});

		successHandler = function (loginPanel) {
			var mask = Ext.get('loginDialogId');
			me.destroy();

			mask.fadeOut({
				remove : true,
				duration : 0.5,
				callback : function () {
					window.location = Mortar.app.props.CONTEXT_ROOT;
				}
			});
		};

		loginPanel.on('success', successHandler);

		this.title = this.title || org.sample.LoginPanel.prototype.titleText;

		this.defaultButton = loginPanel.find('name', 'j_username')[0];

		this.items = [ loginPanel ];

		org.sample.LoginWindow.superclass.initComponent.call(this);
	}

});

Ext.reg('org.sample.LoginWindow', org.sample.LoginWindow);