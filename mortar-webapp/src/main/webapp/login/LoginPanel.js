/*global Ext, Mortar*/
Ext.ns('org.sample');

org.sample.LoginPanel = Ext.extend(Ext.Panel, {
	titleText : 'Login',
	userFieldText : 'Username',
	passwordFieldText : 'Password',
	waitMessageText : 'Please wait...',
	waitTitle : 'Authenticating...',
	loginButtonText : 'Login',
	badLoginText : 'Could not authenticate user',
	unknownErrorText : 'Unexpected error while authenticating user',
	errorTitleText : 'System Error',
	width : 520,
	minWidth : 520,
	height : 220,
	minHeight : 220,
	layout : 'border',
	frame : false,

	initComponent : function () {
		var me = this, doSubmitFormLogin, formPanel, loginButton;

		doSubmitFormLogin = function () {
			formPanel.getForm().submit({
				waitMsg : me.waitMessageText,
				waitTitle : me.waitTitle,
				reset : true,
				success : me.success,
				failure : me.failure,
				scope : me,
				headers : {
					"Accept" : "application/json"
				}
			});
		};

		formPanel = new Ext.form.FormPanel({
			baseCls : 'x-plain',
			monitorValid : true,
			baseParams : {
				'mortar-request-type' : 'ajax-json'
			},
			defaults : {
				width : 200,
				msgTarget : 'side',
				xtype : 'textfield',
				allowBlank : false,
				listeners : {
					specialkey : function (field, event) {
						if (event.keyCode === 13) {
							doSubmitFormLogin();
						}
					}
				}
			},
			items : [ {
				fieldLabel : this.userFieldText,
				itemId : 'username',
				name : 'j_username'
			}, {
				fieldLabel : this.passwordFieldText,
				inputType : 'password',
				name : 'j_password'
			}
			/*
			 * , { xtype : 'hidden', name : '_spring_security_remember_me',
			 * value : 1 }
			 */
			],
			frame : false,
			height : 70,
			cls : 'login-form',
			labelWidth : 120,
			region : 'south',
			url : Mortar.app.props.CONTEXT_ROOT + '/j_spring_security_check'
		});

		this.title = this.titleText;
		this.items = [ {
			xtype : 'panel',
			baseCls : 'x-plain',
			cls : 'login-logo',
			region : 'center'
		}, formPanel ];
		this.buttons = [ {
			text : this.loginButtonText,
			itemId : 'loginButton',
			disabled : true,
			handler : function () {
				doSubmitFormLogin();
			}
		} ];

		org.sample.LoginPanel.superclass.initComponent.call(this);

		this.addEvents(
		/**
		 * @event success Fires after user has been authenticated
		 */
		'success'

		);

		loginButton = me.fbar.find('itemId', 'loginButton')[0];
		formPanel.on('clientvalidation', function (formPanel, valid) {
			loginButton.setDisabled(!valid);
		});

	},

	success : function (f, a) {
		if (a && a.result) {
			this.fireEvent('success', this);
		}
	},

	/**
	 * 
	 */
	failure : function (form, action) {
		if (action.type === 'submit') {
			var obj, msg;
			obj = Ext.util.JSON.decode(action.response.responseText);
			msg = obj.message || '';
			Ext.MessageBox.show({
				title : this.errorTitleText,
				msg : this.badLoginText + ':<br><b>' + msg + '</b>',
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.ERROR
			});
		} else {
			Ext.MessageBox.show({
				title : this.errorTitleText,
				msg : this.unknownErrorText,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.ERROR
			});
		}
	}
});

Ext.reg('org.sample.LoginPanel', org.sample.LoginPanel);
