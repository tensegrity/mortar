package org.sample.dao;

import org.mortar.persistence.hibernate.EntityHibernateDAO;
import org.springframework.stereotype.Repository;

/**
 * Define your own application's base dao.  If you have standardized to a single type of
 * primary key, for example <code>Long</code>, you can pre-define it here.
 * 
 * @author krause
 *
 * @param <T>
 */
@Repository
public abstract class BaseDao<T> extends EntityHibernateDAO<T, Long> {

}
