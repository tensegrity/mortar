package org.sample.dao;

import org.mortar.persistence.EntityDAO;
import org.sample.domain.Item;


public interface ItemDao extends EntityDAO<Item, Long> {

}
