package org.sample.dao;

import org.sample.domain.Item;
import org.springframework.stereotype.Repository;


@Repository
public class ItemDaoImpl extends BaseDao<Item> implements ItemDao {

}
