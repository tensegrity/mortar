package org.sample.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Bid {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private BigDecimal amount;

	@ManyToOne
	private Item item;

	// Accessors

	public long getId() {
		return id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Item getItem() {
		return item;
	}

	// Mutators

	public void setId(long id) {
		this.id = id;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
