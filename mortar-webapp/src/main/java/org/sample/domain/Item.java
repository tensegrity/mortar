package org.sample.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Item {

	// Fields

	@Id
	@GeneratedValue
	private long id;
	private String description;
	private String name;

	@OneToMany(mappedBy = "item", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Set<Bid> bids = new HashSet<Bid>();

	// Helper methods

	/**
	 * Set both sides of the relationship.
	 */
	public void addBid(Bid bid) {
		bid.setItem(this);
		bids.add(bid);
	}

	// Accessors

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Set<Bid> getBids() {
		return bids;
	}

	// Mutators

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBids(Set<Bid> bids) {
		this.bids = bids;
	}

}
