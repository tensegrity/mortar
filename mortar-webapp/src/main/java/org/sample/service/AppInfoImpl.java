package org.sample.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.annotations.RemoteProxy;
import org.sample.webapp.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@RemoteProxy(name = "org.sample.service.AppInfo")
@Service
public class AppInfoImpl extends BaseService implements AppInfo {

	@Autowired
	@Qualifier("main")
	private MenuItem menu;

	@Value("#{appProperties['app.version']}")
	private String version;

	public MenuItem getMenu() {
		return filter(menu);
	}

	/**
	 * Filters menu to only show items authorized for current user.
	 * 
	 * @param item
	 * @return
	 */
	private MenuItem filter(MenuItem item) {
		MenuItem userItem = null;
		for (GrantedAuthority authority : getAuthentication().getAuthorities()) {
			if (null == item.getAuthorizedRoles() || ArrayUtils.contains(item.getAuthorizedRoles(), authority.getAuthority())) {
				userItem = new MenuItem();
				userItem.setLabel(item.getLabel());
				userItem.setValue(item.getValue());
				userItem.setIcon(item.getIcon());
				userItem.setAuthorizedRoles(item.getAuthorizedRoles());
				List subMenu = filterList(item.getSubMenu());
				userItem.setSubMenu(subMenu);
				break; // We already added the menu item, we don't care for any
						// additional roles (authorities)
			}
		}
		return userItem;
	}

	private List<MenuItem> filterList(List items) {
		List<MenuItem> result = new ArrayList<MenuItem>();
		if (null != items && items.size() > 0) {
			for (MenuItem item : (List<MenuItem>) items) {
				MenuItem userItem = filter(item);
				if (null != userItem) {
					result.add(userItem);
				}
			}
		}
		return result;
	}

	public Map getUserInfo() {
		Map user = new HashMap();
		UserDetails userDetails = (UserDetails) getAuthentication().getPrincipal();
		user.put("username", userDetails.getUsername());
		user.put("expired", !userDetails.isAccountNonExpired());
		user.put("locked", !userDetails.isAccountNonLocked());
		user.put("credentialsExpired", !userDetails.isCredentialsNonExpired());
		user.put("enabled", userDetails.isEnabled());
		List<String> authorities = new ArrayList<String>();
		user.put("authorities", authorities);
		if (null != userDetails.getAuthorities() && userDetails.getAuthorities().size() > 0) {
			for (GrantedAuthority authority : userDetails.getAuthorities()) {
				authorities.add(authority.getAuthority());
			}
		}
		return user;
	}

	public String getVersion() {
		return version;
	}

}
