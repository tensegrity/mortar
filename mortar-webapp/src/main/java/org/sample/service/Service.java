/**
 * 
 */
package org.sample.service;

import org.springframework.security.core.Authentication;

/**
 * 
 * @author Pablo Krause
 * 
 */
public interface Service extends org.mortar.service.ApplicationService {
	Authentication getAuthentication();
}