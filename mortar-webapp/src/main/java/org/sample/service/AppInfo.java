package org.sample.service;

import java.util.Map;

import org.sample.webapp.MenuItem;


public interface AppInfo extends Service {

	/**
	 * Gets menu for current user.
	 * 
	 * @return
	 */
	MenuItem getMenu();

	Map getUserInfo();

	String getVersion();
}
