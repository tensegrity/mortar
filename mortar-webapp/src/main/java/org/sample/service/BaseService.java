/**
 * 
 */
package org.sample.service;

import org.mortar.service.AbstractApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

/**
 * Single point of dependency with Mortar's code for application services. You
 * could even remove the dependency if needed.
 * 
 * @author Pablo Krause
 * 
 */
public abstract class BaseService extends AbstractApplicationService implements Service {
	@Autowired
	private Authentication authentication;

	public Authentication getAuthentication() {
		return authentication;
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}
	
}
