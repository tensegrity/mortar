package org.sample.service.sample;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.mortar.annotation.PopulateResponse;
import org.sample.dao.ItemDao;
import org.sample.domain.Item;
import org.sample.service.BaseService;
import org.sample.service.sample.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@RemoteProxy(name = "org.sample.service.sample.BidService")
public class BidServiceImpl extends BaseService implements BidService {

	@Autowired
	private ItemDao itemDao;

	@RemoteMethod
	@PopulateResponse("bids?.size()")
	// Force loading of lazy relation item->bids
	public Item getItem(long id) {
		Item item = itemDao.getById(id);
		return item;
	}

}
