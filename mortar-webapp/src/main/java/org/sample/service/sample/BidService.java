package org.sample.service.sample;

import org.sample.domain.Item;
import org.sample.service.Service;

/**
 * Sample service for showcasing several basic Mortar features.
 * @author pablo
 *
 */
public interface BidService extends Service {

	Item getItem(long id);

}
