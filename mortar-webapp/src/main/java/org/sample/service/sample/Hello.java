/**
 * 
 */
package org.sample.service.sample;

public interface Hello {

	/**
	 * Canonical Hello World.
	 * 
	 * @return a message to let you know the application is alive.
	 */
	String hello(String name);

	/**
	 * Change the default greeting text. Only users with role ROLE_ADMIN should
	 * be able to change the greeting text.
	 * 
	 * @param greeting
	 */
	void changeGreeting(String greeting);

}
