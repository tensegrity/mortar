/**
 * 
 */
package org.sample.service.sample;

import javax.annotation.security.RolesAllowed;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.sample.service.BaseService;
import org.springframework.stereotype.Service;

/**
 * @author Pablo Krause
 * 
 */
@Service
@RemoteProxy(name = "org.sample.service.sample.Hello")
public class HelloImpl extends BaseService implements Hello {
	private final static String DEFAULT_GREETING = "Hello ";
	private final static String UNKNOWN = "stranger";

	private String greeting = DEFAULT_GREETING;

	@RemoteMethod
	@Override
	public String hello(String name) {
		if (name.trim().isEmpty()) {
			log.debug("No name was provided");
			name = UNKNOWN;
		}
		return new StringBuilder(greeting).append(" ").append(name).toString();
	}

	@RolesAllowed("ROLE_ADMIN")
	@RemoteMethod
	@Override
	public void changeGreeting(String aGreeting) {
		this.greeting = aGreeting;
	}

}
