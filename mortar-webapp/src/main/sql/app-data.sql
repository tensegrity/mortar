DELETE FROM BID;
DELETE FROM ITEM;

INSERT INTO ITEM (id, name, description) VALUES (1, 'Breitling watch', 'Brand new golden Breitling watch');
INSERT INTO ITEM (id, name, description) VALUES (2, 'Patek Philippe watch', 'Used Patek Philippe watch in excellent condition');

INSERT INTO BID (item_id, amount) VALUES (1, 56000.00);
INSERT INTO BID (item_id, amount) VALUES (1, 58000.00);
