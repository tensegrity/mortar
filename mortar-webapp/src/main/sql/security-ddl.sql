DROP TABLE persistent_logins;
DROP TABLE group_members;
DROP TABLE group_authorities;
DROP TABLE groups;
DROP TABLE authorities;
DROP TABLE users;

CREATE TABLE users (
 username VARCHAR(100) NOT NULL PRIMARY KEY,
 password VARCHAR(100) NOT NULL,
 enabled BOOLEAN NOT NULL);

CREATE TABLE authorities (
	username VARCHAR(100) NOT NULL,
	authority VARCHAR(100) NOT NULL,
	CONSTRAINT fk_authorities_users FOREIGN KEY(username) REFERENCES users(username));
	
CREATE UNIQUE INDEX ix_auth_username ON authorities(username, authority);

CREATE TABLE groups (
	id BIGINT IDENTITY PRIMARY KEY,
	group_name varchar(100) NOT NULL);
	
CREATE TABLE group_authorities (
	group_id BIGINT NOT NULL,
	authority VARCHAR(100) NOT NULL,
	CONSTRAINT fk_group_authorities_group FOREIGN KEY(group_id) REFERENCES groups(id));

CREATE TABLE group_members (
	id BIGINT IDENTITY PRIMARY KEY,
	username varchar(100) NOT NULL,
	group_id BIGINT NOT NULL,
	CONSTRAINT fk_group_members_group FOREIGN KEY(group_id) REFERENCES groups(id));

/* Persistent Login (Remember-Me) */
CREATE TABLE persistent_logins (
	username VARCHAR(100) NOT NULL,
	series VARCHAR(100) PRIMARY KEY,
	token VARCHAR(100) NOT NULL,
	last_used TIMESTAMP NOT NULL,
	CONSTRAINT fk_persistent_logins_users FOREIGN KEY(username) REFERENCES users(username));
