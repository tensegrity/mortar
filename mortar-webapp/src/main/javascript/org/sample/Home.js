/*global Ext*/
Ext.ns("org.sample");

/**
 * Sample Home
 */
org.sample.Home = Ext.extend(Ext.Viewport, {
	layout : 'border',

	initComponent : function() {
		var usernameTbText, appInfoTbText;

		this.addEvents('logout');

		usernameTbText = new Ext.Toolbar.TextItem({});
		appInfoTbText = new Ext.Toolbar.TextItem({});

		dwr.engine.beginBatch();

		org.sample.service.AppInfo.getUserInfo(function(userInfo) {
			usernameTbText.setText(userInfo.username);
		});

		org.sample.service.AppInfo.getVersion(function(version) {
			appInfoTbText.setText('<b>Mortar WebApp Sample</b> v' + version);
		});

		dwr.engine.endBatch();

		this.items = [ {
			xtype : 'panel',
			region : 'north',
			height : 32,
			layout : 'hbox',
			layoutConfig : {
				align : 'stretch'
			},
			items : [ {
				xtype : 'container',
				html : '<div class="header-background"></div>',
				height : 31,
				flex : 1
			}, {
				xtype : 'container',
				margins : '0 0 0 0',
				layout : 'fit',
				flex : 1,
				items : [ {
					xtype : 'toolbar',
					buttonAlign : 'right',
					items : [ {
						xtype : 'tbseparator'
					}, {
						xtype : 'button',
						iconCls : 'icon-ayuda',
						text : 'Ayuda',
						handler : function() {
//							org.sample.service.sample.Hello.hello("mortar", function(r) {console.info(r);});
							org.sample.service.sample.Hello.changeGreeting("Good evening", function(r) {console.info(r);});
						}
					}, '-', {
						xtype : 'button',
						iconCls : 'icon-salir',
						text : 'Cerrar Sesión',
						scope : this,
						handler : function() {
							this.fireEvent('logout');
						}
					} ]
				} ]

			} ]
		}, {
			xtype : 'tabpanel',
			activeTab : 0,
			region : 'center',
			ref : 'tab',
			items : [ {
				xtype : 'panel',
				title : 'Home',
				bodyStyle : 'background:url(' + Mortar.app.props.CONTEXT_ROOT + '/images/mortar.png) no-repeat center top;'
			} ]
		}, {
			xtype : 'container',
			region : 'south',
			height : 23,
			items : [ {
				xtype : 'statusbar',
				defaultText : '',
				items : [ '-', usernameTbText, '-', appInfoTbText ]
			} ]
		} ];
		org.sample.Home.superclass.initComponent.call(this);
	}
});
