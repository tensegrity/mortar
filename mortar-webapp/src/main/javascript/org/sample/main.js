Ext.onReady(function() {

	var doLogout = function() {
		Ext.MessageBox.show({
			title : 'Mensaje del Sistema',
			msg : '¿Confirma el cerrar su sesión actual activa? <br><br>Los cambios no almacenados en esta sesión se perderán.',
			buttons : Ext.MessageBox.YESNO,
			fn : function(btn) {
				if (btn == 'yes') {
					Ext.Ajax.request({
						url : '/j_spring_security_logout',
						success : function() {
							window.location = 'jsp/index.jsp';
						},
						failure : function() {
							Ext.MessageBox.show({
								title : 'Mensaje del sistema',
								msg : 'Ocurrio un error inesperado en el sistema al tratar de cerrar la sesion.',
								buttons : Ext.MessageBox.OK,
								icon : Ext.MessageBox.ERROR
							});
						}
					});
				}
			}
		});
	};

	new org.sample.Home({
		renderTo : Ext.getBody(),
		listeners : {
			logout : doLogout
		}
	});
});