package org.mortar.persistence.eclipselink.util;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.expressions.Expression;
import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.queries.ReportQuery;
import org.eclipse.persistence.queries.ReportQueryResult;
import org.mortar.application.RuntimeExceptionWrapper;
import org.mortar.persistence.eclipselink.BaseEclipseLinkDAO;
import org.mortar.persistence.util.ComboHelperDAO;
import org.mortar.util.EntityQuery;
import org.mortar.util.Page;
import org.mortar.util.EntityQuery.Conditions;

public class ComboHelperEclipseLinkDAO extends BaseEclipseLinkDAO implements ComboHelperDAO {

	private static final String DATE_FORMAT = "dd/MM/yyyy";

	@SuppressWarnings("unchecked")
	private Field getFieldDefinition(Class entityClass, String fieldName) {
		Field field = null;
		Class currentClass = entityClass;
		if (fieldName.indexOf(".") > 0) {
			String[] fields = fieldName.split("\\.");

			for (int i = 0; i != fields.length - 1; i++) {
				currentClass = this.getSession().getClassDescriptorForAlias(
						StringUtils.capitalize(fields[i])).getJavaClass();
			}
			try {
				field = currentClass.getDeclaredField(fields[fields.length - 1]);
			} catch (Exception e) {
				throw new RuntimeExceptionWrapper(e);
			}
		} else {
			try {
				field = currentClass.getDeclaredField(fieldName);
			} catch (Exception e) {
				throw new RuntimeExceptionWrapper(e);
			}
		}
		return field;
	}

	@SuppressWarnings("unchecked")
	private List<?> buildCriteriaAndLoad(EntityQuery query) {
		Class entityClass = this.getSession().getClassDescriptorForAlias(query.getEntity())
				.getJavaClass();
		ExpressionBuilder eb = new ExpressionBuilder(entityClass);
		Expression exp = null;
		if (query.hasFilters()) {
			for (EntityQuery.Filter filter : query.getFilters()) {
				Field field = getFieldDefinition(entityClass, filter.getField());
				Class<?> paramClass = field.getType();
				List<Object> filterValues = new ArrayList<Object>();
				if (null == filter.getCondition()) {
					//Set the default condition...
					filter.setCondition(Conditions.EQUAL);
					//throw new RuntimeException("The condition for filter was not specified.");
				}
				if (filter.getCondition().equals(Conditions.IN)
						|| filter.getCondition().equals(Conditions.NOT_IN)
						|| filter.getCondition().equals(Conditions.BETWEEN)
						|| filter.getCondition().equals(Conditions.NOT_BETWEEN)) {
					String[] values = filter.getValue().split(",");
					for (String value : values) {
						filterValues.add(getRealValue(value, paramClass));
					}
				} else {
					filterValues.add(getRealValue(filter.getValue(), paramClass));
				}

				exp = getExpression(eb, exp, filter, filterValues);
			}
		}

		List<?> result = null;

		if (query.hasProjections()) {
			ReportQuery rq = new ReportQuery(entityClass, eb);

			for (String projection : query.getProjections()) {
				rq.addAttribute(projection);
			}
			rq.setSelectionCriteria(exp);
	        List<ReportQueryResult> results = (List)this.getSession().executeQuery(rq);
	        ReportQueryResultConverter converter = new ReportQueryResultConverter(rq, query.getProjections().toArray(new String[]{}));

			result = (List<?>) converter.convert(results);

		} else {
			ReadAllQuery qr = new ReadAllQuery(entityClass, eb);

			if (query.hasOrderings()) {
				for (EntityQuery.Order order : query.getOrder()) {
					if (EntityQuery.Order.Dir.DESC == order.getDirection()) {
						qr.addAscendingOrdering(order.getField());
					} else {
						qr.addDescendingOrdering(order.getField());
					}
				}
			}
			qr.setSelectionCriteria(exp);
			result = (List<?>) this.getSession().executeQuery(qr);
		}

		return result;
	}

	private Expression getExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, List<Object> filterValues) {

		switch (filter.getCondition()) {
		case LIKE:
			return getLikeExpression(eb, exp, filter, "%" + filterValues.get(0) + "%", false);
		case NOT_LIKE:
			return getLikeExpression(eb, exp, filter, "%" + filterValues.get(0) + "%", true);			
		case EQUAL:
			return getEqualExpression(eb, exp, filter, filterValues.get(0), false);
		case NOT_EQUAL:
			return getEqualExpression(eb, exp, filter, filterValues.get(0), true);	
		case IN:
			return getInExpression(eb, exp, filter, filterValues.toArray(new Object[]{}), false);
		case NOT_IN:
			return getInExpression(eb, exp, filter, filterValues.toArray(new Object[]{}), true);
		case BETWEEN:
			return getBetweenExpression(eb, exp, filter, filterValues.toArray(new Object[]{}), false);
		case NOT_BETWEEN:
			return getBetweenExpression(eb, exp, filter, filterValues.toArray(new Object[]{}), true);
		case GT:
			return getGreaterThanExpression(eb, exp, filter, filterValues.get(0), false);
		case NOT_GT:
			return getGreaterThanExpression(eb, exp, filter, filterValues.get(0), true);
		case GE:
			return getGreaterThanEqualExpression(eb, exp, filter, filterValues.get(0), false);
		case NOT_GE:
			return getGreaterThanEqualExpression(eb, exp, filter, filterValues.get(0), true);				
		case LT:
			return getLessThanExpression(eb, exp, filter, filterValues.get(0), false);
		case NOT_LT:
			return getLessThanExpression(eb, exp, filter, filterValues.get(0), true);
		case LE:
			return getLessThanEqualExpression(eb, exp, filter, filterValues.get(0), false);
		case NOT_LE:
			return getLessThanEqualExpression(eb, exp, filter, filterValues.get(0), true);
		default:
			return getEqualExpression(eb, exp, filter, filterValues.get(0), false);
		} 
		
	}

	private Object getRealValue(String value, Class<?> paramClass) {
		if (paramClass == Boolean.TYPE || paramClass == Boolean.class) {
			return Boolean.getBoolean(value);
		} else if (paramClass == String.class) {
			return String.valueOf(value);
		} else if (paramClass == Integer.class || paramClass == Integer.TYPE) {
			return Integer.valueOf(value);
		} else if (paramClass == Long.class || paramClass == Long.TYPE) {
			return Long.valueOf(value);
		} else if (paramClass == Double.class || paramClass == Double.TYPE) {
			return Double.valueOf(value);
		} else if (paramClass == Date.class) {
			return parseDate(value);
		} else if (paramClass == java.sql.Date.class) {
			Date date = parseDate(value);
			if (null != date) {
				return new java.sql.Date(date.getTime());
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private Date parseDate(String value) {
		try {
			return DateUtils.parseDate(value, new String[] { DATE_FORMAT });
		} catch (ParseException e) {
			this.logger.error("Error converting to java.util.Date value: " + value
					+ ", using format:" + DATE_FORMAT, e);
			return null;
		}
	}
	
	private Expression getEqualExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value, boolean doNot) {

		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).equal(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).equal(value));
		}
		if (doNot) {
			exp = exp.not();
		}		
		return exp;
	}
	
	private Expression getInExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object[] value, boolean doNot) {

		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).in(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).in(value));
		}
		if (doNot) {
			exp = exp.not();
		}		
		return exp;
	}	

	private Expression getLikeExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, String value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).likeIgnoreCase(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).likeIgnoreCase(value));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}
	
	private Expression getBetweenExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object[] value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).between(value[0], value[1]);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).between(value[0], value[1]));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}
	
	private Expression getGreaterThanExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).greaterThan(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).greaterThan(value));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}
	
	private Expression getGreaterThanEqualExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).greaterThanEqual(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).greaterThanEqual(value));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}	
	
	private Expression getLessThanExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).lessThan(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).lessThan(value));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}

	private Expression getLessThanEqualExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value, boolean doNot) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).lessThanEqual(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).lessThanEqual(value));
		}
		if (doNot) {
			exp = exp.not();
		}
		return exp;
	}	
	
	private Expression getNestedField(ExpressionBuilder eb, String fieldName) {
		Expression exp = null;
		String[] fields = fieldName.split("\\.");
		for (String field : fields) {
			if (null == exp) {
				exp = eb.get(field);
			} else {
				exp = exp.get(field);
			}
		}
		return exp;
	}

	public List<?> load(EntityQuery query) {
		return buildCriteriaAndLoad(query);
	}

	public Page<?> loadPaged(EntityQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

}
