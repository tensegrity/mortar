package org.mortar.persistence.eclipselink.util;

import java.util.List;

import org.eclipse.persistence.exceptions.DatabaseException;
import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.eclipse.persistence.internal.queries.ReportItem;
import org.eclipse.persistence.internal.sessions.AbstractRecord;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.queries.ReportQuery;
import org.eclipse.persistence.queries.ReportQueryResult;

/**
 * Example extension to the ReportQuery to allow the resulting collection of ReportQueryresults to
 * be converted into a collection of user specified view objects based on a constructor. The intent
 * is to provide EJB 3.0 style constructor returned from projection queries.
 * 
 * @author Doug Clarke, TopLink PM
 */

//TODO: Revisar por una implementación mejor sin que se usen componentes deprecated.
public class ConverterReportQuery extends ReportQuery {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Class<?> resultClass;

	public ConverterReportQuery(Class<?> refClass, ExpressionBuilder eb, Class<?> resultClass) {
		super(refClass, eb);
		this.resultClass = resultClass;
	}

	public ConverterReportQuery(Class<?> refClass, Class<?> resultClass) {
		this(refClass, new ExpressionBuilder(refClass), resultClass);
	}

	public ConverterReportQuery(Class<?> refClass) {
		this(refClass, new ExpressionBuilder(refClass), null);
	}
	
	public ConverterReportQuery(Class<?> refClass, ExpressionBuilder eb) {
		this(refClass, eb, null);
	}	

	public Class<?> getResultClass() {
		return resultClass;
	}

	/**
	 * INTERNAL: Additional fields can be added to a query. This is used in m-m bacth reading to
	 * bring back the key from the join table.
	 */
	public List<Object> getAdditionalFields() {
		return super.getAdditionalFields();
	}

	
	@SuppressWarnings("unchecked")
	public Object execute(AbstractSession session, AbstractRecord row) throws DatabaseException {
		List<ReportQueryResult> results = (List)super.execute(session, row);

		ReportQueryResultConverter converter = new ReportQueryResultConverter(this,
				getResultClass());

		return converter.convert(results);
	}
	
	public List<ReportItem> getItems() {
		return super.getItems(); 
	}
}
