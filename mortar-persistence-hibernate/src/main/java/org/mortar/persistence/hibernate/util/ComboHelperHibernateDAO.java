/* Copyright GUCOBA Systems */

package org.mortar.persistence.hibernate.util;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.type.Type;
import org.mortar.persistence.hibernate.BaseHibernateDAO;
import org.mortar.persistence.util.ComboHelperDAO;
import org.mortar.util.EntityQuery;
import org.mortar.util.ExceptionUtils;
import org.mortar.util.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Implementación en Hibernate para la interfase ComboHelperDAO, la cual define métodos que facilitan la consulta de datos para el llenado de combo boxes para el front end.
 * 
 * @author Pablo Krause
 * 
 */
@Repository
public class ComboHelperHibernateDAO extends BaseHibernateDAO implements ComboHelperDAO {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * Crea un objeto Criteria para la entidad dada por entityName
	 * 
	 * @param query
	 * @return
	 */
	private Criteria buildCriteria(EntityQuery query) {
		Type type = ((SessionFactoryImpl) getSessionFactory()).getReturnTypes("from " + query.getEntity())[0];
		Criteria criteria = getSession().createCriteria(type.getName());

		if (query.hasOrderings()) {
			for (EntityQuery.Order order : query.getOrder()) {
				if (EntityQuery.Order.Dir.DESC == order.getDirection()) {
					criteria.addOrder(Order.desc(order.getField()));
				} else {
					criteria.addOrder(Order.asc(order.getField()));
				}
			}
		}

		if (query.hasFilters()) {
			for (EntityQuery.Filter filter : query.getFilters()) {
				Class<?> paramClass;
				try {
					//Se trata de filtar sobre un inner field... soportado hasta nivel 1
					String fieldName = filter.getField();
					if (filter.getField().indexOf(".") > 0) {
						String innerClass = filter.getField().substring(0, filter.getField().indexOf("."));
						fieldName = filter.getField().substring(filter.getField().indexOf(".") + 1);
						type = ((SessionFactoryImpl) getSessionFactory()).getReturnTypes("from " + StringUtils.capitalize(innerClass))[0];
					} 
					Field field = type.getReturnedClass().getDeclaredField(fieldName);
					paramClass = field.getType();
				} catch (SecurityException e) {
					throw ExceptionUtils.throwUnchecked(e);
				} catch (NoSuchFieldException e) {
					throw ExceptionUtils.throwUnchecked(e);
				}
				if (paramClass == Boolean.TYPE || paramClass == Boolean.class) {
					criteria.add(Restrictions.eq(filter.getField(), Boolean.parseBoolean(filter.getValue())));
				} else if (paramClass == Integer.class || paramClass == Integer.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Integer.valueOf(filter.getValue())));
				} else if (paramClass == Long.class || paramClass == Long.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Long.valueOf(filter.getValue())));
				} else if (paramClass == Double.class || paramClass == Double.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Double.valueOf(filter.getValue())));
				} else {
					criteria.add(Restrictions.ilike(filter.getField(), filter.getValue(), MatchMode.ANYWHERE));
				}
			}
		}

		if (query.hasProjections()) {
			ProjectionList plist = Projections.projectionList();
			for (String projection : query.getProjections()) {
				plist.add(Projections.property(projection));
			}
			criteria.setProjection(plist);
		}

		// TODO: pending implementation of conditions

		return criteria;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<?> load(EntityQuery query) {
		Criteria criteria = buildCriteria(query);
		return criteria.list();
	}

	public Page<?> loadPaged(EntityQuery query) {
		// Criteria criteria = buildCriteria(query);
		// return super.findPage(criteria, firstRecord, aPageSize);
		return null;
	}

}
