/* Copyright GUCOBA Systems */

package org.mortar.persistence.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.metadata.ClassMetadata;
import org.mortar.persistence.EntityDAO;
import org.mortar.util.Page;
import org.mortar.util.PagedRequest;
import org.mortar.util.SortDir;
import org.mortar.util.SortInfo;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Toda endidad persistente requiere de un DAO para llevar a cabo operaciones de persistencia que
 * deber&aacute; extender de esta clase, la cual provee funcionalidad b&aacute;sica CRUD.
 * 
 * @author Pablo Krause, powered by GUCOBA Systems S.C.
 * 
 * @param <T>
 *            El tipo de entidad que sera manejado por la instancia del DAO
 * @param <ID>
 *            El tipo de la llave primaria de la entidad
 */
public abstract class EntityHibernateDAO<T, ID extends Serializable> extends BaseHibernateDAO
		implements EntityDAO<T, ID> {
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public EntityHibernateDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}
	
	/**
	 * Devuelve el ClassMetadata de Hibernate para la entidad que es manejada por la instancia del
	 * DAO.
	 * 
	 * @return el ClassMetadata de Hibernate para la entidad que es manejada por la instancia del
	 *         DAO
	 */
	protected ClassMetadata getClassMetadata() {
		return getSessionFactory().getClassMetadata(getPersistentClass());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public T findById(ID id) {
		return findById(id, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getSession().load(getPersistentClass(), id, LockMode.UPGRADE);
		} else {
			entity = (T) getSession().load(getPersistentClass(), id);
		}
		return entity;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public T getById(ID id) {
		return (T) getSession().get(getPersistentClass(), id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> findAll() {
		return findByCriteria();
	}

	/**
	 * {@inheritDoc}
	 */
	public Page<T> findAll(int firstRecord) {
		return findAll(firstRecord, -1);
	}

	/**
	 * {@inheritDoc}
	 */
	public Page<T> findAll(int firstRecord, int pageSize) {
		List<Order> order = new ArrayList<Order>(1);
		// Agregamos ordenamiento por defecto en base a la llave primaria
		// para que al menos haya consistencia en cuanto a la generacion de paginas
		String identityProperty = getClassMetadata().getIdentifierPropertyName();
		order.add(Order.asc(identityProperty));
		Criteria totalCriteria = createCriteria().setProjection(Projections.count(identityProperty));
		return findByCriteria(firstRecord, pageSize, order, totalCriteria);
	}

	/**
	 * Devuelve la lista completa de resultados en base a un Criterion, ordenada por su campo
	 * identidad.
	 * 
	 * @param criterion
	 *            lista opcional de restricciones
	 * @return List de resultados completa (i.e. sin paginar)
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria criteria = createCriteria();
		for (Criterion c : criterion) {
			if (null != c) {
				criteria.add(c);
			}
		}
		return criteria.list();
	}

	/**
	 * Devuelve una página de resultados para el Criterion dado. Utiliza tamaño de página
	 * predeterminado.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una página de resultados para el Criterion dado
	 */
	protected Page<T> findByCriteria(int firstRecord, Criterion... criterion) {
		return findByCriteria(firstRecord, -1, criterion);
	}

	/**
	 * Devuelve una página de resultados para el Criterion dado. El resultado está ordenado en base
	 * al campo identidad o llave primaria de la entidad.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param pageSize
	 *            El tamaño de la página
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una página de resultados para el Criterion dado
	 */
	protected Page<T> findByCriteria(int firstRecord, int pageSize, Criterion... criterion) {
		List<Order> order = new ArrayList<Order>(1);
		// Agregamos ordenamiento por defecto en base a la llave primaria
		// para que al menos haya consistencia en cuanto a la generacion de paginas
		String identityProperty = getClassMetadata().getIdentifierPropertyName();
		order.add(Order.asc(identityProperty));
		return findByCriteria(firstRecord, pageSize, order, criterion);
	}

	/**
	 * Warning: Assumes simple cases where total count of elements can be coumputed by a simple count criteria over the entity.
	 * 
	 * @param firstRecord
	 * @param pageSize
	 * @param order
	 * @param criterion
	 * @return
	 */
	protected Page<T> findPageByCriteria(int firstRecord, int pageSize, Order order, Criterion... criterion) {
		List<Order> orders = null;
		if (null != order) {
			orders = new ArrayList<Order>();
			orders.add(order);
		}
		String identityProperty = getClassMetadata().getIdentifierPropertyName();
		Criteria totalCriteria = createCriteria().setProjection(Projections.count(identityProperty));
		return findByCriteria(firstRecord, pageSize, orders, totalCriteria, criterion);
	}

	/**
	 * A protected version of findPage which additionally accepts criterion. To be used by DAO that extend from this class.
	 * Warning: a criteria for getting the total amount of elements is created automatically; this only works in simple
	 * cases (i.e. flat queries).  If you have problems with the total number of elements provide an appropriate 
	 * criteria or query to obtain the total count.
	 * 
	 * @param request
	 * @param criterion
	 * @return
	 */
	protected Page<T> findPageByCriteria(PagedRequest request, Criterion... criterion) {
		List<Order> orders = createOrderList(request);
		String identityProperty = getClassMetadata().getIdentifierPropertyName();
		Criteria totalCriteria = createCriteria().setProjection(Projections.count(identityProperty));
		return findByCriteria(request.getStart(), request.getLimit(), orders, totalCriteria, criterion);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Page<T> findPage(PagedRequest request) {
		return findPageByCriteria(request);
	}
	
	protected Page<T> findByCriteria(int firstRecord, int pageSize, List<Order> order, Criteria totalCriteria, Criterion... criterion) {
		Criteria criteria = createCriteria();
		for (Criterion c : criterion) {
			if (null != c) {
				criteria.add(c);
			}
		}
		if (null != order) {
			for (Order o : order) {
				if (null != o) {
					criteria.addOrder(o);
				}
			}
		}
		return (Page<T>) findPage(criteria, (List<Order>) null, totalCriteria, firstRecord, pageSize);
	}
		
	/**
	 * Devuelve una página de resultados para el Criterion dado. Este método es el más completo de la
	 * familia de métodos con el mismo nombre ya que permite especificar, además de la información
	 * de paginación y criterios de búsqueda, el ordenamiento del resultado.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param pageSize
	 *            El tamaño de la página
	 * @param order
	 *            una lista con los criterios de ordenamiento requeridos
	 * @param criterion
	 *            criterio de búsqueda
	 * @return una página de resultados para el Criterion y ordenamiento dados
	 */
	@SuppressWarnings("unchecked")
	protected Page<T> findByCriteria(int firstRecord, int pageSize, List<Order> order, Criterion... criterion) {
		return findByCriteria(firstRecord, pageSize, order, null, criterion);
	}

	/**
	 * Crea un objeto Order de Hibernate en base a la información contenida en el PagedRequest.
	 * 
	 * @param request
	 *            un PagedRequest con la información de ordenamiento
	 * @return un objeto Order para ser utilizado en queries de Hibernate.
	 * @deprecated Use createOrderList instead
	 */
	@Deprecated
	protected Order createOrder(PagedRequest request) {
		Order order = null;
		if (request.isAscendingOrder()) {
			order = Order.asc(request.getSort());
		} else {
			order = Order.desc(request.getSort());
		}
		return order;
	}
	
	/**
	 * Create a list of Hibernate's Order objects based on the information on the request.
	 * Ther might be more than one ordering criteria.
	 * 
	 * @param request
	 * @return
	 */
	protected List<Order> createOrderList(PagedRequest request) {
		List<Order> orderList = new ArrayList<Order>();
		Order order = null;
		if (request.isAscendingOrder()) {
			order = Order.asc(request.getSort());
		} else {
			order = Order.desc(request.getSort());
		}
		orderList.add(order);
		// Additional sorting information:
		if (null != request.getAdditionalSort() && request.getAdditionalSort().length > 0) {
			for (SortInfo sort : request.getAdditionalSort()) {
				if (sort.getDir() == SortDir.ASC) {
					order = Order.asc(sort.getField());
				} else {
					order = Order.desc(sort.getField());
				}
				orderList.add(order);
			}
		}
		return orderList;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance, String... excludeProperty) {
		Criteria criteria = createCriteria();
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		criteria.add(example);
		return criteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public Page<T> findByExample(int firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		Criteria criteria = createCriteria();
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		criteria.add(example);
		return (Page<T>) findPage(criteria, firstRecord, pageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public T saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T entity) {
		getSession().delete(entity);
	}

	/**
	 * 
	 * @return un objeto de tipo Criteria de Hibernate de la entidad manejada por este DAO
	 */
	public Criteria createCriteria() {
		return getSession().createCriteria(getPersistentClass());
	}

	// Los siguientes métodos se sobreescriben para que ya devuelvan el tipo de dato correcto.

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Query aQuery, int firstRecord, int aPageSize, int totalElements) {
		return (Page<T>) super.findPage(aQuery, firstRecord, aPageSize, totalElements);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Query aQuery, int firstRecord, int aPageSize) {
		return (Page<T>) super.findPage(aQuery, firstRecord, aPageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Criteria aCriteria, Order order, int firstRecord, int aPageSize) {
		return (Page<T>) super.findPage(aCriteria, order, firstRecord, aPageSize);
	}

}
