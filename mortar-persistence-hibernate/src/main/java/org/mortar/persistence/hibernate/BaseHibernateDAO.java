/* Copyright GUCOBA Systems */

package org.mortar.persistence.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.mortar.persistence.GenericDAO;
import org.mortar.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Esta clase sirve como base para los dos tipos de DAO del sistema, los DAO de
 * entidad y los DAO de repositorio. Los DAO de entidad tienen una relación uno
 * a uno con una entidad persistente y llevan a cabo las operaciones sobre la
 * entidad dada. Los DAO de repositorio operan sobre un conjunto de entidades a
 * la vez como por ejemplo para obtener informaci&oacute;n de varias entidades.
 * 
 * @author Pablo Krause, powered by GUCOBA Systems S.C.
 * 
 */
@Repository
public abstract class BaseHibernateDAO implements GenericDAO {
	/** Log disponible para subclases */
	protected final static Log LOG = LogFactory.getLog(BaseHibernateDAO.class);

	protected SessionFactory sessionFactory;

	public BaseHibernateDAO() {
		super();
	}

	/**
	 * Devuelve un objeto Query a partir del nombre de un query, el cual debe de
	 * estar definido en los archivos de hibernate.
	 * 
	 * @param queryName
	 *            el nombre del query
	 * @return el objeto Query asociado
	 */
	protected Query getNamedQuery(String queryName) {
		return getSession().getNamedQuery(queryName);
	}

	/**
	 * {@inheritDoc}
	 */
	public void clear() {
		getSession().clear();
	}

	/**
	 * {@inheritDoc}
	 */
	public void flush() {
		getSession().flush();
	}

	/**
	 * Devuelve la sesi&oacute;n activa. Si no hay sesión activa este
	 * m&eacute;todo No crea una nueva sesi&oacute;n. Aseg&uacute;rese de qué
	 * est&eacute; bien configurado su ambiente para que exista una
	 * sesi&oacute;n activa al momento de invocar &eacute;ste m&eacute;todo.
	 * 
	 * @return la sesión activa
	 */
	public Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	// Accessors

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Utilizar el m&eacute;todo con el mismo nombre pero que recibe puros int
	 * en lugar de longs.
	 * 
	 * @param aQuery
	 *            deprecated
	 * @param firstRecord
	 *            deprecated
	 * @param aPageSize
	 *            deprecated
	 * @param totalElements
	 *            deprecated
	 * @return deprecated
	 * @throws HibernateException
	 *             deprecated
	 */
	@Deprecated
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize, long totalElements) throws HibernateException {
		return findPage(aQuery, firstRecord, aPageSize, (int) totalElements);
	}

	/**
	 * Obtiene una página de información a partir de un Query de búsqueda
	 * especificado por el objeto Query. A diferencia el método del mismo nombre
	 * que funciona con objetos de tipo Criteria, éste método no es capaz de
	 * determinar el total de registros automáticamente. En caso de requerir el
	 * total es necesario que se pase como parámetro, de lo contrario éste
	 * método devolverá un valor falso del total de registros igual a al número
	 * del último registro de la página más uno (lo que permite que las
	 * paginaciones funcionen correctamente, aunque el número total reportado no
	 * sea correcto). En caso de que se llegara a la última página de
	 * resultados, este método si se da cuenta y sólo entonces devuelve el
	 * número correcto del total de registros.
	 * 
	 * @param aQuery
	 *            el Query que se desea ejecutar y paginar
	 * @param firstRecord
	 *            el número del primer registro que se quiere traer (empezando
	 *            en 0)
	 * @param aPageSize
	 *            el número de registros a traer
	 * @param totalElements
	 *            El número total de elementos que devuelve el query, o -1 si no
	 *            se necesita
	 * @return un objeto Page con los registros encontrados
	 * @throws HibernateException
	 *             si algo falla
	 */
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize, int totalElements) throws HibernateException {
		int pageSize = aPageSize;
		int total = totalElements;
		if (pageSize <= 0) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug(aQuery.getQueryString());
		}
		Query query = aQuery.setFirstResult(firstRecord);
		// No nos confiamos del total y siempre pedimos 1 registro mas
		// para saber si hay otra página de datos.
		query = query.setMaxResults(pageSize + 1);
		List<?> elements = query.list();
		boolean hasNextPage = false;
		if (elements.size() > pageSize) {
			// Eliminamos el último registro, que utilizamos
			// para saber si había otra página.
			elements.remove(pageSize);
			hasNextPage = true;
		}
		if (!hasNextPage) {
			// Si ya no hay mas paginas, ya sabemos el total de registros.
			total = firstRecord + elements.size();
		}
		if (total < 0) {
			// Si no sabemos el total, ponemos un total falso de 1 elemento mas
			// que la
			// pagina actual
			// para que el paginador funcione y permita solicitar la siguiente
			// pagina
			total = firstRecord + pageSize + 1;
		}
		@SuppressWarnings("unchecked")
		Page<?> page = new Page(elements, firstRecord, pageSize, total);
		page.setHasNextPage(hasNextPage);
		return page;
	}

	/**
	 * Obtiene una página de información dado un Query en casos donde no se
	 * necesita saber el total de elementos. Este método manda a llamar a
	 * {@link #findPage(Criteria, Order, int, int)} sin especificar el número
	 * total de elementos.
	 * 
	 * @param aQuery
	 *            el Query que se desea ejecutar y paginar
	 * @param firstRecord
	 *            el número del primer registro que se quiere traer (empezando
	 *            en 0)
	 * @param aPageSize
	 *            el número de registros a traer
	 * @return un objeto Page con los registros encontrados
	 * @throws HibernateException
	 */
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize) {
		return this.findPage(aQuery, firstRecord, aPageSize, -1);
	}

	/**
	 * Este metodo se depreci&oacute; en favor del m&eacute;todo con el mismo
	 * nombre pero que recibe int en lugar de long para firstRecord.
	 * 
	 * @param aCriteria
	 *            deprecated
	 * @param firstRecord
	 *            deprecated
	 * @param aPageSize
	 *            deprecated
	 * @return deprecated
	 * @throws HibernateException
	 *             deprecated
	 */
	@Deprecated
	protected Page<?> findPage(Criteria aCriteria, long firstRecord, int aPageSize) throws HibernateException {
		return findPage(aCriteria, (int) firstRecord, aPageSize);
	}

	/**
	 * 
	 * Este método se encarga de obtener una página de resultados a partir del
	 * criterio de búsqueda especificado por el objeto criteria. Este método se
	 * encarga de obtener el número total de registros que arrojaría el
	 * criteria.
	 * 
	 * ADVERTENCIA: El argumento Criteria es modificado por este método y no
	 * debe vovler a utilizarse. Esto con el fin de poder obtener el número
	 * total de registros.
	 * 
	 * @param aCriteria
	 *            El criterio original que se quiere paginar.
	 * @param firstRecord
	 *            El primer registro requerido (comenzando en 0).
	 * @param aPageSize
	 *            El número de elementos requerido.
	 * @return una página de resultados
	 * @throws HibernateException
	 *             en caso de error
	 */
	protected Page<?> findPage(Criteria aCriteria, int firstRecord, int aPageSize) throws HibernateException {
		return findPage(aCriteria, null, firstRecord, aPageSize);
	}

	/**
	 * Este método se encarga de obtener una página de resultados a partir del
	 * criterio de búsqueda especificado por el objeto criteria. Este método se
	 * encarga de obtener el número total de registros que arrojaría el
	 * criteria.
	 * 
	 * ADVERTENCIA: El argumento Criteria es modificado por este método y no
	 * debe vovler a utilizarse. Esto con el fin de poder obtener el número
	 * total de registros.
	 * 
	 * @param aCriteria
	 *            El criterio original que se quiere paginar.
	 * @param order
	 *            el orden en el que se quieren los resultados
	 * @param firstRecord
	 *            El primer registro requerido (comenzando en 0).
	 * @param aPageSize
	 *            El número de elementos requerido.
	 * @return una página de resultados
	 * @throws HibernateException
	 *             en caso de error
	 */
	protected Page<?> findPage(Criteria aCriteria, Order order, int firstRecord, int aPageSize) throws HibernateException {
		return findPage(aCriteria, order, null, firstRecord, aPageSize);
	}

	protected Page<?> findPage(Criteria aCriteria, Order order, Criteria totalCountCriteria, int firstRecord, int aPageSize) throws HibernateException {
		List<Order> orderList = null;
		if (null != order) {
			orderList = new ArrayList<Order>();
		}
		return findPage(aCriteria, orderList, totalCountCriteria, firstRecord, aPageSize);
	}
	
	protected Page<?> findPage(Criteria aCriteria, List<Order> order, Criteria totalCountCriteria, int firstRecord, int aPageSize) throws HibernateException {
		int totalElements = -1;
		boolean hasNextPage = false;
		int pageSize = aPageSize;

		if (pageSize <= 0) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
			LOG.debug("Using default page size: " + pageSize);
		}

		if (null != order && order.size() > 0) {
			for (Order o : order) {
				aCriteria.addOrder(o);
			}
		}

		// Modificamos el criteria para obtener unicamente los registros
		// de la pagina solicitada:
		Criteria criteria = aCriteria.setFirstResult((int) firstRecord);
		// No nos confiamos del total y siempre pedimos 1 registro mas
		// para saber si hay otra página de datos.
		criteria = criteria.setMaxResults(pageSize + 1);

		List<?> elements = criteria.list();

		if (elements.size() > pageSize) {
			// Eliminamos el último registro, que utilizamos
			// para saber si había otra página.
			elements.remove(pageSize);
			hasNextPage = true;
		}
		if (hasNextPage) {
			if (totalCountCriteria != null) {
				Object result = totalCountCriteria.uniqueResult();
				if (result instanceof Integer) {
					totalElements = (Integer) result;
				} else if (result instanceof Long) {
					totalElements = ((Long) result).intValue();
				}
			}
		} else {
			// Si ya no hay mas paginas, ya sabemos el total de registros.
			totalElements = firstRecord + elements.size();
		}
		@SuppressWarnings("unchecked")
		Page<?> page = new Page(elements, firstRecord, pageSize, totalElements);
		page.setHasNextPage(hasNextPage);
		return page;
	}

}
