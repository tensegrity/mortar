/**
 * Mortar
 */
package org.mortar.persistence.hibernate.usertype;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

/**
 * Parameterizable UserType for dates that are stored as Strings in the DB. This
 * is intended for legacy databases only. New applications should use Date
 * datatypes.
 * 
 * <p>Usage:</p>
 * 
 * <pre>
 * &#064;TypeDef(
	name=&quot;dateAsString&quot;,
	typeClass=StringDateUserType.class
	parameters={&#064;Parameter(name=&quot;pattern&quot;, value=&quot;yyyyMMdd&quot;)})
 * </pre>
 * 
 * and then you can annotate each field as:
 * 
 * <pre>
 * &#064;Type(type = &quot;dateAsString&quot;)
 * &#064;Column(name = &quot;SAMPLE_DATE&quot;)
 * private Date sampleDate;
 * </pre>
 * 
 * 
 * @author Pablo Krause
 * 
 */
public class DateAsStringUserType implements UserType, ParameterizedType {
	private String datePattern;

	/**
	 * Converts a date as String to a Date object using <code>pattern</code>
	 * The String is trimmed before attempting convertion.
	 * If the String is "0" it is treated as null (TODO could be parametrized).
	 * 
	 * @param paramDateString
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	protected static Date toDate(String paramDateString, String pattern) throws ParseException {
		String dateString = null == paramDateString ? null : paramDateString.trim();
		if (null == dateString || dateString.length() == 0
				|| "0".equals(dateString)) {
			return null;
		}
		Date date;
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setLenient(false);
		date = dateFormat.parse(dateString);
		return date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#nullSafeGet(java.sql.ResultSet,
	 * java.lang.String[], java.lang.Object)
	 */
	public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner)
			throws HibernateException, SQLException {
		String dateString = resultSet.getString(names[0]);
		if (resultSet.wasNull()) {
			return null;
		}
		try {
			return toDate(dateString, this.datePattern);
		} catch (ParseException e) {
			throw new HibernateException("Error converting String date '"+dateString+"' to Date object using pattern"+datePattern, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hibernate.usertype.UserType#nullSafeSet(java.sql.PreparedStatement,
	 * java.lang.Object, int)
	 */
	public void nullSafeSet(PreparedStatement statement, Object value, int index)
			throws HibernateException, SQLException {
		if (value == null) {
			statement.setNull(index, Types.CHAR);
		} else {
			Date date = (Date) value;
			SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
			statement.setString(index, dateFormat.format(date));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#assemble(java.io.Serializable,
	 * java.lang.Object)
	 */
	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return cached;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#deepCopy(java.lang.Object)
	 */
	public Object deepCopy(Object date) throws HibernateException {
		if (null == date) {
			return null;
		}
		return new Date(((Date) date).getTime());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#disassemble(java.lang.Object)
	 */
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#equals(java.lang.Object,
	 * java.lang.Object)
	 */
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y)
			return true;
		if (x == null || y == null)
			return false;
		return x.equals(y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#hashCode(java.lang.Object)
	 */
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#isMutable()
	 */
	public boolean isMutable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#replace(java.lang.Object,
	 * java.lang.Object, java.lang.Object)
	 */
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		if (null == original) {
			return null;
		}
		return new Date(((Date) original).getTime());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#returnedClass()
	 */
	@SuppressWarnings("unchecked")
	public Class returnedClass() {
		return Date.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.usertype.UserType#sqlTypes()
	 */
	public int[] sqlTypes() {
		return new int[] { Types.CHAR };
	}

	/**
	 * @see ParameterizedType#setParameterValues(Properties)
	 */
	public void setParameterValues(Properties paramProperties) {
		this.datePattern = paramProperties.getProperty("pattern");
		if (null == this.datePattern) {
			// Lets throw exception rather than use a default value which could
			// mask the error and make it difficult to find.
			throw new HibernateException("No date pattern specified for DateAsStringUserType");
		}
	}

}
