package org.mortar.persistence.hibernate.dialect;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.dialect.InformixDialect;

/**
 * Enhancement to Hibernates InformixDialect, which surprisingly can't cope
 * with stored procedures.
 * 
 * Thanks to Diego Paz Sierra for posting the solution <a href="http://www.epidataconsulting.com/tikiwiki/tiki-index.php?page=Hibernate%2BSP%2BBase+de+datos+Informix">here</a>.
 * @author Pablo Krause
 * 
 */
public class EnhancedInformixDialect extends InformixDialect {

	@Override
	public int registerResultSetOutParameter(CallableStatement statement,
			int col) throws SQLException {
		return col;
	}
	
	@Override
	public ResultSet getResultSet(CallableStatement ps) throws SQLException {
		boolean isResultSet = ps.execute();

		while (!isResultSet && ps.getUpdateCount() != -1) {
			isResultSet = ps.getMoreResults();
		}

		return ps.getResultSet();
	}
}
