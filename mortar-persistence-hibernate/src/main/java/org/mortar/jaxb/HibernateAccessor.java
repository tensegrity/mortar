package org.mortar.jaxb;

import org.hibernate.Hibernate;

import com.sun.xml.bind.api.AccessorException;
import com.sun.xml.bind.v2.runtime.JAXBContextImpl;
import com.sun.xml.bind.v2.runtime.reflect.Accessor;

public class HibernateAccessor<BeanT, ValueT> extends Accessor<BeanT, ValueT> {

	private Accessor<BeanT, ValueT> delegate;

	public HibernateAccessor(Accessor<BeanT, ValueT> delegate) {
		super(delegate.getValueType());
		this.delegate = delegate;
	}

	@Override
	public Accessor<BeanT, ValueT> optimize(JAXBContextImpl context) {
		delegate = delegate.optimize(context);
		return this;
	}

	@Override
	public ValueT get(BeanT bean) throws AccessorException {
		return hideLazy(delegate.get(bean));
	}

	@Override
	public void set(BeanT bean, ValueT value) throws AccessorException {
		delegate.set(bean, value);
	}

	protected ValueT hideLazy(ValueT value) {
		if (Hibernate.isInitialized(value)) {
			return value;
		}
		return null;
	}
}