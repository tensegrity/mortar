package org.mortar.jaxb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.xml.bind.JAXBException;

import com.sun.xml.bind.AccessorFactory;
import com.sun.xml.bind.AccessorFactoryImpl;
import com.sun.xml.bind.v2.runtime.reflect.Accessor;

public class HibernateAccessorFactory implements AccessorFactory {

	private final AccessorFactory delegate;

	public HibernateAccessorFactory() {
		this(AccessorFactoryImpl.getInstance());
	}

	public HibernateAccessorFactory(AccessorFactory delegate) {
		this.delegate = delegate;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Accessor createFieldAccessor(Class bean, Field f, boolean readOnly)
			throws JAXBException {
		return new HibernateAccessor(delegate.createFieldAccessor(bean, f, readOnly));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Accessor createPropertyAccessor(Class bean, Method getter,
			Method setter) throws JAXBException {
		return new HibernateAccessor(delegate.createPropertyAccessor(bean, getter, setter));
	}
}