insert into product (id, name) values (1, 'Camera');
insert into product (id, name) values (2, 'Ipod');

insert into bid (id, name, active, product) values (1, 'Cámara',1, 1)
insert into bid (id, name, active, product) values (2, 'Laptop',1, 1)
insert into bid (id, name, active, product) values (3, 'iPad',1, 1)
insert into bid (id, name, active, product) values (4, 'iPod',0, 1)
insert into bid (id, name, active, product) values (5, 'Mouse',1, 1)
insert into bid (id, name, active, product) values (6, 'Hard Drive',0, 2)
insert into bid (id, name, active, product) values (7, 'Batteries',0, 2)
insert into bid (id, name, active, product) values (8, 'Cable',1, 2)
insert into bid (id, name, active, product) values (9, 'Memory',1, 2)
insert into bid (id, name, active, product) values (10, 'Enclosure',1, 2)
insert into bid (id, name, active, product) values (11, 'Stand',0, 2)