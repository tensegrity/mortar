package org.mortar.persistence.hibernate;

import org.mortar.persistence.test.domain.Bid;
import org.springframework.stereotype.Repository;

@Repository
public class SampleHibernateEntityDao extends EntityHibernateDAO<Bid, Integer> implements SimpleEntityDao {

}
