package org.mortar.persistence.hibernate;

import org.mortar.persistence.EntityDAO;
import org.mortar.persistence.test.domain.Bid;

public interface SimpleEntityDao extends EntityDAO<Bid, Integer> {

}
