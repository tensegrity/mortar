package org.mortar.persistence.hibernate;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mortar.persistence.util.ComboHelperDAO;
import org.mortar.util.EntityQuery;
import org.mortar.util.EntityQuery.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/persistenceContext.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@Transactional
public class ComboHelperDAOTest {

	@Autowired
	ComboHelperDAO comboHelperDAO;

	@Test
	public void doQuery() {
		EntityQuery query = new EntityQuery();
		List<String> projections = new ArrayList<String>();
		List<Filter> filters = new ArrayList<Filter>();
		
		projections.add("id");
		projections.add("name");
		projections.add("active");
		query.setEntity("Bid");
		
		//Para los activos
		filters.add(new Filter("active", "true"));
		
		query.setProjections(projections);
		query.setFilters(filters);
		
		List<?> result = comboHelperDAO.load(query);
		assertTrue(result.size() == 7);
		
		filters.clear();
		//Para los inactivos
		filters.add(new Filter("active", "false"));
		
		query.setProjections(projections);
		query.setFilters(filters);
		
		result = comboHelperDAO.load(query);	
		assertTrue(result.size() == 4);
	}
	
	@Test
	public void doQueryAdvanced() {
		EntityQuery query = new EntityQuery();
		List<String> projections = new ArrayList<String>();
		List<Filter> filters = new ArrayList<Filter>();
		
		projections.add("id");
		projections.add("name");
		projections.add("active");
		query.setEntity("Bid");
		
		//Para los activos
		filters.add(new Filter("active", "true"));
		filters.add(new Filter("product.id", "1"));
		
		query.setProjections(projections);
		query.setFilters(filters);
		
		List<?> result = comboHelperDAO.load(query);
		assertTrue(result.size() == 4);		
	}
	
	@Test
	public void noProjections() {
		EntityQuery query = new EntityQuery();
		List<String> projections = new ArrayList<String>();
		List<Filter> filters = new ArrayList<Filter>();
		
		query.setEntity("Bid");
		
		//Para los activos
		filters.add(new Filter("active", "true"));
		
		query.setProjections(projections);
		query.setFilters(filters);
		
		List<?> result = comboHelperDAO.load(query);
		assertTrue(result.size() == 7);
	}
}
