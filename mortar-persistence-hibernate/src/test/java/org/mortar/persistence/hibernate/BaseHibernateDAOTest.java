package org.mortar.persistence.hibernate;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mortar.persistence.test.domain.Bid;
import org.mortar.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/persistenceContext.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@Transactional
public class BaseHibernateDAOTest {

	@Autowired
	SimpleEntityDao simpleEntityDao;

	@Test
	public void shouldReturnSomething() {
		List<Bid> result = simpleEntityDao.findAll();
		assertTrue(result.size() > 0);
	}
	
	@Test
	public void pageWithKnownTotal() {
		Page<Bid> page = simpleEntityDao.findAll(2, 4);
		assertTrue(page.getElements().size() > 0);
		assertEquals(11, page.getTotal());
	}
	
//	@Test
//	public void pageWithUnknownTotal() {
//		Page<Bid> page = simpleEntityDao.
//		assertTrue(page.getElements().size() > 0);
//		assertEquals(11, page.getTotal());
//	}

}
