package org.mortar.web;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class JavascriptPathResolverTest {
	static JavascriptPathResolver resolver;
	
	@BeforeClass
	public static void init() {
		resolver = new JavascriptPathResolver("/contextPath");
	}

	@Test
	public void simpleTest() {
		String js = resolver.resolve("/contextPath/jsp/zzz.jsp");
		Assert.assertEquals("", "/js/zzz.js", js);
	}

	@Test
	public void shouldReturnDefaultFirstLevel() {
		String js = resolver.resolve("/contextPath/jsp");
		Assert.assertEquals("", "/js/index.js", js);
	}

	@Test
	public void shouldReturnDefaultNestedLevel() {
		String js = resolver.resolve("/contextPath/jsp/foo");
		Assert.assertEquals("", "/js/foo/index.js", js);
	}

	@Test
	public void shouldReturnDefaultFirstLevelTrailingSlash() {
		String js = resolver.resolve("/contextPath/jsp/");
		Assert.assertEquals("", "/js/index.js", js);
	}

	@Test
	public void shoudlFilterQueryParams() {
		String js = resolver.resolve("/contextPath/jsp/zzz.jsp?foo=bar");
		Assert.assertEquals("", "/js/zzz.js", js);
	}
	
	@Test
	public void shouldReturnNestedJS() {
		String js = resolver.resolve("/contextPath/jsp/foo/bar.jsp");
		Assert.assertEquals("", "/js/foo/bar.js", js);
	}
	
	/**
	 * Should return same path since it does not match the expected pattern
	 */
	@Test
	public void shouldReturnSamePath() {
		String js = resolver.resolve("/contextPath/foo/bar.jsp");
		Assert.assertEquals("", "/contextPath/foo/bar.jsp", js);
	}
	
	/**
	 * Should return same path since it does not match the expected pattern
	 */
	@Test
	public void shouldWorkWithoutContextRoot() {
		JavascriptPathResolver rootResolver = new JavascriptPathResolver();
		String js = rootResolver.resolve("/jsp/foo/bar.jsp");
		Assert.assertEquals("", "/js/foo/bar.js", js);
	}

}
