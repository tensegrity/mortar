package org.mortar.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class SampleServiceImpl implements SampleService {

	public SamplePojo sampleOperationWithoutAnnotation() {
		SamplePojo r = new SamplePojo();
		r.setLazyOn(true);
		return r;
	}

	@PopulateResponse({ "stringProperty", "lazyRelation" })
	public SamplePojo sampleOperation(String sampleArg) {
		SamplePojo r = new SamplePojo();
		r.setLazyOn(true);
		return r;
	}

	@PopulateResponse({ "stringProperty", "intProperty" })
	public SamplePojo sampleOperation2() {
		SamplePojo r = new SamplePojo();
		r.setLazyOn(true);
		return r;
	}

	@Override
	// @PopulateResponse({"![lazyRelation]"})
	@PopulateResponse({ "![lazyRelation]", "![lazyCollection]" })
	public List<SamplePojo> getList() {
		List<SamplePojo> response = new ArrayList<SamplePojo>();
		SamplePojo r = new SamplePojo();
		response.add(r);
		r = new SamplePojo();
		response.add(r);
		return response;
	}

	@Override
	@PopulateResponse({ "![lazyRelation]", "![lazyCollection]" })
	public SamplePojo returnNull() {
		return null;
	}

}
