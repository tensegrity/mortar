package org.mortar.annotation;

import java.util.ArrayList;
import java.util.List;

public class SamplePojo {
	private String stringProperty = null;
	private int intProperty = 0;
	private List<Integer> intListProperty = null;
	private String nullProperty = null;
	private SamplePojo lazyRelation;
	private List<SamplePojo> lazyCollection = null;

	public boolean lazyOn = true;

	public String getStringProperty() {
		return stringProperty;
	}

	public void setStringProperty(String stringPropery) {
		this.stringProperty = stringPropery;
	}

	public int getIntProperty() {
		return intProperty;
	}

	public void setIntProperty(int intProperty) {
		this.intProperty = intProperty;
	}

	public List<Integer> getIntListProperty() {
		return intListProperty;
	}

	public void setIntListProperty(List<Integer> intListProperty) {
		if (lazyOn && null == intListProperty) {
			this.intListProperty = new ArrayList<Integer>();
		}
		this.intListProperty = intListProperty;
	}

	public boolean isLazyOn() {
		return lazyOn;
	}

	public void setLazyOn(boolean lazyOn) {
		this.lazyOn = lazyOn;
	}

	public String getNullProperty() {
		return nullProperty;
	}

	public void setNullProperty(String nullProperty) {
		this.nullProperty = nullProperty;
	}

	public SamplePojo getLazyRelation() {
		if (lazyOn && null == lazyRelation) {
			this.lazyRelation = new SamplePojo();
		} else if (null == lazyRelation) {
			throw new RuntimeException("Simulated lazy-loading exception, no Session available");
		}
		return lazyRelation;
	}

	public void setLazyRelation(SamplePojo lazyRelation) {
		this.lazyRelation = lazyRelation;
	}

	public List<SamplePojo> getLazyCollection() {
		if (lazyOn && null == lazyCollection) {
			this.lazyCollection = new ArrayList<SamplePojo>();
		} else if (null == lazyCollection) {
			throw new RuntimeException("Simulated lazy-loading exception, no Session available");
		}
		return lazyCollection;
	}

	public void setLazyCollection(List<SamplePojo> lazyCollection) {
		this.lazyCollection = lazyCollection;
	}

}
