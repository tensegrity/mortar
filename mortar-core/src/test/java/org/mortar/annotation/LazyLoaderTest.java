package org.mortar.annotation;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/evalAnnotationTestContext.xml" })
public class LazyLoaderTest {

	@Autowired
	SampleService sampleService;

	@Test
	public void testLoadedLazyRelation() {
		SamplePojo response = sampleService.sampleOperation("FOOBAR");
		response.setLazyOn(false);
		Assert.assertNotNull(response.getLazyRelation());
	}

	@Test(expected = RuntimeException.class)
	public void shouldFailRelationNotLoaded() {
		SamplePojo response = sampleService.sampleOperationWithoutAnnotation();
		response.setLazyOn(false);
		Assert.assertNotNull(response.getLazyRelation());
	}

	@Test
	public void loadList() {
		List<SamplePojo> response = sampleService.getList();
		for (SamplePojo p : response) {
			p.setLazyOn(false);
			System.out.println(p.getLazyRelation());
		}
	}

	@Test
	public void loadNestedCollection() {
		List<SamplePojo> response = sampleService.getList();
		for (SamplePojo p : response) {
			p.setLazyOn(false);
			Assert.assertNotNull(p.getLazyCollection().iterator());
		}
	}

	@Test
	public void shouldNotFailWithNullResponse() {
		SamplePojo response = sampleService.returnNull();
		Assert.assertNull(response);
	}
}
