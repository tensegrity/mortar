package org.mortar.annotation;

import java.util.List;

public interface SampleService {
	SamplePojo sampleOperationWithoutAnnotation();
	SamplePojo sampleOperation(String sampleArg);
	SamplePojo sampleOperation2();
	List<SamplePojo> getList();
	SamplePojo returnNull();
}
