package org.mortar.util.bean;

import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;
import org.mortar.util.bean.BeanUtils;

public class BeanUtilsTest {

	@Test
	public void simpleBean() {
		Properties props = BeanUtils.toProperties(new Object() {
			private int intAttribute = 3;

			public int getIntAttribute() {
				return intAttribute;
			}

			public void setIntAttribute(int intAttribute) {
				this.intAttribute = intAttribute;
			}
		});
		Assert.assertEquals("3", props.get("intAttribute"));
	}
	
	@Test
	public void shouldSkipTransient() {
		Properties props = BeanUtils.toProperties(new Object() {
			private int intAttribute = 3;
			private transient int transientAttribute = 4;

			public int getTransientAttribute() {
				return transientAttribute;
			}

			public void setTransientAttribute(int transientAttribute) {
				this.transientAttribute = transientAttribute;
			}

			public int getIntAttribute() {
				return intAttribute;
			}

			public void setIntAttribute(int intAttribute) {
				this.intAttribute = intAttribute;
			}
		});
		Assert.assertNull(props.get("transientAttribute"));
	}
	
	@Test
	public void inheritanceTest() {
		Properties props = BeanUtils.toProperties(new Child());
		Assert.assertEquals("3", props.get("intAttribute"));
	}
	
}

class Parent {
	private int intAttribute = 3;
	private int transientAttribute = 4;

	public int getTransientAttribute() {
		return transientAttribute;
	}

	public void setTransientAttribute(int transientAttribute) {
		this.transientAttribute = transientAttribute;
	}

	public int getIntAttribute() {
		return intAttribute;
	}

	public void setIntAttribute(int intAttribute) {
		this.intAttribute = intAttribute;
	}
}

class Child extends Parent {
	int childIntAttribute;

	public int getChildIntAttribute() {
		return childIntAttribute;
	}

	public void setChildIntAttribute(int childIntAttribute) {
		this.childIntAttribute = childIntAttribute;
	}
}
