package org.mortar.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;

public class ExceptionUtilsTest {

	@Test(expected=FileNotFoundException.class)
	public void test1() {
		exceptionThrower1();
	}
	
	@Test(expected=FileNotFoundException.class)
	public void test2() {
		exceptionThrower2();
	}
	
	public void exceptionThrower1() {
		try {
			FileInputStream fis = new FileInputStream("foobar");
		} catch (FileNotFoundException e) {
			throw ExceptionUtils.throwUnchecked(e);
		}
	}
	
	public void exceptionThrower2() {
		try {
			FileInputStream fis = new FileInputStream("foobar");
		} catch (FileNotFoundException e) {
			ExceptionUtils.throwUnchecked(e);
		}
	}
}
