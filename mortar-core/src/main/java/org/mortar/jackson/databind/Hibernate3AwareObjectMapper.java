package org.mortar.jackson.databind;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate3.Hibernate3Module;

/**
 * A Hibernate 4 aware object mapper to convert hibernate entities to JSON avoiding loading lazy relationships.
 * 
 * @author pakman
 * 
 */
public class Hibernate3AwareObjectMapper extends ObjectMapper {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 9078046297960380276L;

	@Autowired(required = false)
	private Hibernate3Module Hibernate3Module;

	public Hibernate3AwareObjectMapper() {
	}

	public void setPrettyPrint(boolean prettyPrint) {
		configure(SerializationFeature.INDENT_OUTPUT, prettyPrint);
	}

	public Hibernate3Module getHibernate3Module() {
		return Hibernate3Module;
	}

	public void setHibernate3Module(Hibernate3Module Hibernate3Module) {
		this.Hibernate3Module = Hibernate3Module;
	}

	@PostConstruct
	public void init() {
		if (this.Hibernate3Module == null) {
			logger.debug("No Hibernate3Module specified, using default");
			this.Hibernate3Module = new Hibernate3Module();
		}
		registerModule(this.Hibernate3Module);
		configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}

}
