package org.mortar.legacy.webservices;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.net.URL;

import org.apache.axis.client.Service;
import org.apache.axis.client.Stub;

/**
 * Esta clase sirve para consumir web services legacy (i.e. RPC/encoded de Axis) a partir de los
 * stubs generados por Axis.
 * 
 * @param <I>
 *            la interfase del webservice (generada por Axis)
 * @param <L>
 *            el serviceLocator de axis para el web service (generado por Axis)
 * 
 * @author Pablo Krause
 * 
 */
public abstract class AbstractWebServiceDelegate<I, L> extends BaseWebServiceDelegate {
	private Stub serviceStub;
	private org.apache.axis.client.Service serviceLocator;

	private enum GenericParam {
		I, L
	}

	@SuppressWarnings( { "unchecked" })
	private Class<I> getServiceInterface() {
		return (Class<I>) getGenericParameter(GenericParam.I.ordinal());
	}

	@SuppressWarnings( { "unchecked" })
	private Class<L> getLocatorType() {
		return (Class<L>) getGenericParameter(GenericParam.L.ordinal());
	}

	/**
	 * 
	 * @param index
	 *            0=I, 1=L
	 * @return
	 */
	private Class<?> getGenericParameter(int index) {
		return (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[index];
	}

	// public abstract Class<? extends Service> getServiceLocatorClass();

	/**
	 * Default constructor.
	 */
	public AbstractWebServiceDelegate() {
		try {
			// this.serviceLocator = (Service) getServiceLocatorClass().newInstance();
			this.serviceLocator = (Service) getLocatorType().newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructor que acepta un endpoint.
	 * 
	 * @param endpoint
	 *            el endpoint del web service
	 */
	public AbstractWebServiceDelegate(URL endpoint) {
		this();
		setEndpoint(endpoint);
	}

	@SuppressWarnings("unchecked")
	public I getServiceStub() {
		if (null == serviceStub) {
			Method[] methods = serviceLocator.getClass().getDeclaredMethods();
			for (Method m : methods) {
				if (m.getReturnType() == getServiceInterface() && m.getParameterTypes().length == 1
						&& m.getParameterTypes()[0] == URL.class) {
					try {
						serviceStub = (Stub) m.invoke(serviceLocator,
								new Object[] { getEndpoint() });
					} catch (InvocationTargetException e) {
						throw new RuntimeException(e);
					} catch (IllegalArgumentException e) {
						throw new RuntimeException(e);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					}
					serviceStub.setUsername(getUsername());
					serviceStub.setPassword(getPassword());
					break;
				}
			}
		}
		return (I) serviceStub;
	}

}
