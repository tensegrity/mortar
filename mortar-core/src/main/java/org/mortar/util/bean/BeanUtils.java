package org.mortar.util.bean;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import org.mortar.util.ExceptionUtils;

public final class BeanUtils {
	public final static Properties toProperties(Object javaBean) {
		return toProperties(javaBean, null);
	}

	public final static Properties toProperties(Object javaBean, String aPrefix) {
		Properties props = new Properties();
		String prefix = null == aPrefix ? "" : aPrefix + ".";

		Class clazz = javaBean.getClass();
		while (clazz != null && !clazz.equals(Object.class)) {
			for (Field field : clazz.getDeclaredFields()) {
				if (!Modifier.isTransient(field.getModifiers()) && !field.isSynthetic()) {
					try {
						field.setAccessible(true);
						props.setProperty(prefix + field.getName(), String.valueOf(field.get(javaBean)));
					} catch (IllegalArgumentException e) {
						throw ExceptionUtils.throwUnchecked(e);
					} catch (IllegalAccessException e) {
						throw ExceptionUtils.throwUnchecked(e);
					}
				}
			}
			clazz = clazz.getSuperclass();
		}
		return props;
	}
}
