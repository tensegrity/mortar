package org.mortar.util.bean;

import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;

/**
 * ConverterUtilBean to be used with apache commons Bean Utils for handling
 * enums.
 * 
 * Thanks to Jeremy Chone and "greg" Taken from
 * http://www.bitsandpix.com/entry/java
 * -beanutils-enum-support-generic-enum-converter/
 * 
 * Usage: org.apache.commons.beanutils.BeanUtilsBean beanUtilsBean = new
 * BeanUtilsBean(new EnumAwareConvertUtilsBean());
 * 
 * @author Pablo Krause (actually "greg")
 * 
 */
public class EnumAwareConvertUtilsBean extends ConvertUtilsBean {
	private final EnumConverter enumConverter = new EnumConverter();

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Converter lookup(Class clazz) {
		final Converter converter = super.lookup(clazz);
		// no specific converter for this class, so it's neither a String,
		// (which has a default converter), nor any known object that has a
		// custom converter for it. It might be an enum !
		if (converter == null && clazz.isEnum()) {
			return enumConverter;
		} else {
			return converter;
		}
	}

	/**
	 * 
	 */
	private class EnumConverter implements Converter {
		@SuppressWarnings("unchecked")
		public Object convert(Class type, Object value) {
			return Enum.valueOf(type, (String) value);
		}
	}
}
