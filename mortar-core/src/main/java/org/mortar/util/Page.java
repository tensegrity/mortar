/* Copyright GUCOBA Systems */

package org.mortar.util;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;


/**
 * 
 * @param <T>
 *            tipo de dato que contendrá la página
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 */
public class Page<T> implements Serializable {
	public static final int DEFAULT_PAGE_SIZE = 15;
	private static final long serialVersionUID = 1L;
	private List<T> elements;
	private int pageSize = DEFAULT_PAGE_SIZE;
	private int pageNumber;
	private int pageFirstElementNumber;
	private int totalElements = -1;
	private int numberOfPages;
	private boolean hasNextPage = false;

	/**
	 * Crea una nueva instancia utilizando el tamaño de página predeterminado.
	 * 
	 * @param elements
	 *            los elementos de la página
	 * @param firstRecord
	 *            el índice del primer elemento de la página
	 * @param totalElements
	 *            el número total de elementos
	 */
	public Page(List<T> elements, int firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * 
	 * @deprecated Utilizar el método {@link #Page(List, int, int)}
	 * @param elements
	 *            los elementos de la página
	 * @param firstRecord
	 *            el índice del primer elemento de la página
	 * @param totalElements
	 *            el número total de elementos
	 */
	@Deprecated
	public Page(List<T> elements, long firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see #Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la página
	 * @param first
	 *            el índice del primer elemento de la página
	 * @param aPageSize
	 *            el número de elementos por página
	 * @param total
	 *            el número total de elementos
	 */
	@Deprecated
	public Page(List<T> elements, long first, int aPageSize, long total) {
		this(elements, (int) first, aPageSize, (int) total);
	}

	/**
	 * Constructor principal.
	 * 
	 * @see #Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la página
	 * @param first
	 *            el índice del primer elemento de la página
	 * @param aPageSize
	 *            el número de elementos por página
	 * @param total
	 *            el número total de elementos
	 */
	public Page(List<T> elements, int first, int aPageSize, int total) {
		this.pageFirstElementNumber = first;
		this.pageSize = aPageSize;
		this.totalElements = total;
		if (null == elements) {
			// this.elements = Collections.EMPTY_LIST;
			this.elements = Collections.<T> emptyList();
		} else {
			this.elements = elements;
		}

		if (pageFirstElementNumber < 0) {
			// En lugar de error lo fijamos en la primera página
			pageFirstElementNumber = 0;
		}
		if (pageSize < 0) {
			// En lugar de dar error, lo fijamos al default
			pageSize = DEFAULT_PAGE_SIZE;
		}

		// Validamos que los valores de las variables de instancia hagan sentido.
		sanityCheck();

		if (pageFirstElementNumber == 0) {
			this.pageNumber = 0;
		} else {
			this.pageNumber = (int) (pageFirstElementNumber / pageSize);
		}
		if (totalElements < 0) {
			numberOfPages = -1;
		} else {
			numberOfPages = totalElements / pageSize;
			// La última páguina puede tener menos elementos que pageSize:
			numberOfPages += totalElements % pageSize == 0 ? 0 : 1;
		}
	}

	/**
	 * Determina si existen más páginas o se trata de la última página.
	 * 
	 * @return true si existen mas páginas, false si es la última página
	 */
	public boolean isHasNextPage() {
		if (getTotal() < 0) {
			return this.hasNextPage;
		} else {
			return !(getPageNumber() >= getNumberOfPages() - 1);
		}
	}

	public void setHasNextPage(boolean flag) {
		this.hasNextPage = flag;
	}

	public boolean isHasPreviousPage() {
		return getPageNumber() > 0;
	}

	/**
	 * Devuelve el contenido de la página.
	 * 
	 * @return los elementos de la página.
	 */
	public List<T> getElements() {
		return elements;
	}

	/**
	 * Devuelve el número total de elementos.
	 * 
	 * @return número total de elementos
	 */
	public int getTotal() {
		return totalElements;
	}

	/**
	 * @see #getTotal()
	 * 
	 * @deprecated utilizar getTotal() en lugar de este m&eacute;todo.
	 * @return número total de elementos
	 */
	@Deprecated
	public long getTotalNumberOfElements() {
		return totalElements;
	}

	public int getFirstElementIndex() {
		return pageFirstElementNumber;
	}

	/**
	 * @see #getFirstElementIndex()
	 * 
	 * @deprecated utilizar {@link #getFirstElementIndex()} en lugar de este m&eacute;todo.
	 * @return el índice del primer elemento de la página
	 */
	@Deprecated
	public long getPageFirstElementNumber() {
		return pageFirstElementNumber;
	}

	/**
	 * Devuelve el índice del último elemento de la página.
	 * 
	 * @return el índice del último elemento de la página
	 */
	public int getLastElementIndex() {
		return getFirstElementIndex() + getElements().size() - 1;
	}

	/**
	 * @deprecated utilizar {@link #getLastElementIndex()} en lugar de este m&eacute;todo.
	 * @return el índice del último elemento de la página
	 */
	@Deprecated
	public long getPageLastElementNumber() {
		return getPageFirstElementNumber() + getElements().size() - 1;
	}

	/**
	 * Devuelve el número de elementos que contiene cada página.
	 * 
	 * @return el tamaño de la página
	 */
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Devuelve el número de página que representa esta página. Depende del número total de
	 * registros y del tamaño de la página.
	 * 
	 * @return el n&uacute;mero de página
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * Devuelve la cantidad de de p&aacute;ginas totales.
	 * 
	 * @return número de páginas
	 */
	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 * Validamos que las variables de instancia hagan sentido.
	 */
	private void sanityCheck() {
		if (totalElements == 0 && null != elements && elements.size() != 0) {
			throw new RuntimeException(
					"The total number of elements cannot be 0 if the page contains elements");
		}
		if (elements == null & totalElements > 0) {
			throw new RuntimeException(
					"There cannot be an empty page if total number of elements is not 0");
		}
		if (elements != null && totalElements > 0 && totalElements < elements.size()) {
			throw new RuntimeException("Total number of elements cannot be less "
					+ "than the number of elements in the page.");
		}
		if (elements != null && pageSize < elements.size()) {
			throw new RuntimeException(
					"The page size cannot be less than the number of elements of the page!");
		}
		if (elements != null && elements.size() == 0 && totalElements > 0) {
			throw new RuntimeException(
					"There cannot be an empty page if total number of elements > 0");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Total elements=").append(getTotal() >= 0 ? getTotal() : "Unkown");
		sb.append("\nPage number=").append(getPageNumber());
		sb.append("\nPage size =").append(getPageSize());
		sb.append("\nActual page size=").append(getElements().size());
		sb.append("\nFirst element on page =").append(getFirstElementIndex());
		sb.append("\nLast element on page =").append(getLastElementIndex());
		sb.append("\nNumber of pages=").append(
				getNumberOfPages() >= 0 ? getNumberOfPages() : "Unkown");
		sb.append("\nHas next page=").append(this.isHasNextPage());
		return sb.toString();
	}

}
