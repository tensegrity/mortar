package org.mortar.util;

/**
 * 
 * 
 * 
 * @author krause
 * 
 */
public final class ExceptionUtils {
	private ExceptionUtils() {
	}

	/**
	 * Static method that simply re-throws the given (checked) exception as-is with the
	 * advantage that it will continue to propagate as an unchecked exception.  It kind
	 * of converts a checked exception into an unchecked exception without having to
	 * wrap it inside a RuntimeException.  The advantage of this is that you avoid having
	 * to litter your code with try/catch blocks that simply propagate the exception.
	 * The downside is that if you need to catch an exception somewhere you will have to 
	 * have a catch all block and determine the type of exception since the compiler will
	 * complain if you add a catch block of a checked exception wich is never declared as
	 * beign thrown.
	 * 
	 * Taken from http://www.gamlor.info/wordpress/?p=1020
	 * 
	 * @param ex The checked exception
	 */
	public final static RuntimeException throwUnchecked(final Exception e) {
		ExceptionUtils.<RuntimeException> throwsUnchecked(e);
		// This is here is only to satisfy the compiler. It's actually unreachable code!
        throw new AssertionError("This code should be unreachable. Something went terrible wrong here!");
	}

	/**
	 * Taken from http://www.gamlor.info/wordpress/?p=1020
	 * 
	 * @param <T>
	 * @param toThrow
	 * @throws T
	 */
	@SuppressWarnings("unchecked")
	private final static <T extends Exception> void throwsUnchecked(Exception toThrow) throws T {
		throw (T) toThrow;
	}
}