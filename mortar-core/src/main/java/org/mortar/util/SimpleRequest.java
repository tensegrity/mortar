package org.mortar.util;

import java.io.Serializable;

/**
 * 
 * @author krause
 *
 * @param <T>
 */
public class SimpleRequest<T> implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -6028752563885286318L;
	
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
