package org.mortar.util;

import java.io.Serializable;

public class SortInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1101979923749988225L;

	private String field;

	private SortDir dir;
	
	public SortInfo() {
		dir = SortDir.ASC;
	}
	
	public SortInfo(String field) {
		this(field, SortDir.ASC);
	}

	public SortInfo(String field, SortDir dir) {
		this.field = field;
		this.dir = dir;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public SortDir getDir() {
		return dir;
	}

	public void setDir(SortDir dir) {
		this.dir = dir;
	}

}
