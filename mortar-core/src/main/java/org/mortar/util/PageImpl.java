/* Copyright GUCOBA Systems */

package org.mortar.util;

import java.util.List;

/**
 * @author Pablo Krause
 * @deprecated Se debe usar la clase {@link Page} en lugar de PageImpl. 
 *             Esta clase se deja para no romper el código preexistente.
 * @see org.mortar.util.Page
 * @param <T>
 *            tipo de dato que contendrá la página
 */
@Deprecated
public class PageImpl<T> extends Page<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4049391415903451437L;

	/**
	 * Crea una nueva instancia utilizando el tamaño de página predeterminado.
	 * 
	 * @deprecated Utilizar el método {@link Page#Page(List, int, int)}
	 * @param elements
	 *            los elementos de la página
	 * @param firstRecord
	 *            el índice del primer elemento de la página
	 * @param totalElements
	 *            el número total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, int firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * 
	 * @deprecated Utilizar el método {@link Page#Page(List, int, int)}
	 * @param elements
	 *            los elementos de la página
	 * @param firstRecord
	 *            el índice del primer elemento de la página
	 * @param totalElements
	 *            el número total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, long firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * @deprecated Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see Page#Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la página
	 * @param first
	 *            el índice del primer elemento de la página
	 * @param aPageSize
	 *            el número de elementos por página
	 * @param total
	 *            el número total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, long first, int aPageSize, long total) {
		this(elements, (int) first, aPageSize, (int) total);
	}

	/**
	 * @deprecated Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see Page#Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la página
	 * @param first
	 *            el índice del primer elemento de la página
	 * @param aPageSize
	 *            el número de elementos por página
	 * @param total
	 *            el número total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, int first, int aPageSize, int total) {
		super(elements, first, aPageSize, total);
	}

}
