package org.mortar.util;

public enum SortDir {
	ASC, DESC
}
