package org.mortar.util.delegate;

public interface Delegate<O, I> {
	O run(I input);
}
