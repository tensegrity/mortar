package org.mortar.util.delegate;

/**
 * A class for creating simple {@link Delegate}s, which recieve only one argument.
 * <p>Usage:</p>
 * 
 * Define a delegate:
 * <pre>
		Delegate<Integer, String> myDelegate = new SimpleDelegate<Integer, String, DTest>(this) {
			&#064;Override
			public Integer run(String input) {
				return target.bar(input);
			}
		};
 * </pre> 
 * 
 * Invoke a delegate:
 * <pre>Integer result = myDelegate.run("Input");</pre>
 * 
 * If you need to mass more than one argument, you can declare the Input Type <code>I</code>
 * as Object[], but you will loose compile type checkings.
 * 
 * @author Pablo Krause
 *
 * @param <O> Output Type
 * @param <I> Input Type
 * @param <T> Type of the target object
 */
public abstract class SimpleDelegate<O, I, T> implements Delegate<O, I> {
	protected T target;

	/**
	 * Constructor used only if the delegate is going to call a static method
	 * and thus doesn't need a target object.
	 */
	public SimpleDelegate() {
	}

	/**
	 * Use this constructor to specify the target object whose method is going
	 * to be invoked by the delegate.
	 * 
	 * @param target
	 */
	public SimpleDelegate(T target) {
		this.target = target;
	}

}
