/* Copyright GUCOBA Systems */

package org.mortar.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public final class Banner {
	private static final Map<String, String[]> LETTER_MAP = new HashMap<String, String[]>();

	private static final String[] A = { "   #   ", "  # #  ", " #   # ", "#     #", "#     #",
			"#######", "#     #", "#     #" };
	private static final String[] B = { "###### ", "#     #", "#     #", "###### ", "#     #",
			"#     #", "#     #", "###### " };
	private static final String[] C = { " ######", "#      ", "#      ", "#      ", "#      ",
			"#      ", "#      ", " ######" };
	private static final String[] D = { "###### ", "#     #", "#     #", "#     #", "#     #",
			"#     #", "#     #", "###### " };
	private static final String[] E = { "#######", "#      ", "#      ", "#####  ", "#      ",
			"#      ", "#      ", "#######" };
	private static final String[] F = { "#######", "#      ", "#      ", "#####  ", "#      ",
			"#      ", "#      ", "#      " };
	private static final String[] G = { "#######", "#      ", "#      ", "#   ###", "#     #",
			"#     #", "#     #", "#######" };
	private static final String[] H = { "#     #", "#     #", "#     #", "#######", "#     #",
			"#     #", "#     #", "#     #" };
	private static final String[] I = { "###", " # ", " # ", " # ", " # ", " # ", " # ", "###" };
	private static final String[] J = { " ####  ", "    #  ", "    #  ", "    #  ", "    #  ",
			"    #  ", "    #  ", " ###   " };
	private static final String[] K = { "#     #", "#    # ", "#   #  ", "#  #   ", "# ##   ",
			"##  #  ", "#    # ", "#     #" };
	private static final String[] L = { "#      ", "#      ", "#      ", "#      ", "#      ",
			"#      ", "#      ", "#######" };
	private static final String[] M = { "#     #", "##   ##", "# # # #", "#  #  #", "#  #  #",
			"#     #", "#     #", "#     #" };
	private static final String[] N = { "#     #", "##    #", "# #   #", "#  #  #", "#  #  #",
			"#   # #", "#    ##", "#     #" };
	private static final String[] O = { " ##### ", "#     #", "#     #", "#     #", "#     #",
			"#     #", "#     #", " ##### " };
	private static final String[] P = { "###### ", "#     #", "#     #", "#     #", "###### ",
			"#      ", "#      ", "#      " };
	private static final String[] Q = { " ##### ", "#     #", "#     #", "#     #", "#  #  #",
			"#   # #", "#    # ", " #### #" };
	private static final String[] R = { "###### ", "#     #", "#     #", "#     #", "###### ",
			"#   #  ", "#    # ", "#     #" };
	private static final String[] S = { " ##### ", "#      ", "#      ", " ##### ", "      #",
			"      #", "      #", "###### " };
	private static final String[] T = { "#######", "   #   ", "   #   ", "   #   ", "   #   ",
			"   #   ", "   #   ", "   #   " };
	private static final String[] U = { "#     #", "#     #", "#     #", "#     #", "#     #",
			"#     #", "#     #", " ##### " };
	private static final String[] V = { "#     #", "#     #", "#     #", "#     #", "#     #",
			" #   # ", "  # #  ", "   #   " };
	private static final String[] W = { "#     #", "#     #", "#     #", "#     #", "#  #  #",
			"# # # #", "##   ##", "#     #" };
	private static final String[] X = { "#     #", " #   # ", "  # #  ", "   #   ", "   #   ",
			"  # #  ", " #   # ", "#     #" };
	private static final String[] Y = { "#     #", "#     #", "#     #", " #   # ", "  ###  ",
			"   #   ", "   #   ", "   #   " };
	private static final String[] Z = { "#######", "     # ", "    #  ", "   #   ", "  #    ",
			" #     ", "#      ", "#######" };
	private static final String[] N0 = { " ##### ", "##    #", "# #   #", "#  #  #", "#  #  #",
			"#   # #", "#    ##", " ##### " };
	private static final String[] N1 = { "  #", " ##", "# #", "  #", "  #", "  #", "  #", "  #" };
	private static final String[] N2 = { " ##### ", "#     #", "#     #", "     # ", "  ###  ",
			" #     ", "#      ", "#######" };
	private static final String[] N3 = { "###### ", "      #", "      #", "  #### ", "      #",
			"      #", "      #", "###### " };
	private static final String[] N4 = { "#      ", "#      ", "#   #  ", "#   #  ", "#######",
			"    #  ", "    #  ", "    #  " };
	private static final String[] N5 = { "###### ", "#      ", "#      ", "###### ", "      #",
			"      #", "      #", "###### " };
	private static final String[] N6 = { " ##### ", "#      ", "#      ", "###### ", "#     #",
			"#     #", "#     #", " ##### " };
	private static final String[] N7 = { "#######", "      #", "      #", "     # ", "    #  ",
			"    #  ", "    #  ", "    #  " };
	private static final String[] N8 = { " ##### ", "#     #", "#     #", " ##### ", "#     #",
			"#     #", "#     #", " ##### " };
	private static final String[] N9 = { " ##### ", "#     #", "#     #", " ######", "      #",
			"     # ", "    #  ", "   #   " };
	private static final String[] DASH = { "       ", "       ", "       ", " ##### ", "       ",
			"       ", "       ", "       " };

	static {
		LETTER_MAP.put("A", A);
		LETTER_MAP.put("B", B);
		LETTER_MAP.put("C", C);
		LETTER_MAP.put("D", D);
		LETTER_MAP.put("E", E);
		LETTER_MAP.put("F", F);
		LETTER_MAP.put("G", G);
		LETTER_MAP.put("H", H);
		LETTER_MAP.put("I", I);
		LETTER_MAP.put("J", J);
		LETTER_MAP.put("K", K);
		LETTER_MAP.put("L", L);
		LETTER_MAP.put("M", M);
		LETTER_MAP.put("N", N);
		LETTER_MAP.put("O", O);
		LETTER_MAP.put("P", P);
		LETTER_MAP.put("Q", Q);
		LETTER_MAP.put("R", R);
		LETTER_MAP.put("S", S);
		LETTER_MAP.put("T", T);
		LETTER_MAP.put("U", U);
		LETTER_MAP.put("V", V);
		LETTER_MAP.put("W", W);
		LETTER_MAP.put("X", X);
		LETTER_MAP.put("Y", Y);
		LETTER_MAP.put("Z", Z);
		LETTER_MAP.put("0", N0);
		LETTER_MAP.put("1", N1);
		LETTER_MAP.put("2", N2);
		LETTER_MAP.put("3", N3);
		LETTER_MAP.put("4", N4);
		LETTER_MAP.put("5", N5);
		LETTER_MAP.put("6", N6);
		LETTER_MAP.put("7", N7);
		LETTER_MAP.put("8", N8);
		LETTER_MAP.put("9", N9);
		LETTER_MAP.put("-", DASH);
	}

	/**
	 * Constructor para evitar instanciación de esta clase.
	 */
	private Banner() {

	}

	public static String generateBanner(String someText) {
		String text;
		if (null == someText) {
			text = "null";
		} else {
			text = someText.toUpperCase();
		}
		StringBuffer stringBuf = new StringBuffer(text.length() * 64);
		for (int row = 0; row < 8; row++) {
			for (int charcount = 0; charcount < text.length(); charcount++) {
				char c = text.charAt(charcount);
				String[] letter = LETTER_MAP.get(Character.toString(c));
				if (null != letter) {
					stringBuf.append(letter[row]);
				}
				stringBuf.append("  ");
			}
			stringBuf.append("\n");
		}
		return stringBuf.toString();
	}

}
