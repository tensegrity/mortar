/* Copyright GUCOBA Systems */

package org.mortar.report;

import org.springframework.web.servlet.mvc.Controller;

/**
 * @author Juvenal Guzmán powered by GUCOBA Systems S.C.
 * 
 */
public interface ReportsController extends Controller {
}
