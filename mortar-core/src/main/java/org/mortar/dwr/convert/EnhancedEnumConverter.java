package org.mortar.dwr.convert;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.directwebremoting.ConversionException;
import org.directwebremoting.convert.BeanConverter;
import org.directwebremoting.extend.InboundContext;
import org.directwebremoting.extend.InboundVariable;
import org.directwebremoting.extend.NonNestedOutboundVariable;
import org.directwebremoting.extend.OutboundContext;
import org.directwebremoting.extend.OutboundVariable;
import org.directwebremoting.util.LocalUtil;

/**
 * Enhanced Converter for Enums
 * 
 * @author Pablo Krause
 */
public class EnhancedEnumConverter extends BeanConverter {
//	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String NAME = "name";
	private static final String ORDINAL = "ordinal";

	/*
	 * Esta implementación acepta tanto un String, correspondiente al nombre del
	 * valor del enum como el EnumConverter original así como un objeto que
	 * contenga un atributo "name" equivalente al que regresa el
	 * convertOutbound. (non-Javadoc)
	 * 
	 * @see
	 * org.directwebremoting.extend.Converter#convertInbound(java.lang.Class,
	 * org.directwebremoting.extend.InboundVariable,
	 * org.directwebremoting.extend.InboundContext)
	 */
	@SuppressWarnings("unchecked")
	public Object convertInbound(Class<?> paramType, InboundVariable data,
			InboundContext inctx) throws ConversionException {
		Map enumMap = null;
		String name;
		try {
			// We first try to convert it to a map... although we only need the
			// name.
			enumMap = (Map) this.converterManager.convertInbound(Map.class, data, inctx.getCurrentProperty());
			name = (String) enumMap.get(NAME);
		} catch (Exception x) {
			// If convertion to a Map fails, we asume it is a String as expected
			// by the original EnumConverter.
			name = LocalUtil.urlDecode(data.getValue());
		}

		try {
			
			Method getter = paramType.getMethod("valueOf", String.class);
			Object reply = getter.invoke(paramType, name);
			if (reply == null) {
				throw new ConversionException(paramType);
			}
			return reply;
		} catch (NoSuchMethodException ex) {
			// We would like to have done: if (!paramType.isEnum())
			// But this catch block has the same effect
			throw new ConversionException(paramType, ex);
		} catch (Exception ex) {
			throw new ConversionException(paramType, ex);
		}
	}

	/*
	 * Si se trata de un enum básico (i.e. sin propiedades) esta implementación
	 * devuelve un String con el nombre del valor del enum, tal como la
	 * implementación original de EnumConverter. En caso de tratarse de un enum
	 * con propiedades adicionales este método devuelve un objeto con los
	 * atributos name y ordinal del enum y adicionalmente las propiedades
	 * adicionales que se encuentren. (non-Javadoc)
	 * 
	 * @see
	 * org.directwebremoting.extend.Converter#convertOutbound(java.lang.Object,
	 * org.directwebremoting.extend.OutboundContext)
	 */
	@SuppressWarnings("unchecked")
	public OutboundVariable convertOutbound(Object data, OutboundContext outctx)
			throws ConversionException {
		Map enumMap = null;
		Enum<?> enumData = null;

		if (!(data instanceof Enum)) {
			throw new ConversionException(data.getClass(),
					"Can only convert Enums");
		}

		// We'll dissassemble the enum and store its attributes in a Map that we
		// will later pass to the converter in order to generate the JSON
		enumData = (Enum<?>) data;
		enumMap = enum2Map(enumData);

		if (null == enumMap || 0 == enumMap.size()) {
			// Basic enum converter
			return new NonNestedOutboundVariable('\'' + enumData.name() + '\'');
		} else {
			// Enum with additional properties
			enumMap.put(NAME, enumData.name());
			enumMap.put(ORDINAL, enumData.ordinal());
			return this.converterManager.convertOutbound(enumMap, outctx);
		}
	}

	/**
	 * Convierte un enum a un Mapa de atributos.
	 * 
	 * @param enumData
	 *            el enum a convertir
	 * @return un Map con los atributos del enum
	 * @throws ConversionException
	 *             Si se genera algún error al convertir
	 */
	@SuppressWarnings("unchecked")
	private static Map enum2Map(Enum enumData) throws ConversionException {
		Map enumMap = new HashMap();
		try {
			BeanInfo bi = Introspector.getBeanInfo(enumData.getClass());
			for (PropertyDescriptor pd : bi.getPropertyDescriptors()) {
				if ("class".equals(pd.getName())
						|| "declaringClass".equals(pd.getName())) {
					// We skip class and declaringClass properties, we are only
					// interested in user defined properties.
					continue;
				}
				Object propertyValue = pd.getReadMethod().invoke(enumData);
				enumMap.put(pd.getName(), propertyValue);
			}
			return enumMap;
		} catch (Exception e) {
			throw new ConversionException(enumData.getClass(), e);
		}
	}

}
