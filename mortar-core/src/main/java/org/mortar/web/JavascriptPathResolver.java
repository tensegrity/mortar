package org.mortar.web;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to get the default javascript file for a given JSP in order to automatically
 * include it. It is used by the file defaultPrelude.jspf in web applications.
 */ 
public class JavascriptPathResolver {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Pattern patternObject;
	private String jsRoot = "/js";
	private String jspRoot = "/jsp";
	private String contextPath;
	private String defaultName = "index.js";
	private String pattern;

	public JavascriptPathResolver() {
		this("");
	}

	public JavascriptPathResolver(String theContextPath) {
		if (null == theContextPath || theContextPath.trim().equals("/")) {
			this.contextPath = "";
		} else {
			this.contextPath = theContextPath;
		}
		patternObject = Pattern.compile("^" + this.contextPath + jspRoot + "(.*/)?(\\w*)(\\.\\w*)?.*");
	}

	/**
	 * Given a uri for a jsp, this method returns the path to the javascript file automatically associated to it.
	 *  
	 * @param uri
	 * @return
	 */
	public String resolve(String uri) {
		StringBuilder defaultJS = new StringBuilder();

		Matcher matcher = patternObject.matcher(uri);

		if (!matcher.find()) {
			logger.debug("No match found, returning same uri");
			return uri;
		}
		String path = matcher.group(1);
		String file = matcher.group(2);
		String extension = matcher.group(3);
		if (logger.isDebugEnabled()) {
			logger.debug("g0={}", matcher.group(0));
			logger.debug("path(g1)={}", path);
			logger.debug("file(g2)={}", file);
			logger.debug("extension(g3)={}", extension);
		}
		if (null == extension || extension.trim().length() == 0) {
			defaultJS.append(jsRoot).append(path == null ? "/" : path).append(file == null || file.trim().length() == 0 ? "" : file + "/").append(defaultName);
		} else {
			defaultJS.append(jsRoot).append(path).append(file).append(".js");
		}
		return defaultJS.toString();
	}

	public String getJsRoot() {
		return jsRoot;
	}

	public void setJsRoot(String jsRoot) {
		this.jsRoot = jsRoot;
	}

	public String getJspRoot() {
		return jspRoot;
	}

	public void setJspRoot(String jspRoot) {
		this.jspRoot = jspRoot;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
