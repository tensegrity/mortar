package org.mortar.web.servlet.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortar.application.RuntimeExceptionWrapper;
import org.mortar.business.BusinessException;
import org.mortar.util.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;

/**
 * Any error generated at the JSON marshalling layer won't be cautght by the
 * default ExceptionHandler interceptor, so we use this instead.
 * 
 * @author pablo
 * 
 */
@SuppressWarnings("deprecation")
public class JsonHandlerExceptionResolver extends AbstractHandlerExceptionResolver {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
		RuntimeExceptionWrapper exceptionWrapper;
		
		if (e instanceof BusinessException) {
			logger.info(e.getMessage(), e);
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "{\"error\":\"" + e.getMessage() + "\"}");
			} catch (IOException x) {
				ExceptionUtils.throwUnchecked(x);
			}
			return new ModelAndView();
		}
		
		// For backward compatibility
		if (e instanceof RuntimeExceptionWrapper) {
			exceptionWrapper = ((RuntimeExceptionWrapper) e);
		} else {
			exceptionWrapper = new RuntimeExceptionWrapper(e);
		}
		String folio = exceptionWrapper.getFolio();
		logger.error("ERROR FOLIO #:" + folio, exceptionWrapper);
		try {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "{\"folio\":\"" + folio + "\", \"error\":\"" + e.getMessage() + "\"}");
		} catch (IOException x) {
			ExceptionUtils.throwUnchecked(x);
		}
		return new ModelAndView();
	}

}
