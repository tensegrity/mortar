package org.mortar.web.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleHttpProxyServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7461343419491606539L;
	private Log logger = LogFactory.getLog(this.getClass());

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String targetURI = request.getParameter("url");
		doGetPost(request, response, new GetMethod(targetURI));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String targetURI = request.getParameter("url");
		PostMethod method = new PostMethod(targetURI);
		// Iterator paramIterator = request.getParameterMap().entrySet().iterator();
		// logger.debug("Request parameters:");
		// while (paramIterator.hasNext()) {
		// Entry param = (Entry) paramIterator.next();
		// String paramKey = (String) param.getKey();
		// if (param.getValue() instanceof String) {
		// method.addParameter(paramKey, (String) param.getValue());
		// logger.debug(paramKey + "=" + param.getValue());
		// } else {
		// for (String v : (String[]) param.getValue()) {
		// method.addParameter(paramKey, v);
		// logger.debug(paramKey + "=" + v);
		// }
		// }
		// }
		// method.setRequestEntity(new
		// StringRequestEntity("<wfs:Transaction xmlns:wfs=\"http://www.opengis.net/wfs\" service=\"WFS\" version=\"1.0.0\" xsi:schemaLocation=\"http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-transaction.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><wfs:Insert><wfs:null><wfs:the_geom><gml:Polygon xmlns:gml=\"http://www.opengis.net/gml\" srsName=\"EPSG:32614\"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates decimal=\".\" cs=\",\" ts=\" \">483381.15234375,2157278.3203125 483445.60546875,2157256.8359375 483453.41796875,2157296.875 483381.15234375,2157278.3203125</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></wfs:the_geom></wfs:null></wfs:Insert></wfs:Transaction>",
		// request.getContentType(), null));
		method.setRequestEntity(new InputStreamRequestEntity(request.getInputStream()));
		doGetPost(request, response, method);
	}

	protected void doGetPost(HttpServletRequest request, HttpServletResponse response,
			HttpMethod method) throws ServletException, IOException {
		InputStream in = null;
		OutputStream out = response.getOutputStream();
		try {
			HttpClient httpclient = new HttpClient();
			Enumeration e = request.getHeaderNames();
			logger.debug("Request headers:");
			while (e.hasMoreElements()) {
				String h = (String) e.nextElement();
				method.addRequestHeader(h, request.getHeader(h));
				logger.debug(h + "=" + request.getHeader(h));
			}
			int rc = httpclient.executeMethod(method);
			logger.debug("Response code=" + rc);
			in = method.getResponseBodyAsStream();

			logger.debug("Remote response headers:");

			Header[] headers = method.getResponseHeaders();
			for (int i = 0; i < headers.length; i++) {
				response.setHeader(headers[i].getName(), headers[i].getValue());
				logger.debug(headers[i].getName() + "=" + headers[i].getValue());
			}
			byte[] buffer = new byte[1024];
			int read = 0;
			// String cType = "text/xml;charset=" + "iso-8859-1";
			while (true) {
				read = in.read(buffer);
				if (read <= 0) {
					break;
				}
				out.write(buffer, 0, read);
			}
		} catch (Exception x) {
			logger.error("Error inesperado en el proxy", x);
		} finally {
			IOUtils.closeQuietly(in);
			if (out != null) {
				out.flush();
				IOUtils.closeQuietly(out);
			}
			IOUtils.closeQuietly(in);
			method.releaseConnection();
		}
	}

}
