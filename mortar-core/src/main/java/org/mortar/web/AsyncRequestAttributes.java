package org.mortar.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.context.request.AbstractRequestAttributes;

/**
 * Implementación de la interfase RequestAttributes que utiliza un Map para almacenar los atributos.
 * Esta clase fue desarrollada para soportar la funcionalidad de invocaciones asíncronas mediante el
 * AsyncServiceInterceptor y con el fin de pasar los atributos con scope="request" de un lado al
 * otro del MOM.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class AsyncRequestAttributes extends AbstractRequestAttributes {
	private Map<String, Serializable> attributes;

	public AsyncRequestAttributes() {
		this.attributes = new HashMap<String, Serializable>();
	}

	public AsyncRequestAttributes(Map<String, Serializable> attributes) {
		this.attributes = attributes;
	}

	@Override
	protected void updateAccessedSessionAttributes() {
		// no hacemos nada... no manejamos sesión
	}

	public Object getAttribute(String name, int scope) {
		if (scope == SCOPE_REQUEST) {
			return this.attributes.get(name);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public String[] getAttributeNames(int arg0) {
		return (String[]) attributes.keySet().toArray(new String[attributes.size()]);
	}

	public String getSessionId() {
		throw new UnsupportedOperationException();
	}

	public Object getSessionMutex() {
		throw new UnsupportedOperationException();
	}

	public void registerDestructionCallback(String name, Runnable callback, int scope) {
		if (scope == SCOPE_REQUEST) {
			registerRequestDestructionCallback(name, callback);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public void removeAttribute(String name, int scope) {
		if (scope == SCOPE_REQUEST) {
			this.attributes.remove(name);
			removeRequestDestructionCallback(name);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public void setAttribute(String name, Object value, int scope) {
		if (scope == SCOPE_REQUEST) {
			this.attributes.put(name, (Serializable) value);
		} else {
			throw new UnsupportedOperationException();
		}
	}

  /* (non-Javadoc)
   * @see org.springframework.web.context.request.RequestAttributes#resolveReference(java.lang.String)
   */
  public Object resolveReference( String arg0 )
  {
    // TODO Auto-generated method stub
    return null;
  }

}
