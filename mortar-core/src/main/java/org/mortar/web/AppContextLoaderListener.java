/* Copyright GUCOBA Systems */

package org.mortar.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortar.util.Banner;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.util.Log4jWebConfigurer;

/**
 * ContextLoaderListener que carga configuración de Log4J delegando a la clase
 * org.springframework.web.util.Log4jWebConfigurer) y posteriormente carga el contexto de la
 * aplicación, delegando a la clase org.springframework.web.context.ContextLoaderListener.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class AppContextLoaderListener extends ContextLoaderListener {
	protected static final Log LOG = LogFactory.getLog(AppContextLoaderListener.class);

	/**
	 * Si no se especifica (mediante el parametro de contexto 'profileKey' en el archivo web.xml, se
	 * asume que el perfil de arranque de la aplicación está dada por el parámetro o variable de
	 * entorno con el nombre definido por DEFAULT_PROFILE_KEY, es decir APP_PROFILE.
	 */
	public static final String DEFAULT_PROFILE_KEY = "APP_PROFILE";

	public static final String DEFAULT_PROFILE = "production";
	
	/**
	 * Si no se desea usar APP_PROFILE como nombre para especificar el perfil de arranque se puede
	 * cambiar fijando un parámetro de context en web.xml, por ejemplo:
	 * 
	 * <pre>
	 *  &lt;context-param&gt;
	 *    &lt;param-name&gt;profileKey&lt;/param-name&gt;
	 *     &lt;para m-value&gt;perfil_arranque&lt;/param-value&gt;
	 *  &lt;/context-param&gt;
	 * </pre>
	 */
	protected static final String PROFILE_KEY_PARAM = "profileKey";


	/**
	 * Para cambiar el valor del perfil de arranque por defecto (i.e. cuando no se
	 * especifica mediante la variable de entorno o bien la propiedad de sistema).
	 * 
	 * <pre>
	 *  &lt;context-param&gt;
	 *    &lt;param-name&gt;defaultProfile&lt;/param-name&gt;
	 *     &lt;para m-value&gt;development&lt;/param-value&gt;
	 *  &lt;/context-param&gt;
	 * </pre>
	 */
	protected static final String PROFILE_VALUE_PARAM = "defaultProfile";
	
	public static final String JAVASCRIPT_RESOLVER_KEY = "mortar.javascriptPathResolver";
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext servletContext = event.getServletContext();
		String profileKey = servletContext.getInitParameter(PROFILE_KEY_PARAM);
		if (profileKey == null) {
			profileKey = DEFAULT_PROFILE_KEY;
		}

		String paramValue = System.getProperty(profileKey);
		String envValue = System.getenv(profileKey);
		String profileValue = servletContext.getInitParameter(PROFILE_VALUE_PARAM);

		if (null == paramValue) {
			if (null == envValue) {
				if (null != profileValue) {
					LOG.info("Using default profile '" + profileValue + "'.");
					System.setProperty(profileKey, profileValue);
				} else {
					profileValue = DEFAULT_PROFILE;
					LOG.warn("No profile specified, using default profile '" + profileValue + "'.");
					System.setProperty(profileKey, profileValue);
				}
			} else {
				profileValue = envValue;
				// Fijamos como propiedad de sistema el valor de la variable de entorno.
				System.setProperty(profileKey, profileValue);
				LOG.info("Using profile " + profileValue + " from environment variable.");
			}
		} else if (null == envValue) {
			profileValue = paramValue;
			LOG.info("Using profile '" + profileValue + "' from system parameter.");
		} else {
			profileValue = paramValue;
			// Si tanto el parámetro como la varialbe de entorno están definidas generamos una advertencia.
			if (profileValue.equals(envValue)) {
				LOG.info("Using profile '" + profileValue + "' from system parameter.");
			} else {
				LOG.warn("Using profile '" + profileValue + "' from system property, overriding profile '" + envValue + "' from environment variable.");
			}
		}

		// Lets set the profile value as a web application-wide attribute
		servletContext.setAttribute( profileKey, profileValue );
		
		try {
			// Primero hay que inicializar log4j
			Log4jWebConfigurer.initLogging(servletContext);
			// Ahora si, inicializamos el contexto de la aplicacion
			super.contextInitialized(event);
		} catch (RuntimeException e) {
			LOG.error("\n" + Banner.generateBanner("ERROR"));
			throw e;
		}
		servletContext.setAttribute(JAVASCRIPT_RESOLVER_KEY, new JavascriptPathResolver(servletContext.getContextPath()));
//		LOG.info("Profile:\n" + Banner.generateBanner(profileValue));
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);
		Log4jWebConfigurer.shutdownLogging(event.getServletContext());
	}

}
