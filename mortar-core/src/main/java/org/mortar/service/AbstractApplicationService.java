/* Copyright GUCOBA Systems */

package org.mortar.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.mortar.application.GraphDefinition;
import org.mortar.application.Session;
import org.mortar.service.upload.UploadFileManager;
import org.mortar.util.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public abstract class AbstractApplicationService implements ApplicationService {
	/** Log disponible para subclases */
	protected final Log log = LogFactory.getLog(getClass());
	/* Este loger no es static para que cada subclase tenga su propio logger */

	protected List<GraphDefinition> graphDefinitionList = new ArrayList<GraphDefinition>(0);

	/**
	 * En caso que el ApplicationService requiera hacer uso de la sesión, es necesario inyectarle
	 * una referencia a un bean de sessión. En caso de Spring, se debe de inyectar una referencia de
	 * un bean como el que se muestra:
	 * 
	 * <pre>
	 * &lt;bean id=&quot;session&quot; scope=&quot;session&quot; 
	 *       class=&quot;org.mortar.application.SessionImpl&quot;&gt;
	 *    &lt;aop:scoped-proxy /&gt;
	 * &lt;/bean&gt;
	 * </pre>
	 * 
	 * Para mayor referencias consultar sección "3.4.4.5. Scoped beans as dependencies" de la
	 * documentación de Spring.
	 */
	@Autowired(required=false)
	protected Session session;

	/**
	 * Usar este método para cargar únicamente datos de la entidad sin ninguna relación lazy
	 * adicional.
	 * 
	 * @param entity
	 *            la entidad a cargar o inicializar
	 */
	protected void loadEntity(Object entity) {
		// TODO Find a way to load a proxied entity in a Hibernate independent way.
		Hibernate.initialize(entity);
	}

	/**
	 * Carga o inicializa las relaciones lazy especificadas mediante un GraphDefinition. Hibernate
	 * maneja el concepto de lazy loading por lo que antes de regresar la entidad al front-end es
	 * necesario asegurarse de que todas las relaciones necesarias se encuentren cargadas.
	 * 
	 * @param entity
	 *            la entidad a cargar o inicializar
	 * @param graphDef
	 *            el grafo que define que relaciones se quieren cargar
	 */
	protected void loadEntityGraph(Object entity, GraphDefinition graphDef) {
		if (null == entity) {
			log.warn("Se trato de cargar una entidad pero era null.");
			return;
		}
		if (null != graphDef) {
			graphDef.load(entity);
		} else {
			// Si no mandan un grafphDef, simplemente nos aseguramos que la
			// entidad está cargada.
			loadEntity(entity);
		}
	}

	/**
	 * 
	 * Carga el grafo de los objetos de una lista según la definición del graphDef. Se debe de tener
	 * cuidad de verificar que esta es la mejor opción para cargar los datos y no un query que
	 * traiga todo de un jalón.
	 * 
	 * @param entityList
	 *            lista de entidades a cargar
	 * @param graphDef
	 *            el grafo que define que relaciones se quieren cargar
	 */
	protected void loadEntityGraph(List<? extends Object> entityList, GraphDefinition graphDef) {
		if (null != entityList) {
			// Si nos mandan null, simplemente no hacemos nada
			for (Object entity : entityList) {
				loadEntityGraph(entity, graphDef);
			}
		}
	}

	// Accessors

	public List<GraphDefinition> getGraphDefinitionList() {
		return graphDefinitionList;
	}

	public void setGraphDefinitionList(List<GraphDefinition> aGraphDefinitionList) {
		this.graphDefinitionList = aGraphDefinitionList;
	}

	/**
	 * 
	 * 
	 * @return el objeto de Sesion, en caso de existir
	 * @see AbstractApplicationService#session
	 */
	public Session getSession() {
		if (null == session) {
			log.warn("Se invoco al metodo getSession(), pero nunca se inyectó"
					+ " una referencia al objeto de sesión de la aplicación.");
		}
		return session;
	}

	/**
	 * 
	 * @param aSession
	 *            El objeto de sesion, el cual debe ser un scoped-proxy de Spring
	 * @see AbstractApplicationService#session
	 */
	public void setSession(Session aSession) {
		this.session = aSession;
	}

	/**
	 * Copia las propiedades (i.e. atributos) de una clase origen a una clase destino. Internamente
	 * hace uso de la clase BeanUtils de Jakarta Commons, pero envolviendo las posibles excepciones
	 * en excepciones de tipo RuntimeExceptionWrapper.
	 * 
	 * @param dest
	 *            Objeto destino
	 * @param orig
	 *            Objeto fuente
	 */
	protected void copyProperties(Object dest, Object orig) {
		try {
			BeanUtils.copyProperties(dest, orig);
		} catch (IllegalAccessException e) {
			log.error("Error al copiar las propiedades de un objeto a otro.", e);
			throw ExceptionUtils.throwUnchecked(e);
		} catch (InvocationTargetException e) {
			log.error("Error al copiar las propiedades de un objeto a otro.", e);
			throw ExceptionUtils.throwUnchecked(e);
		}
	}
	
	/**
	 * Obtiene el manejador de cargas de archivos de la sesión.
	 * 
	 * @return UploadFileManager manejador de cargas de archivos
	 */
	protected UploadFileManager getUploadFileManger() {
		return (UploadFileManager)session.getAttribute("_UPLOAD_FILE_MANAGER");
	}
}
