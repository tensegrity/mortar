/* Copyright GUCOBA Systems */

package org.mortar.service;

/**
 * Todos los servicios que sean expuestos al front-end deben de implementar esta interfase.
 * 
 * @see AbstractApplicationService
 * 
 * @author Pablo Krause
 *
 */
public interface ApplicationService {

}
