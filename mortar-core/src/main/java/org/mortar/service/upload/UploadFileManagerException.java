package org.mortar.service.upload;

/**
 * @author Juvenal Guzmán
 *
 */
public class UploadFileManagerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public UploadFileManagerException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UploadFileManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UploadFileManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UploadFileManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
