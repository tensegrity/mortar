package org.mortar.service.upload;

public class FileProcessMultipartException extends Exception {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 6002307876856309393L;

	public FileProcessMultipartException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
