package org.mortar.service.upload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UploadFileManagerImpl implements Serializable, UploadFileManager {
	private static final Log LOG = LogFactory.getLog(UploadFileManagerImpl.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Map<String, FileItemDefinition> files = new HashMap<String, FileItemDefinition>();

	
	/**
	 * {@inheritDoc}
	 */
	public String addFile(FileItemDefinition file) {
		LOG.debug("Adding file to upload file manager [" +  file.getFileName() + "]");
		String key = new StringBuffer().append(System.currentTimeMillis()).append("-").append(file.hashCode()).toString();
		this.files.put(key, file);
		return key;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public byte[] getUploadedFile(String key, boolean delete) {
		byte[] data = new byte[] {};
		FileItemDefinition file = getFileDefinition(key);
		if (file != null) {
			data = getBytesFromFile(file.getPath(), file.getFileName(), file.getFileId(), delete);
		}
		return data;
	}
	
	private FileItemDefinition getFileDefinition(String key) {
		FileItemDefinition file = files.get(key);
		if (file == null) {
			//Intentamos buscar por el nombre del campo...
			for(Entry<String, FileItemDefinition> entry: files.entrySet()) {
				if (entry.getValue().getFieldName().equals(key)) {
					file = entry.getValue();
					break;
				}
			}
		}
		return file;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public byte[] getUploadedFile(String key) {
		return getUploadedFile(key, true);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public FileItemDefinition getUploadedFileDefinition(String key) {
		return files.get(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	private byte[] getBytesFromFile(String path, String filename, String key, boolean delete) {
		LOG.debug("Reading content from file [" + filename + "]");
		byte[] bytes;
		File file = new File(path + "/" + filename);
		InputStream fis;
		try {
			fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bytes = new byte[bis.available()];
			bis.read(bytes);
			bis.close();
			fis.close();
			if (delete) {
				deleteFileAndDirectory(path, filename);
				files.remove(key);
			}
		} catch (Exception e) {
			LOG.debug("Error reading content from file [" + filename +"]", e);
			throw new UploadFileManagerException("Error reading content from file [" + filename + "]",e);
		}
		return bytes;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void deleteUploadedFile(String key) {
		FileItemDefinition fileDef = files.get(key);
		if (fileDef != null) {
			deleteFileAndDirectory(fileDef.getPath(), fileDef.getFileName());
			files.remove(key);
		} else {
			LOG.error("No file item definition with key [" + key + "] was found!");
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void dispose() {
		//TODO: Disposing the object sould delete al the register files...
		LOG.debug("Disposing upload file manager for current session...");
	}
	
	/**
	 * {@inheritDoc}
	 */
	void deleteFileAndDirectory(String path, String filename) {
		File file = new File(path + "/" + filename);
		File directory = new File(path);
		deleteFile(file);
		if (directory.listFiles().length == 0) {
			deleteDir(directory);
		}
	}
	
	private void deleteFile(File file) {
		if(!file.delete()) {
			LOG.error("Error deleting file!");
		}
	}
	
	private void deleteDir(File file) {
		if(!file.delete()) {
			LOG.error("Error deleting directory!");
		}		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void flushAll() {
		for (FileItemDefinition file : files.values()) {
			deleteFileAndDirectory(file.getPath(), file.getFileName());
		}
		files.clear();
	}
}
