package org.mortar.service.upload;

/**
 * Interfase que define las operaciones para el manejo de archivos que son 
 * transmitidos ("subidos") por el cliente desde el navegador.
 *  
 * @author Juvenal Guzmán
 *
 */
public interface UploadFileManager {

	/**
	 * TODO: documentación pendiente
	 * 
	 * @param file
	 * @return
	 */
	public abstract String addFile(FileItemDefinition file);

	/**
	 * TODO: documentación pendiente
	 * 
	 * @param key
	 * @param delete
	 * @return
	 * @throws UploadFileManagerException
	 */
	public abstract byte[] getUploadedFile(String key, boolean delete) throws UploadFileManagerException;

	/**
	 * TODO: documentación pendiente
	 * 
	 * @param key
	 * @return
	 * @throws UploadFileManagerException
	 */
	public abstract byte[] getUploadedFile(String key) throws UploadFileManagerException;

	/**
	 * TODO: documentación pendiente
	 * 
	 * @param key
	 */
	public abstract void deleteUploadedFile(String key);

	/**
	 * TODO: documentación pendiente
	 * 
	 */
	public void dispose();

	/**
	 * TODO: documentación pendiente
	 * 
	 */
	public void flushAll();

	/**
	 * TODO: documentación pendiente
	 * 
	 * @param key
	 * @return
	 */
	public FileItemDefinition getUploadedFileDefinition(String key);

}
