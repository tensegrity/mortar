package org.mortar.service.upload;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortar.application.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class UploadManagerController extends MultiActionController {
	private static final int OUTPUT_BYTE_ARRAY_INITIAL_SIZE = 4096;
	private static final Log LOGGER = LogFactory.getLog(UploadManagerController.class);
	
	private String repositoryName = System.getProperty("java.io.tmpdir");
	
	@Autowired
	private Session session;
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		LOGGER.debug("Processing uploading action, request to UploadManagerController!...");
		processAction(request, response);
		return null;
	}

	private void processAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Processing uploading action...");
		String responseData = getDataResponse(request, response);
		if (responseData != null) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println(responseData);
			out.flush();
			out.close();
		}
	}
	
	private String getDataResponse(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer jsonResponse = new StringBuffer();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		boolean processJSON = true;
		List<FileItemDefinition> files = new ArrayList<FileItemDefinition>();
		if (isMultipart) {
			LOGGER.debug("It's a multipart content request...");
			try {
				files = processMultipart(request);
			} catch (FileProcessMultipartException e) {
				jsonResponse.append("{success:false, error:'");
				jsonResponse.append(e.getMessage());
				jsonResponse.append("'}");
			}
		} else {
			processJSON = proccessRequest(request, response);
		}
		
		if (processJSON) {
			String comma = "";
			jsonResponse.append("{success:true, files: [");
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setJsonPropertyFilter(new PropertyFilter() {
				public boolean apply(Object source, String name, Object value) {
					if (name.equals("path")) {
						return true;
					}
					return false;
				}
			});  		
			for (FileItemDefinition file : files) {
				jsonResponse.append(comma);
				jsonResponse.append(JSONObject.fromObject(file, jsonConfig).toString());
				comma = ",";
			}
			jsonResponse.append("]}");
			return jsonResponse.toString();
		} else {
			return null;
		}
	}

	private boolean proccessRequest(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.debug("No multipart content was found...");
		String action = request.getParameter("_action");
		if (action != null && action.equals("deleteUploadedFile")){
			LOGGER.debug("Delete uploaded file action request.");
			String fileId = request.getParameter("_fileId");
			if (fileId != null && (!fileId.equals(""))) {
				LOGGER.debug("Deleting file with id [" + fileId + "]");
				getFileManager().deleteUploadedFile(fileId);
			}
		} else if (action != null && action.equals("flushAllUploadedFiles")){
			LOGGER.debug("Flush all files in upload manager.");
			getFileManager().flushAll();
		} else if (action != null && action.equals("renderUploadedFile")){
			String fileId = request.getParameter("_fileId");
			if (fileId != null && (!fileId.equals(""))) {
				String contentType = getFileManager().getUploadedFileDefinition(fileId).getContentType();
				LOGGER.debug("Render uploaded file [" + fileId + "].");
				ByteArrayOutputStream data = new ByteArrayOutputStream(OUTPUT_BYTE_ARRAY_INITIAL_SIZE);
				try {
					data.write(getFileManager().getUploadedFile(fileId, false));
				} catch (UploadFileManagerException e) {
					LOGGER.error("Error getting uploaded file...");
				} catch (IOException ioe) {
					LOGGER.error("Error getting uploaded file...", ioe);
				}
				response.setContentType(contentType);
				response.setContentLength(data.size());
				response.setHeader("pragma", "no-cache");
				response.setHeader("cache-control", "no-cache");
				response.setHeader("expires", "0");	
				OutputStream os;
				try {
					os = response.getOutputStream();
					data.writeTo(os);
					os.flush();
					//os.close();				
				} catch (IOException e) {
					LOGGER.error("Error rendering content from uploaded file!", e);
				}
				return false;
			}			
			
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private List<FileItemDefinition> processMultipart(HttpServletRequest request) throws FileProcessMultipartException {
		
		final String sessionId = new StringBuffer().append(System.currentTimeMillis()).append("-").append(this.hashCode()).toString();
		
		UploadFileManager uploadFileManager = getFileManager();
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();

		LOGGER.debug("Setting repository for temp upload to [" + repositoryName + "]");
		// Set factory constraints
		factory.setRepository(new File(repositoryName));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		repositoryName = repositoryName.replaceAll("\\\\", "/");
		if (repositoryName.indexOf(":") > -1) {
			repositoryName = repositoryName.substring(repositoryName.indexOf(":") + 1);
		}
		
		if(!repositoryName.endsWith("/")) {
			repositoryName = repositoryName + "/";
		}
		
		List<FileItemDefinition> fileItemsDefinition = new ArrayList<FileItemDefinition>();
		// Parse the request
		List<FileItem> items = null;
		try {
			LOGGER.debug("Parsing for upload files in request...");
			items = upload.parseRequest(request);
		} catch (FileUploadException e) {
			LOGGER.error("Ocurrio un error al procesar los archivos de upload.",e);
			throw new FileProcessMultipartException("Ocurrio un error al procesar los archivos de upload", e);
		}
		
		if (items != null && items.size() > 0) {
			LOGGER.debug("Found [" + items.size() + "] files, procesing...");
			// Process the uploaded items
			String action = getSourceFormFiles(items);
			Iterator<FileItem> iter = items.iterator();				
			while (iter.hasNext()) {
			    FileItem item = (FileItem) iter.next();

			    if (item.isFormField()) {
			        LOGGER.info("[Multipart] isFormField, name=" + item.getFieldName() + ", value=" + item.getString());
			    } else {
			    	LOGGER.info("[Multipart] not isFormField, fieldName=" + item.getFieldName() + ", fileName=" + item.getName() + ", contentType=" + item.getContentType()+ ", isInMemory=" + item.isInMemory() + ", sizeInBytes=" + item.getSize());
			    	LOGGER.debug("The action is : " + action);
			        if ((item.getName() != null && !item.getName().equals("")) && item.getSize() > 0) {
			        	FileItemDefinition fileDefinition = new FileItemDefinition(item.getFieldName(), item.getName(), item.getContentType(), item.getSize());
			        	
			        	String uploadRepository = repositoryName + "uploads/" + action + "/" + sessionId;
			        	
			        	fileDefinition.setPath(uploadRepository);
			        	fileDefinition.setAction(action);
			        	fileDefinition.setFileId(uploadFileManager.addFile(fileDefinition));

			        	File uploadedFile = new File(uploadRepository + "/" + fileDefinition.getFileName());
			        	File uploadedDir = new File(uploadRepository);
			        	
			        	LOGGER.debug("Creating temporal upload repository [" + uploadRepository + "]");
				        if (!uploadedDir.exists()) {
				        	if (!uploadedDir.mkdirs()) {
				        		String error = "Error al crear el directorio temporal para la carga de archivos, " + uploadRepository;
				        		LOGGER.error(error);
				        		throw new FileProcessMultipartException(error, null);
				        	}
				        }
				        try {
				        	LOGGER.debug("Writing file [" + fileDefinition.getFileName() + "] to temporal repository...");
							item.write(uploadedFile);
						} catch (Exception e) {
							LOGGER.error("Error al salvar el archivo al repositorio de carga de archivos del servidor.", e);
							fileDefinition.setErrorDetail("error: " + e.getMessage());
						}
						fileItemsDefinition.add(fileDefinition);
			        }
			    }
			}

		}
		return fileItemsDefinition;
	}

	private String getSourceFormFiles(List<FileItem> items) {
		String source = "noAction";
		for (FileItem item : items) {
			if (item.isFormField()) {
		        if (item.getFieldName().equals("source")) {
		        	source = item.getString();
		        	break;
		        }
			}
		}
		return source;
	}
	private UploadFileManager getFileManager() {
		UploadFileManager fileManager = (UploadFileManager)session.getAttribute("_UPLOAD_FILE_MANAGER");
		if (fileManager == null) {
			fileManager = new UploadFileManagerImpl();
			session.setAttribute("_UPLOAD_FILE_MANAGER", fileManager);
		}
		return fileManager;
	}
	
	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
