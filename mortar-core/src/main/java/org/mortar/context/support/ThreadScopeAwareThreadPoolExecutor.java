package org.mortar.context.support;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * A {@link java.util.concurrent.ThreadPoolExecutor} that sets and cleans thread
 * scoped attributes before/after executing a task in a thread.
 * 
 * @author krause
 * 
 */
public class ThreadScopeAwareThreadPoolExecutor extends ThreadPoolExecutor {

	public ThreadScopeAwareThreadPoolExecutor(int corePoolSize,
			int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				handler);
	}

	public ThreadScopeAwareThreadPoolExecutor(int corePoolSize,
			int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,
			RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory, handler);
	}

	public ThreadScopeAwareThreadPoolExecutor(int corePoolSize,
			int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory);
	}

	public ThreadScopeAwareThreadPoolExecutor(int corePoolSize,
			int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	/**
	 * Binds the <code>ThreadAttributes</code> to the thread, via
	 * ThreadContextHolder.
	 */
	@Override
	protected void beforeExecute(Thread paramThread, Runnable paramRunnable) {
		ThreadAttributes attributes = new ThreadAttributesImpl();
		ThreadContextHolder.setAttributes(attributes);
	}

	/**
	 * Removesthe <code>ThreadAttributes</code> from the thread, and calls the
	 * destruction callbacks
	 */
	@Override
	protected void afterExecute(Runnable paramRunnable, Throwable paramThrowable) {
		ThreadAttributes attributes = ThreadContextHolder.getAttributes();
		ThreadContextHolder.resetAttributes();
		if (attributes != null) {
			attributes.workCompleted();
		}
	}

}
