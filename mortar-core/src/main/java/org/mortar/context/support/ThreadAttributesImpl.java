package org.mortar.context.support;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * Thread scope attributes.
 *
 * @see org.springframework.web.context.request.AbstractRequestAttributes
 * @see org.springframework.web.context.request.ServletRequestAttributes
 * @author Pablo Krause
 */
public class ThreadAttributesImpl implements ThreadAttributes {

	private final Logger logger = LoggerFactory
			.getLogger(ThreadAttributesImpl.class);

	protected final Map<String, Object> threadAttributes = new HashMap<String, Object>();
	protected final Map<String, Runnable> threadDestructionCallbacks = new LinkedHashMap<String, Runnable>();

//	private volatile boolean threadActive = true;

	/**
	 * Signal that the thread's work has been completed.
	 * <p>
	 * Executes all thread destruction callbacks.
	 */
	public void workCompleted() {
		executeDestructionCallbacks();
		// updateAccessedSessionAttributes();
//		this.threadActive = false;
	}

	/**
	 * Clears beans and processes all bean destruction callbacks.
	 */
	protected final void clear() {
		processDestructionCallbacks();

		threadAttributes.clear();
	}

	/**
	 * Processes all bean destruction callbacks.
	 */
	private final void processDestructionCallbacks() {
		for (String name : threadDestructionCallbacks.keySet()) {
			Runnable callback = threadDestructionCallbacks.get(name);

			logger.debug("Performing destruction callback for '" + name
					+ "' bean" + " on thread '"
					+ Thread.currentThread().getName() + "'.");

			callback.run();
		}

		threadDestructionCallbacks.clear();
	}

	public Object getAttribute(String name) {
		return this.threadAttributes.get(name);
	}

	public void setAttribute(String name, Object value) {
		this.threadAttributes.put(name, value);
	}

	public void removeAttribute(String name) {
		this.threadAttributes.remove(name);
		removeDestructionCallback(name);
	}

	/**
	 * Remove the thread destruction callback for the specified attribute, if
	 * any.
	 * 
	 * @param name
	 *            the name of the attribute to remove the callback for
	 */
	protected final void removeDestructionCallback(String name) {
		Assert.notNull(name, "Name must not be null");
		synchronized (this.threadDestructionCallbacks) {
			this.threadDestructionCallbacks.remove(name);
		}
	}

	public void registerDestructionCallback(String name, Runnable callback) {
		Assert.notNull(name, "Name must not be null");
		Assert.notNull(callback, "Callback must not be null");
		synchronized (this.threadDestructionCallbacks) {
			this.threadDestructionCallbacks.put(name, callback);
		}
	}

	/**
	 * Execute all callbacks that have been registered for execution after
	 * thread completion.
	 */
	private void executeDestructionCallbacks() {
		synchronized (this.threadDestructionCallbacks) {
			for (Runnable runnable : this.threadDestructionCallbacks.values()) {
				runnable.run();
			}
			this.threadDestructionCallbacks.clear();
		}
	}

	public String[] getAttributeNames() {
		String[] names = new String[this.threadAttributes.size()];
		return this.threadAttributes.keySet().toArray(names);
	}
}