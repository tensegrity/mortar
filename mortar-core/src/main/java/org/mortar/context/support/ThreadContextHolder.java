package org.mortar.context.support;

import org.springframework.core.NamedInheritableThreadLocal;

/**
 * Thread scope context holder for storing <code>ThreadAttributes</code> in a
 * <code>ThreadLocal</code> variable to proved thread scoped access.
 * 
 * @author Pablo Krause
 */
public class ThreadContextHolder {

	private static final ThreadLocal<ThreadAttributes> inheritableThreadAttributesHolder = new NamedInheritableThreadLocal<ThreadAttributes>(
			"Thread context");

	public static ThreadAttributes getAttributes() {
		return inheritableThreadAttributesHolder.get();
	}

	public static void setAttributes(ThreadAttributes attributes) {
		ThreadContextHolder.inheritableThreadAttributesHolder.set(attributes);
	}

	/**
	 * Gets current <code>ThreadAttributes</code>.
	 */
	public static ThreadAttributes currentAttributes()
			throws IllegalStateException {
		ThreadAttributes attributes = inheritableThreadAttributesHolder.get();
		if (attributes == null) {
			throw new IllegalStateException(
					"No thread scoped attributes found.");
		}
		return attributes;
	}

	/**
	 * Reset the <code>ThreadAttributes</code> for the current thread.
	 */
	public static void resetAttributes() {
		inheritableThreadAttributesHolder.remove();
	}

}
