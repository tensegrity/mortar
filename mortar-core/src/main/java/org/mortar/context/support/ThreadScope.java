package org.mortar.context.support;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * Thread scope implementation which cleans up after itself.
 * 
 * @author Pablo Krause
 * @see org.springframework.context.support.SimpleThreadScope
 */
public class ThreadScope implements Scope {

	/**
	 * Gets bean from scope.
	 */
	public Object get(String name, ObjectFactory<?> objectFactory) {
		ThreadAttributes attributes = ThreadContextHolder
				.currentAttributes();
		Object scopedObject = attributes.getAttribute(name);
		if (scopedObject == null) {
			scopedObject = objectFactory.getObject();
			attributes.setAttribute(name, scopedObject);
		}
		return scopedObject;
	}

	/**
	 * Removes bean from scope.
	 */
	public Object remove(String name) {
		ThreadAttributes attributes = ThreadContextHolder
				.currentAttributes();
		Object scopedObject = attributes.getAttribute(name);
		if (scopedObject != null) {
			attributes.removeAttribute(name);
			return scopedObject;
		} else {
			return null;
		}
	}

	public void registerDestructionCallback(String name, Runnable callback) {
		ThreadAttributes attributes = ThreadContextHolder
				.currentAttributes();
		attributes.registerDestructionCallback(name, callback);
	}

	/**
	 * Resolve the contextual object for the given key, if any. Which in this
	 * case will always be <code>null</code>.
	 */
	public Object resolveContextualObject(String key) {
		return null;
	}

	/**
	 * Gets current thread name as the conversation id.
	 */
	public String getConversationId() {
		return Thread.currentThread().getName();
	}

}