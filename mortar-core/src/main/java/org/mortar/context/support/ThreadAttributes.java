package org.mortar.context.support;


/**
 * Abstraction for accessing attribute objects associated with a trhead.
 *
 * <p>Can be implemented for any kind of request/session mechanism,
 * in particular for servlet requests and portlet requests.
 *
 * @author Pablo Krause (baed on Juergen Hoeller's RequestAttributes)
 */
public interface ThreadAttributes {
    String REFERENCE_THREAD = "thread";

    /**
     * Return the value for the thread scoped attribute of the given name, if any.
     * 
     * @param name
     *            the name of the attribute
     * @return the current attribute value, or <code>null</code> if not found
     */
    Object getAttribute(String name);

    /**
     * Set the value for the thread scoped attribute of the given name, replacing an
     * existing value (if any).
     * 
     * @param name
     *            the name of the attribute
     * @param value
     *            the value for the attribute
     */
    void setAttribute(String name, Object value);

    /**
     * Remove the thread scoped attribute of the given name, if it exists.
     * <p>
     * Note that an implementation should also remove a registered destruction
     * callback for the specified attribute, if any. It does, however,
     * <i>not</i> need to <i>execute</i> a registered destruction callback in
     * this case, since the object will be destroyed by the caller (if
     * appropriate).
     * 
     * @param name
     *            the name of the attribute
     */
    void removeAttribute(String name);

    /**
     * Retrieve the names of all attributes in the thread scope.
     * 
     * @return the attribute names as String array
     */
    String[] getAttributeNames();

    /**
     * Register a callback to be executed on destruction of the specified
     * attribute in the thread scope.
     * <p>
     * Implementations should do their best to execute the callback at the
     * appropriate time: that is, at thread work completion or termination.
     * If such a callback is not supported by the underlying
     * runtime environment, the callback <i>must be ignored</i> and a
     * corresponding warning should be logged.
     * <p>
     * Note that 'destruction' usually corresponds to destruction of the entire
     * scope, not to the individual attribute having been explicitly removed by
     * the application. If an attribute gets removed via this facade's
     * {@link #removeAttribute(String, int)} method, any registered destruction
     * callback should be disabled as well, assuming that the removed object
     * will be reused or manually destroyed.
     * 
     * @param name
     *            the name of the attribute to register the callback for
     * @param callback
     *            the destruction callback to be executed
     */
    void registerDestructionCallback(String name, Runnable callback);

    /**
     * Signal that the thread work has been completed.
     * <p>
     * Executes all thread destruction callbacks
     */
    public void workCompleted();
}
