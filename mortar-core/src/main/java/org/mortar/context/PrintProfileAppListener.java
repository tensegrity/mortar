package org.mortar.context;

import org.mortar.util.Banner;
import org.mortar.web.AppContextLoaderListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class PrintProfileAppListener implements ApplicationListener<ContextRefreshedEvent> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		String banner = Banner.generateBanner(System.getProperty(AppContextLoaderListener.DEFAULT_PROFILE_KEY));
		logger.info("Context: " + event.getApplicationContext().getId());
		logger.info("\n" + banner);
	}

}
