package org.mortar.interceptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortar.application.RuntimeExceptionWrapper;
import org.mortar.business.BusinessException;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class ExceptionInterceptor {
	private final static Log log = LogFactory.getLog(ExceptionInterceptor.class);

	public void handleException(Throwable throwable) {
		RuntimeExceptionWrapper exceptionWrapper;
		if (throwable instanceof BusinessException) {
			log.info(throwable.getMessage(), throwable);
			return;
		}

		if ((throwable instanceof RuntimeExceptionWrapper)) {
			exceptionWrapper = (RuntimeExceptionWrapper) throwable;
		} else {
			exceptionWrapper = new RuntimeExceptionWrapper(throwable);
		}
		log.error("ERROR FOLIO #:" + exceptionWrapper.getFolio(), exceptionWrapper);
		throw exceptionWrapper;
	}
}
