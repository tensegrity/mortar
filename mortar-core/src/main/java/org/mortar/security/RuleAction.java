/* Copyright GUCOBA Systems */

package org.mortar.security;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public enum RuleAction {
	NO_ACTION, WARNING, EXCEPTION, REQUIRES_AUTHORIZATION;

	/**
	 * Dado un valor ordinal, devuelve el valor de este enum correspondiente.
	 * 
	 * @param ordinal
	 *            valor ordinal del cual se quiere el valor del enum
	 * @return el enum correspondiente al valor ordinal
	 */
	public static RuleAction getValue(int ordinal) {
		for (RuleAction r : RuleAction.values()) {
			if (r.ordinal() == ordinal) {
				return r;
			}
		}
		throw new IndexOutOfBoundsException(
				"The ordinal value does not correspond to any enumeration value");
	}
}
