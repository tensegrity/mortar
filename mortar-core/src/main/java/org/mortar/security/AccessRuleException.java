/* Copyright GUCOBA Systems */

package org.mortar.security;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class AccessRuleException extends Exception {
	private static final long serialVersionUID = 1L;
	public final static String PREFIX = "iid";
	private String invocationId;
	private RuleAction ruleAction;
	private String message;

	// Constructors:

	/**
	 * Constructor
	 * 
	 * @param aRuleAction
	 *            la accion por la cual se genera la excepción
	 */
	public AccessRuleException(RuleAction aRuleAction) {
		this.ruleAction = aRuleAction;
		this.invocationId = generateInvocationId();
		generateMessage();
	}

	/**
	 * Este constructor sirve para instanciar la excepción para una invocación preexistente para la
	 * cual ya se tiene un identificador.
	 * 
	 * @param anInvocationId
	 *            el identificador de la invocación
	 * @param aRuleAction
	 *            la acción que genera la excepción
	 */
	public AccessRuleException(String anInvocationId, RuleAction aRuleAction) {
		this.ruleAction = aRuleAction;
		this.invocationId = anInvocationId;
		generateMessage();
	}

	/**
	 * 
	 */
	private void generateMessage() {
		StringBuffer msg = new StringBuffer();
		switch (ruleAction) {
		case NO_ACTION:
			// Nunca debería caer aquí, pues no es una excepción...
			msg.append("Operación '").append(invocationId).append("' autorizada.");
			break;

		case REQUIRES_AUTHORIZATION:
			msg.append("La operación '").append(invocationId).append("' requiere autorización.");
			break;

		case WARNING:
			msg.append("La operación '").append(invocationId).append("' requiere confirmación.");
			break;

		case EXCEPTION:
			msg.append("No tiene permisos para ejecutar la operación '").append(invocationId)
					.append("'.");
			break;

		default:
			throw new UnsupportedOperationException();
		}
		message = msg.toString();
	}

	// Methods:

	@Override
	public String getMessage() {
		// Sobrecargamos este método ya que para fijar el mensaje sólo es posible hacerlo
		// en la primera línea del constructor, sin embargo el mensaje requiere que generemos
		// el invocationId antes.
		return message;
	}

	/**
	 * Genera un identificador único
	 * 
	 * @return un String con un identificador único
	 */
	private String generateInvocationId() {
		StringBuffer id = new StringBuffer();
		id.append(PREFIX).append(ruleAction.ordinal()).append("-").append(
				System.currentTimeMillis()).append("-").append(this.hashCode());
		return id.toString();
	}

	// Accessor methods:

	public RuleAction getRuleAction() {
		return ruleAction;
	}

	public String getInvocationId() {
		return invocationId;
	}

}
