/* Copyright GUCOBA Systems */

package org.mortar.security.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.config.PropertiesFactoryBean;

/**
 * Lee un archivo de propiedades encriptado.
 * 
 * @author Pablo Krause
 * 
 */
public class EncryptedPropertiesFactoryBean extends PropertiesFactoryBean {
	private StandardPBEStringEncryptor passwordEnryptor;
	private static final String DEFAULT_ALGORITHM = "PBEWithMD5AndDES";
	// WARNING: el default key debe ser usado únicamente en desarrollo
	private static final char[] DEFAULT_KEY = { 51, 112, 51, 115, 89, 100, 112, 98, 66, 114, 51,
			116, 77 };
	private static Log log = LogFactory.getLog(EncryptedPropertiesFactoryBean.class);



	// Dependencias

	public StandardPBEStringEncryptor getPasswordEnryptor() {
		if (null == passwordEnryptor) {
			log.warn("SE ESTA UTILIZANDO LA LLAVE DE ENCRIPCION POR DEFECTO!");
			passwordEnryptor = new StandardPBEStringEncryptor();
			passwordEnryptor.setAlgorithm(DEFAULT_ALGORITHM);
			passwordEnryptor.setPassword(new String(DEFAULT_KEY));
		}
		return passwordEnryptor;
	}

	public void setPasswordEnryptor(StandardPBEStringEncryptor passwordEnryptor) {
		this.passwordEnryptor = passwordEnryptor;
	}
	
	
// TODO: Esto ya no jala con Spring 3 (creo que jasypt ya trae una implementación)
	
//	@SuppressWarnings("unchecked")
//	@Override
//	protected Object createInstance() throws IOException {
//		Properties props = (Properties) super..createInstance();
//		for (Iterator<?> i = props.entrySet().iterator(); i.hasNext();) {
//			Entry<?, String> entry = (Entry<?, String>) i.next();
//			String value = (String) entry.getValue();
//			// String[] valueAsArray = StringUtils.commaDelimitedListToStringArray(value);
//			String[] valueAsArray = value.split(",");
//			valueAsArray[0] = getPasswordEnryptor().decrypt(valueAsArray[0]);
//			value = StringUtils.arrayToCommaDelimitedString(valueAsArray);
//			entry.setValue(value);
//		}
//		return props;
//	}
	
}
