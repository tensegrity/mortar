package org.mortar.security;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.directwebremoting.dwrp.Batch;
import org.directwebremoting.extend.ServerException;
import org.directwebremoting.util.MimeConstants;
import org.mortar.util.ExceptionUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

public class DwrAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		@SuppressWarnings("unchecked")
		boolean jsonRequest = false;
		Enumeration<String> hEnum = request.getHeaders("Accept");
		while (hEnum.hasMoreElements()) {
			String header = hEnum.nextElement();
			if (header.toLowerCase().contains(MimeConstants.MIME_JSON)) {
				jsonRequest = true;
				break;
			}
		}
		if (jsonRequest) {
			// Instead of sending a 401, which the browser will intercept and
			// show a pop-up window, we send a json response so that the client
			// code can handle it as needed.

			String batchId = request.getParameter("batchId");

			Batch batch = null;
			try {
				batch = new Batch(request);
				batchId = batch.getExtraParameters().get("batchId").getString();
			} catch (ServerException e) {
				ExceptionUtils.throwUnchecked(e);
			}

			StringBuilder json = new StringBuilder("//#DWR-INSERT\n//#DWR-REPLY\ndwr.engine.remote.handleCallback(\"" +batchId+  "\", \"0\", {\"success\":false, \"error\":401, ");

			if (authException instanceof InsufficientAuthenticationException) {
				json.append("\"message\":\"No está autenticado\"");
			} else if (authException instanceof BadCredentialsException) {
				json.append("\"message\":\"Credenciales incorrectas\"");
			}

			json.append("});\n");

			response.addHeader("Content-Type", "text/javascript; charset=UTF-8");
			response.getWriter().append(json.toString());
		} else {
			// If it is not an ajax request, process normally
			super.commence(request, response, authException);
		}
	}

}