/* Copyright GUCOBA Systems */

package org.mortar.security;

import javax.security.auth.login.LoginException;

/**
 * 
 * Servicio para la autorización, confirmación o cancelación de invocaciones.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public interface AuthorizationService {

	/**
	 * Solicita la autorización de la ejecución de una invocación.
	 * 
	 * @param invocationId
	 *            el identificador de la invocación
	 * @param username
	 *            clave del usuario que autoriza
	 * @param password
	 *            password del usuario que autoriza
	 * @return regresa lo que sea que la operación que se pretende ejecutar regrese
	 * 
	 * @throws AccessRuleException
	 *             si aplica alguna regla de acceso
	 * @throws LoginException
	 *             si falla la autenticación del usuario que pretende autorizar
	 */
	Object authorize(String invocationId, String username, String password)
			throws AccessRuleException, LoginException;

	/**
	 * Este método cancela la invocación en curso lo que significa que se elimina la invocación de
	 * la sesión.
	 * 
	 * @param invocationId
	 *            el identificador de la invocación a cancelar
	 */
	void cancelInvocation(String invocationId);

	/**
	 * Este método se invoca cuando el usuario confirma que si quiere realizar la operación cuyo
	 * identificador se envía.
	 * 
	 * @param invocationId
	 *            invocationId el identificador de la invocación a confirmar
	 * @return el resultado de la invocación que quedó pendiente de confirmar
	 */
	Object confirmInvocation(String invocationId);

	/**
	 * Este método sirve para solicitar autorización para ejecutar una operación mediante la
	 * evaluación de las reglas de acceso para dicha operación. El método preferido NO es usar este
	 * método sino que a través de un aspecto que intercepte la invocación del servicio (operación)
	 * que se desea proteger mediante reglas. Este método es para aquellos casos en los que el
	 * front-end requiere llevar a cabo explícitamente la evaluación para, por ejemplo, mostrar o no
	 * ciertas partes de la pantalla.
	 * 
	 * @deprecated Se prefiere el uso de control de acceso medianta aspectos, es decir mediante un
	 *             interceptor que atrape la invocación y mande llamar las reglas de acceso. Este
	 *             método se incluye para soportar funcionalidad que ya estaba construida.
	 * 
	 * @param objecto
	 *            el nombre del objeto (e.g. pantalla)
	 * @param operacion
	 *            el nombre de la operación que se pretende ejecutar
	 * @param args
	 *            posibles argumentos que requiera la operación
	 * @throws AccessRuleException
	 *             si aplica alguna regla de acceso
	 */
	@Deprecated
	void evaluateAccessRules(String objecto, String operacion, Object... args)
			throws AccessRuleException;

	/**
	 * Invoca la ejecución de las reglas de acceso. Este metodo es consumido por el
	 * AccessRuleInterceptor y no está pensado para ser invocado directamente.
	 * 
	 * @param invocationInfo
	 *            la información de invocación para la evaluación de la regla de acceso
	 * @param userInfo
	 *            información del usuario con el cual se debe evaluar la regla de acceso
	 * @return un enum de tipo RuleAction con el resultado de la evaluación de las reglas.
	 */
	RuleAction invokeAccessRule(InvocationInfo invocationInfo, UserInfo userInfo);

}
