package org.mortar.security.auth.callback;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Password Callback that delegates to Spring Security.  Works for either Digest or Text passwords.
 * You would wire it like:
 * <pre>
 * <bean id="wsAuthenticationInterceptor" class="org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor">
		<constructor-arg>
			<map>
				<entry key="action" value="UsernameToken Timestamp" />
				<entry key="passwordType" value="PasswordDigest" />
<!--				<entry key="passwordType" value="PasswordText" />-->
				<entry key="passwordCallbackRef">
					<bean class="org.mortar.security.auth.callback.SpringSecurityPasswordCallback" />
				</entry>
			</map>
		</constructor-arg>
<!--		<property name="authenticationManager" ref="authenticationManager"/>-->
	</bean>
 * </pre> 
 * 
 * Remember to add this class to the search path for Spring annotations
 * or wire the authenticationManager manually in your xml.
 * 
 * Taken from:
 * http://enikofactory.blogspot.com/2009/10/cxf-wss4j-spring-security-recipe.html
 * 
 * @author Pablo Krause
 * 
 */
public class SpringSecurityPasswordCallback implements CallbackHandler {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userService;

	// @Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

		WSPasswordCallback pwdCallback = (WSPasswordCallback) callbacks[0];

		int usage = pwdCallback.getUsage();
		if ((usage == WSPasswordCallback.USERNAME_TOKEN) || (usage == WSPasswordCallback.USERNAME_TOKEN_UNKNOWN)) {
			String password = pwdCallback.getPassword();
			if (usage == WSPasswordCallback.USERNAME_TOKEN) {
				UserDetails userDetails = userService.loadUserByUsername(pwdCallback.getIdentifier());
				password = userDetails.getPassword();
			}
			Authentication authentication = new UsernamePasswordAuthenticationToken(pwdCallback.getIdentifier(), password);
			authentication = authenticationManager.authenticate(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			// Return the password to the caller
			pwdCallback.setPassword(password);
		}
	}

	// Accessors

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public UserDetailsService getUserService() {
		return userService;
	}

	public void setUserService(UserDetailsService userService) {
		this.userService = userService;
	}
}
