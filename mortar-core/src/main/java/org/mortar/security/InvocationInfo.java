/* Copyright GUCOBA Systems */

package org.mortar.security;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Esta clase se utiliza para pasar información del contexto de la invocación al motor de reglas
 * responsable de determinar la acción a tomar en base, entre otras cosas, a la clase y método que
 * se está pretendiendo invocar así como de los argumentos utilizados en dicha invocación (e.g. para
 * poder tomar acciones en base a datos).
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class InvocationInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String className;
	private String methodName;
	private Object[] args;

	public void setArguments(Object... arguments) {
		this.args = arguments;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
