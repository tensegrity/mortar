/**
 * 
 */
package org.mortar.security.web;

import java.util.Properties;

import org.acegisecurity.GrantedAuthority;
import org.apache.commons.lang.StringUtils;

/**
 * @author Juvenal Guzmán
 *
 */
public class SimpleSecureObjectInspectorImpl implements SecureObjectsInspector {

	private Properties objects;
	/* (non-Javadoc)
	 * @see com.gucoba.security.ui.webapp.SecureObjectsInspector#isObjectSecure(java.lang.String, org.acegisecurity.GrantedAuthority[])
	 */
	public boolean isObjectSecure(String objectId, GrantedAuthority[] auths) {
		String roles = objects.getProperty(objectId);
		if (roles != null && !StringUtils.isEmpty(roles)){
	        for( int i = 0; i < auths.length ; i++ ){
	        	if(roles.indexOf(auths[i].getAuthority()) > -1){
	        		return true;
	        	}
	        }
		}
		return false;
	}
	
	public void setObjects(Properties objects) {
		this.objects = objects;
	}

}
