package org.mortar.security.web;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.directwebremoting.util.MimeConstants;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

/**
 * This class is equivalent to
 * {@link SavedRequestAwareAuthenticationSuccessHandler} but instead of
 * redirecting or forwarding to another url it responds with a json object. For
 * this to happen, the login request mus include a parameter whose name and
 * value is defined by {@link AjaxHandlerUtils}. If this parameter/value is not
 * present in the request, this class behaves exactly as the standard
 * {@link SavedRequestAwareAuthenticationSuccessHandler}.
 * 
 * For example, in your securityContext.xml:
 * 
 * <pre>
 * 
 * 	<beans:bean id="successAuthenticationHandler"
 * 		class="org.mortar.spring.security.AjaxSavedRequestAwareAuthenticationSuccessHandler">
 * 	</beans:bean>
 * 
 * 	<beans:bean id="failureAuthenticationHandler"
 * 		class="org.mortar.spring.security.AjaxSimpleUrlAuthenticationFailureHandler">
 * 	</beans:bean>
 * 	
 * <http auto-config="true" use-expressions="true">
 * 		<form-login login-processing-url="/j_spring_security_check"
 * 			login-page="/login/login.jsp" 
 * 			authentication-success-handler-ref="successAuthenticationHandler"
 * 			authentication-failure-handler-ref="failureAuthenticationHandler" />
 * 		<logout logout-url="/j_spring_security_logout" />
 * 
 * 		<!-- Configure these elements to secure URIs in your application -->
 * 		<intercept-url pattern="/choices/**" access="hasRole('ROLE_ADMIN')" />
 * 		<intercept-url pattern="/jsp/**" access="isAuthenticated()" />
 * 		<intercept-url pattern="/resources/**" access="permitAll" />
 * 		<intercept-url pattern="/**" access="permitAll" />
 * 		<remember-me />
 * 	</http>
 * </pre>
 * 
 * @author Pablo Krause
 * 
 */
public class AjaxSavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private RequestCache requestCache = new HttpSessionRequestCache();

	private AjaxHandlerUtils ajaxHandlerUtils = new AjaxHandlerUtils();

	public AjaxSavedRequestAwareAuthenticationSuccessHandler() {
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException,
			IOException {
		SavedRequest savedRequest = requestCache.getRequest(request, response);

		if (savedRequest == null) {
			super.onAuthenticationSuccess(request, response, authentication);

			return;
		}

		if (isAlwaysUseDefaultTargetUrl() || StringUtils.hasText(request.getParameter(getTargetUrlParameter()))) {
			requestCache.removeRequest(request, response);
			super.onAuthenticationSuccess(request, response, authentication);

			return;
		}

		clearAuthenticationAttributes(request);
		
		boolean jsonRequest = false;
		Enumeration<String> hEnum = request.getHeaders("Accept");
		while (hEnum.hasMoreElements()) {
			String header = hEnum.nextElement();
			if (header.toLowerCase().contains(MimeConstants.MIME_JSON)) {
				jsonRequest = true;
				break;
			}
		}

		/*@formatter:off*/
		//                      For backward compatibility
		//                                  |
		//                                  V
		/*@formatter:on*/
		if (jsonRequest || ajaxHandlerUtils.isAjaxRequest(request)) {
			Writer writer = response.getWriter();
			writer.write("{\"success\":true, \"sessionId\":\"" + request.getSession().getId() + "\"}");
			writer.flush();
			writer.close();
		} else {
			// Use the DefaultSavedRequest URL
			String targetUrl = savedRequest.getRedirectUrl();
			logger.debug("Redirecting to DefaultSavedRequest Url: " + targetUrl);
			getRedirectStrategy().sendRedirect(request, response, targetUrl);
		}
	}

	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}

	public AjaxHandlerUtils getAjaxHandlerUtils() {
		return ajaxHandlerUtils;
	}

	public void setAjaxHandlerUtils(AjaxHandlerUtils ajaxHandlerUtils) {
		this.ajaxHandlerUtils = ajaxHandlerUtils;
	}

}
