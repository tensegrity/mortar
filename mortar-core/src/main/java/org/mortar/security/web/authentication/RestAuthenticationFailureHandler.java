package org.mortar.security.web.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * This class is equivalent to {@link SimpleUrlAuthenticationFailureHandler} but
 * instead of redirecting or forwarding to another url it responds with a json
 * object.
 * 
 * <pre>
 * 
 * 	<beans:bean id="successAuthenticationHandler"
 * 		class="org.mortar.spring.security.web.authentication.RestAuthenticationSuccessHandler">
 * 	</beans:bean>
 * 
 * 	<beans:bean id="failureAuthenticationHandler"
 * 		class="org.mortar.spring.security.web.authentication.RestAuthenticationFailureHandler">
 * 	</beans:bean>
 * 	
 * <http auto-config="true" use-expressions="true">
 * 		<form-login login-processing-url="/j_spring_security_check"
 * 			login-page="/login/login.jsp" 
 * 			authentication-success-handler-ref="successAuthenticationHandler"
 * 			authentication-failure-handler-ref="failureAuthenticationHandler" />
 * 		<logout logout-url="/j_spring_security_logout" />
 * 
 * 		<!-- Configure these elements to secure URIs in your application -->
 * 		<intercept-url pattern="/choices/**" access="hasRole('ROLE_ADMIN')" />
 * 		<intercept-url pattern="/jsp/**" access="isAuthenticated()" />
 * 		<intercept-url pattern="/resources/**" access="permitAll" />
 * 		<intercept-url pattern="/**" access="permitAll" />
 * 		<remember-me />
 * 	</http>
 * </pre>
 * 
 * @author Pablo Krause
 * 
 */
public class RestAuthenticationFailureHandler implements AuthenticationFailureHandler {

	public RestAuthenticationFailureHandler() {
	}

	/**
	 * Performs the redirect or forward to the {@code defaultFailureUrl} if set, otherwise returns a 401 error code.
	 * <p>
	 * If redirecting or forwarding, {@code saveException} will be called to cache the exception for use in the target view.
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException,
			ServletException {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		JSONObject errorJSON = new JSONObject();
		errorJSON.put("message", exception.getMessage());
		response.getWriter().append(errorJSON.toString());
	}

}
