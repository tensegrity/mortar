package org.mortar.security.web;

import javax.servlet.http.HttpServletRequest;

public class AjaxHandlerUtils {
	private final String DEFAULT_PARAM_NAME = "mortar-request-type";
	private final String DEFAULT_PARAM_VALUE = "ajax-json";
	private String paramName = DEFAULT_PARAM_NAME;
	private String paramValue = DEFAULT_PARAM_VALUE;

	public boolean isAjaxRequest(HttpServletRequest request) {
		String source = request.getParameter(paramName);
		if (source != null && source.equals(paramValue)) {
			return true;
		}
		return false;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

}
