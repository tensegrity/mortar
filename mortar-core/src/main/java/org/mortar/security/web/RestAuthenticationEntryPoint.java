package org.mortar.security.web;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
	private String realmName;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public void afterPropertiesSet() throws Exception {
		Assert.hasText(realmName, "realmName must be specified");
	}

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		logger.debug(exception.getMessage());
		String ua = request.getHeader("User-Agent");
		
		
		if (ua == null || ua.startsWith("Apache-HttpClient")) {
			response.addHeader("WWW-Authenticate", "Basic realm=\"" + realmName + "\"");
		} else {
			// For browsers, we use a custom header to prevent it from showing a login dialog.
			response.addHeader("WWW-Authenticate", "X-Basic realm=\"" + realmName + "\"");
		}
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		JSONObject errorJSON = new JSONObject();
		errorJSON.put("message", exception.getMessage());
		response.getWriter().append(errorJSON.toString());
	}
	
	static String md5Hex(String data) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No MD5 algorithm available!");
        }

        return new String(Hex.encode(digest.digest(data.getBytes())));
    }

	public String getRealmName() {
		return realmName;
	}

	public void setRealmName(String realmName) {
		this.realmName = realmName;
	}

}
