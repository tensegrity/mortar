package org.mortar.security.web.access;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

public class RestAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException,
			ServletException {
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		JSONObject errorJSON = new JSONObject();
		errorJSON.put("message", accessDeniedException.getMessage());
		response.getWriter().write(errorJSON.toString());
	}

}
