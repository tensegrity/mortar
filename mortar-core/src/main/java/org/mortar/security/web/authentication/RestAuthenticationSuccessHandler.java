package org.mortar.security.web.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 * This class is equivalent to {@link SimpleUrlAuthenticationSuccessHandler} but instead of redirecting or forwarding to another url it responds with a json
 * object.
 * 
 * <pre>
 * 
 * 	<beans:bean id="successAuthenticationHandler"
 * 		class="org.mortar.spring.security.web.authentication.RestAuthenticationSuccessHandler">
 * 	</beans:bean>
 * 
 * 	<beans:bean id="failureAuthenticationHandler"
 * 		class="org.mortar.spring.security.web.authentication.RestAuthenticationFailureHandler">
 * 	</beans:bean>
 * 	
 * <http auto-config="true" use-expressions="true">
 * 		<form-login login-processing-url="/j_spring_security_check"
 * 			login-page="/login/login.jsp" 
 * 			authentication-success-handler-ref="successAuthenticationHandler"
 * 			authentication-failure-handler-ref="failureAuthenticationHandler" />
 * 		<logout logout-url="/j_spring_security_logout" />
 * 
 * 		<!-- Configure these elements to secure URIs in your application -->
 * 		<intercept-url pattern="/choices/**" access="hasRole('ROLE_ADMIN')" />
 * 		<intercept-url pattern="/jsp/**" access="isAuthenticated()" />
 * 		<intercept-url pattern="/resources/**" access="permitAll" />
 * 		<intercept-url pattern="/**" access="permitAll" />
 * 		<remember-me />
 * 	</http>
 * </pre>
 * 
 * @author Pablo Krause
 * 
 */
public class RestAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	public RestAuthenticationSuccessHandler() {
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException,
			IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		clearAuthenticationAttributes(request);
	}

	/**
	 * Removes temporary authentication-related data which may have been stored in the session during the authentication process.
	 */
	protected final void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);

		if (session == null) {
			return;
		}

		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
	
}
