package org.mortar.security.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * A LogoutHandler that returns an ajax response instead of a 302 (Redirect) HTTP response.
 * Usage:
 * 
 * {@literal <http>
      <logout logout-url="/j_spring_security_logout" success-handler-ref="ajaxLogoutSuccessHandler" />
  </http>}
 * 
 * @author pablo
 *
 */
public class AjaxLogoutSuccessHandler implements LogoutSuccessHandler {

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		response.getWriter().write("{\"success\": true}");
	}

}
