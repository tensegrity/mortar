package org.mortar.security.web;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 * This class is equivalent to
 * {@link SimpleUrlAuthenticationSuccessHandler} but instead of
 * redirecting or forwarding to another url it responds with a json object. For
 * this to happen, the login request must include a parameter whose name and
 * value is defined by {@link AjaxHandlerUtils}. If this parameter/value is not
 * present in the request, this class behaves exactly as the standard
 * {@link SimpleUrlAuthenticationSuccessHandler}.
 * 
 * For example, in your securityContext.xml:
 * 
 * <pre>
 * 
 * 	<beans:bean id="successAuthenticationHandler"
 * 		class="org.mortar.spring.security.JsonAuthenticationSuccessHandler">
 * 	</beans:bean>
 * 
 * 	<beans:bean id="failureAuthenticationHandler"
 * 		class="org.mortar.spring.security.JsonAuthenticationFailureHandler">
 * 	</beans:bean>
 * 	
 * <http auto-config="true" use-expressions="true">
 * 		<form-login login-processing-url="/j_spring_security_check"
 * 			login-page="/login/login.jsp" 
 * 			authentication-success-handler-ref="successAuthenticationHandler"
 * 			authentication-failure-handler-ref="failureAuthenticationHandler" />
 * 		<logout logout-url="/j_spring_security_logout" />
 * 
 * 		<!-- Configure these elements to secure URIs in your application -->
 * 		<intercept-url pattern="/choices/**" access="hasRole('ROLE_ADMIN')" />
 * 		<intercept-url pattern="/jsp/**" access="isAuthenticated()" />
 * 		<intercept-url pattern="/resources/**" access="permitAll" />
 * 		<intercept-url pattern="/**" access="permitAll" />
 * 		<remember-me />
 * 	</http>
 * </pre>
 * 
 * @deprecated Use {@link org.mortar.security.web.JsonAuthenticationSuccessHandler} instead.
 * @author Pablo Krause
 * 
 */
@Deprecated
public class JsonAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private AjaxHandlerUtils ajaxHandlerUtils = new AjaxHandlerUtils();

	public JsonAuthenticationSuccessHandler() {
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException,
			IOException {

		if (ajaxHandlerUtils.isAjaxRequest(request)) {
			Writer writer = response.getWriter();
			writer.write("{\"success\":true, \"sessionId\":\"" + request.getSession().getId() + "\"}");
			writer.flush();
			writer.close();
			clearAuthenticationAttributes(request);
		} else {
			super.onAuthenticationSuccess(request, response, authentication);
			return;
		}
	}

	public AjaxHandlerUtils getAjaxHandlerUtils() {
		return ajaxHandlerUtils;
	}

	public void setAjaxHandlerUtils(AjaxHandlerUtils ajaxHandlerUtils) {
		this.ajaxHandlerUtils = ajaxHandlerUtils;
	}

}
