package org.mortar.security.web;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.directwebremoting.util.MimeConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.util.StringUtils;

/**
 * This class is equivalent to {@link SimpleUrlAuthenticationFailureHandler} but
 * instead of redirecting or forwarding to another url it responds with a json
 * object. For this to happen, the login request mus include a parameter whose
 * name and value is defined by {@link AjaxHandlerUtils}. If this
 * parameter/value is not present in the request, this class behaves exactly as
 * the standard {@link SimpleUrlAuthenticationFailureHandler}.
 * 
 * <pre>
 * 
 * 	<beans:bean id="successAuthenticationHandler"
 * 		class="org.mortar.spring.security.AjaxSavedRequestAwareAuthenticationSuccessHandler">
 * 	</beans:bean>
 * 
 * 	<beans:bean id="failureAuthenticationHandler"
 * 		class="org.mortar.spring.security.AjaxSimpleUrlAuthenticationFailureHandler">
 * 	</beans:bean>
 * 	
 * <http auto-config="true" use-expressions="true">
 * 		<form-login login-processing-url="/j_spring_security_check"
 * 			login-page="/login/login.jsp" 
 * 			authentication-success-handler-ref="successAuthenticationHandler"
 * 			authentication-failure-handler-ref="failureAuthenticationHandler" />
 * 		<logout logout-url="/j_spring_security_logout" />
 * 
 * 		<!-- Configure these elements to secure URIs in your application -->
 * 		<intercept-url pattern="/choices/**" access="hasRole('ROLE_ADMIN')" />
 * 		<intercept-url pattern="/jsp/**" access="isAuthenticated()" />
 * 		<intercept-url pattern="/resources/**" access="permitAll" />
 * 		<intercept-url pattern="/**" access="permitAll" />
 * 		<remember-me />
 * 	</http>
 * </pre>
 * 
 * @deprecated Use {@link org.mortar.security.web.authentication.RestAuthenticationFailureHandler} instead.
 * @author Pablo Krause
 * 
 */
@Deprecated
public class AjaxSimpleUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler implements AuthenticationFailureHandler {

	private static final String ACCEPT_HEADER = "Accept";
	
	private String defaultFailureUrl;

	private AjaxHandlerUtils ajaxHandlerUtils = new AjaxHandlerUtils();

	public AjaxSimpleUrlAuthenticationFailureHandler() {
	}

	public AjaxSimpleUrlAuthenticationFailureHandler(String defaultFailureUrl) {
		setDefaultFailureUrl(defaultFailureUrl);
	}

	/**
	 * Performs the redirect or forward to the {@code defaultFailureUrl} if set,
	 * otherwise returns a 401 error code.
	 * <p>
	 * If redirecting or forwarding, {@code saveException} will be called to
	 * cache the exception for use in the target view.
	 */
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException,
			ServletException {
		
		boolean jsonRequest = false;
		Enumeration<String> hEnum = request.getHeaders(ACCEPT_HEADER);
		while (hEnum.hasMoreElements()) {
			String header = hEnum.nextElement();
			if (header.toLowerCase().contains(MimeConstants.MIME_JSON)) {
				jsonRequest = true;
				break;
			}
		}

		/*@formatter:off*/
		//                      For backward compatibility
		//                                  |
		//                                  V
		/*@formatter:on*/
		if (jsonRequest || ajaxHandlerUtils.isAjaxRequest(request)) {
			// For ajax response
			saveException(request, exception);
			logger.debug("Authentication failure", exception);
			JSONObject errorJSON = new JSONObject();
			
			errorJSON.put("success", false);
			errorJSON.put("error", 401);

			String message;
			if (exception instanceof InsufficientAuthenticationException) {
				message = "No está autenticado";
			} else if (exception instanceof BadCredentialsException) {
				message = "Credenciales incorrectas";
			} else {
				message = exception.getMessage();
			}
			
			errorJSON.put("message", message);
			
			if (exception instanceof DisabledException) {
				errorJSON.put("accountDisabled", true);
			}
			
//			response.getWriter().write("{\"success\": false, \"errors\": " + errorJSON.toString() + "}");
			response.getWriter().write(errorJSON.toString());
			response.setHeader("Accept-Charset", "utf-8");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Content-Type", "text/html; charset=UTF-8");
		} else {
			// Original code
			if (defaultFailureUrl == null) {
				logger.debug("No failure URL set, sending 401 Unauthorized error");

				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication Failed: " + exception.getMessage());
			} else {
				saveException(request, exception);
				if (isUseForward()) {
					logger.debug("Forwarding to " + defaultFailureUrl);

					request.getRequestDispatcher(defaultFailureUrl).forward(request, response);
				} else {
					logger.debug("Redirecting to " + defaultFailureUrl);
					getRedirectStrategy().sendRedirect(request, response, defaultFailureUrl);
				}
			}
		}

	}

	public AjaxHandlerUtils getAjaxHandlerUtils() {
		return ajaxHandlerUtils;
	}

	public void setAjaxHandlerUtils(AjaxHandlerUtils ajaxHandlerUtils) {
		this.ajaxHandlerUtils = ajaxHandlerUtils;
	}

}
