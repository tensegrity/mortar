/**
 * 
 */
package org.mortar.async;

import java.util.Date;

import org.mortar.util.PagedRequest;


/**
 * Clase que contiene los criterios con los que se desea localizar los procesos registrados.
 * 
 * @author Alfredo Lopez powered by GUCOBA Systems S.C.
 * @version 1.0
 * 
 */
public class FindProcessRequest extends PagedRequest {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -6737738778501514463L;
	private String userName;
	private Date initialDate;
	private Date finalDate;
	private Integer state;
	private String className;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
}
