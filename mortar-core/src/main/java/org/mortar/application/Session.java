/* Copyright GUCOBA Systems */

package org.mortar.application;

import java.io.Serializable;

/**
 * Interfase que define el contrato para el manejo de la sesión desde los servicios aplicativos del
 * sistema. El ciclo de vida de la sesión está directamente ligada al HttpSession.
 * 
 * Para facilitar la recuperación de valores de la sesión se recomienda utilizar la clase
 * org.apache.commons.collections.MapUils, por ejemplo:
 * 
 * <pre>
 * Session session;
 * ...
 * MapUtils.getDouble(session, "llave");
 * </pre>
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public interface Session extends Serializable {

	/**
	 * Devuelve el objeto registrado con la llave especificada, o null si no encuentra ningún
	 * atributo con ese nombre.
	 * 
	 * @param key
	 *            la llave
	 * @return el objeto almacenado en la sesión con la llave especificada.
	 */
	Object getAttribute(String key);

	/**
	 * Agrega o sustituye un objecto (value) con la llave (key) dado.
	 * 
	 * @param key
	 *            la llave
	 * @param value
	 *            el objeto a almacenar bajo el nombre de la llave
	 * @return el objeto que se anterior con el mismo key, o null si el objeto es nuevo
	 */
	Object setAttribute(String key, Object value);

	/**
	 * Quita de la sesión el objeto registrado con la llave especificada.
	 * 
	 * @param key
	 *            la llave
	 * @return el objeto correspondiente a la llave dada o null si no existe.
	 */
	Object removeAttribute(String key);
}
