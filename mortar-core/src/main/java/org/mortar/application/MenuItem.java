package org.mortar.application;

import java.util.List;

public class MenuItem {
	private String label;
	private String value;
	private String icon;
	private String[] authorizedRoles;
	@SuppressWarnings("rawtypes")
	private List subMenu;

	public String getLabel() {
		return label;
	}

	public void setLabel(String text) {
		this.label = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@SuppressWarnings("unchecked")
	public void addItem(MenuItem item) {
		subMenu.add(item);
	}

	public List getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(List<? extends MenuItem> subMenu) {
		this.subMenu = subMenu;
	}

	public String[] getAuthorizedRoles() {
		return authorizedRoles;
	}

	public void setAuthorizedRoles(String[] authorizedRoles) {
		this.authorizedRoles = authorizedRoles;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
