/* Copyright GUCOBA Systems */

package org.mortar.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ognl.Ognl;
import ognl.OgnlException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;

/**
 * Esta clase se utiliza para definir rutas de navegación para entidades
 * obtenidas mediante Hibernate y que necesitan ser recorridas para garantizar
 * que las relaciones de tipo lazy que requiere el front-end sean cargadas de la
 * base de datos.
 * 
 * Uso:
 * 
 * <pre>
 * private static final GraphDefinition CUENTA_DETALLE = new GraphDefinition(Cuenta.class, &quot;cuentaProductoEje.tipoCuentaActual.detalleTipoCuenta&quot;, &quot;persona&quot;,
 * 		&quot;cuentaAsociada&quot;);
 * </pre>
 * 
 * y posteriormente dentro del ApplicationService:
 * 
 * <pre>
 * loadEntityGraph(cuenta, CUENTA_DETALLE);
 * </pre>
 * 
 * donde cuenta es una instancia de una entidad obtenida mediante Hibernate.
 * 
 * @deprecated Use {@link org.mortar.annotation.PopulateResponse @PopulateResponse} annotation instead.
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
@Deprecated
public class GraphDefinition {
	private List<String> pathList;
	private List<Object> compiledPathList = new ArrayList<Object>();
	private final static Log LOG = LogFactory.getLog(GraphDefinition.class);

	// Constructors

	// No queremos un constructor sin argumentos, ya que queremos forzar la
	// validación de las rutas especificadas para la entidad al instanciar el
	// objeto. De no ser así el programador podría olvidar invocar al método
	// despues de instanciarlo.
	@SuppressWarnings("unused")
	private GraphDefinition() {
	}

	/**
	 * 
	 * Constructor principal
	 * 
	 * @param entityClass
	 *            la clase de la entidad cuyas rutas de navegación se quiere
	 *            cargar
	 * @param path
	 *            rutas de navegación que requieren ser cargadas de la entidad
	 *            correspondiente
	 */
	public GraphDefinition(String... path) {
		this.pathList = Arrays.asList(path);
		String currentPath = null;
		try {
			for (String p : path) {
				currentPath = p;
				Object expr = Ognl.parseExpression(p);
				compiledPathList.add(expr);
			}
		} catch (OgnlException e) {
			throw new RuntimeException("La expresion '" + currentPath + "' no es una ruta de navegación válida.", e);
		}
	}

	// Methods

	/**
	 * 
	 * Carga las relaciones o rutas de navegación definidas en la instancia del
	 * GraphDefinition para la entidad solicitada.
	 * 
	 * Es importante que este método se invoque en el contexto de una sesión de
	 * Hibernate, de lo contrario se producirá un error.
	 * 
	 * @param entity
	 *            la instancia de la entidad que se desea hidratar
	 * @return devuelve la misma entidad que se pasó como parámetro pero ya
	 *         hidratada
	 */
	public Object load(Object entity) {
		if (null == entity) {
			return null;
		}
		for (Object path : compiledPathList) {
			try {
				Object value = Ognl.getValue(path, entity);
				// Forzamos la carga de la base de datos:
				Hibernate.initialize(value);
			} catch (OgnlException e) {
				if (e.getMessage() != null && e.getMessage().startsWith("source is null")) {
					// Si alguna propiedad es nula, la evaluación de OGNL falla
					// sin embargo no hay problema, simplemente significa que no
					// hay más
					// objetos que cargar en ese path.
					if (LOG.isDebugEnabled()) {
						LOG.debug("Propiedad nula al tratar de hidratar entidad: " + e.getMessage());
					}
					continue;
				}
				// Cualquier otro error de evaluación genera una excepción ya
				// que significa
				// un error en la codificación o definición de algún
				// GraphDefinition.
				throw new RuntimeExceptionWrapper(e);
			}
		}
		return entity;
	}

	// Accesors

	public List<String> getPathList() {
		return pathList;
	}

}
