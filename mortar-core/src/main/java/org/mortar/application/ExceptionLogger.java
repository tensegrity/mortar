/* Copyright GUCOBA Systems */

package org.mortar.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortar.business.BusinessException;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class ExceptionLogger {
	private static final Log LOG = LogFactory.getLog(ExceptionLogger.class);

	/**
	 * Este metodo se engarga de registrar en la bitacora la exepci&oacute;n.
	 * 
	 * @param throwable
	 *            La excepci&oacute;n a manejar
	 */
	@SuppressWarnings("deprecation")
	public void handleException(Throwable throwable) {
		RuntimeExceptionWrapper exceptionWrapper;
		if (throwable instanceof BusinessException) {
			LOG.info(throwable.getMessage(), throwable);
			return;
		}
		Throwable cause;

		if (throwable instanceof RuntimeExceptionWrapper) {
			exceptionWrapper = (RuntimeExceptionWrapper) throwable;
			cause = exceptionWrapper.getCause();
		} else {
			exceptionWrapper = new RuntimeExceptionWrapper(throwable);
			cause = throwable;
		}
		LOG.error("ERROR FOLIO #:" + createFolio(cause), throwable);
		// TODO se debe de lanzar excepción aquí? Creo que está mal.
		throw exceptionWrapper;
	}

	public String createFolio(Throwable cause) {
		// TODO debería ser un numero más corto
		return new StringBuffer().append(System.currentTimeMillis())
				.append("-").append(cause.hashCode()).toString();
	}

}
