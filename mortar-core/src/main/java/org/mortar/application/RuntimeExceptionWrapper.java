/* Copyright GUCOBA Systems */

package org.mortar.application;

/**
 * Excepción que nos permite envolver excepciones de tipo checked (i.e. de tipo
 * Exception) dentro de esta envoltura que es de tipo unchecked (i.e.
 * RuntimeException) y así evitar que el código se llene de try/catch que de
 * todas formas no son recuperables. Esta clase no genera un nuevo stacktrace
 * (ver método fillInSTackTrace) sino que adopta el stacktrace de la excepción
 * original.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * @deprecated Use ExceptionUtils.throwUnchecked instead
 */
@Deprecated
public class RuntimeExceptionWrapper extends RuntimeException {
	private static final long serialVersionUID = -8469145996050478913L;
	private String folio;

	/**
	 * 
	 * @param cause
	 */
	public RuntimeExceptionWrapper(Throwable cause) {
		super(cause.getMessage(), cause);
		setStackTrace(cause.getStackTrace());
		String f = String.valueOf(System.currentTimeMillis());
		this.folio = f.substring(f.length() - 6);
		//this.folio = new StringBuffer().append(System.currentTimeMillis()).append("-").append(cause.hashCode()).toString();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		// We override this method to avoid unnecessary stack generation
		return this;
	}

	@Override
	public String toString() {
		String s = this.getClass().getName() + "( " + getCause().getClass().getName();
		String message = getLocalizedMessage();
		return (message != null) ? (s + ": " + message + " )") : s;
	}

	public String getFolio() {
		return folio;
	}

}
