/* Copyright GUCOBA Systems */

package org.mortar.application;

import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Implementación de una sesión.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
@Component("session")
@Scope(value=WebApplicationContext.SCOPE_SESSION, proxyMode=ScopedProxyMode.INTERFACES)
public class SessionImpl implements Session {
	private static final long serialVersionUID = -2792589512128747476L;
	@SuppressWarnings("unchecked")
	private HashMap backingMap = new HashMap();

	/**
	 * Constructor básico.
	 */
	public SessionImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public Object getAttribute(String key) {
		return backingMap.get(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public Object setAttribute(String key, Object value) {
		return backingMap.put(key, value);
	}

	/**
	 * {@inheritDoc}
	 */
	public Object removeAttribute(String key) {
		return backingMap.remove(key);
	}

}
