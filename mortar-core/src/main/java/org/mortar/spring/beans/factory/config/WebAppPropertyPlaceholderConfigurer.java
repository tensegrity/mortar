package org.mortar.spring.beans.factory.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.web.context.ServletContextAware;

/**
 * A property resource configurer that exposes the configured properties and
 * sets them in the ServletContext as application attributes.
 * 
 * @author Pablo Krause
 * 
 */
public class WebAppPropertyPlaceholderConfigurer extends
		EnhancedPropertyPlaceholderConfigurer implements ServletContextAware {
	private ServletContext servletContext;
	public final static String DEFAULT_KEY = "mortar.app.props";
	public final static String DEFAULT_INCLUDE_PATTERN = "app\\..*";
	
	/**
	 * Name of the key that will be used to set the attributes in the {@link ServletContext}.
	 * Defaults to mortar.app.props
	 */
	private String key = DEFAULT_KEY;
	
	/**
	 * Only properties matching the include pattern regular expression will be exposed.
	 * Defaults to app\..*
	 */
	private String includePattern = DEFAULT_INCLUDE_PATTERN;
	
	private Map<String, String> attributes = new HashMap<String, String>();

	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactoryToProcess,
			Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		setAsApplicationProperties();
	}

	public void setServletContext(ServletContext aServletContext) {
		this.servletContext = aServletContext;
		setAsApplicationProperties();
	}

	private void setAsApplicationProperties() {
		Map<String, String> props = this.getProperties();
		servletContext.setAttribute(this.key, attributes);
		if (null != props && props.size() > 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Setting Spring context placeholder properties as web application properties (ServletContext) under the key " + this.key);
			}
			for (@SuppressWarnings("rawtypes") Entry entry : this.getProperties().entrySet()) {
				String key = String.valueOf(entry.getKey());
				if (null != key && key.matches(this.includePattern)) {
					String value = String.valueOf(entry.getValue());
					attributes.put(key, value);
					logger.debug(String.format("'%1$s'='%2$s'", key, value));
				} else {
					logger.debug(String.format("'%1$s'=(Will not be exposed to the web application)", key));
				}
			}
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getIncludePattern() {
		return includePattern;
	}

	public void setIncludePattern(String includePattern) {
		this.includePattern = includePattern;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}
	
}
