package org.mortar.annotation;

public interface Evaluator {
	Object evaluate(Object root, PopulateResponse annotation);
}
