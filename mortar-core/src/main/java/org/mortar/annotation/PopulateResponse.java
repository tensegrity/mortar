package org.mortar.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to declare which relations should be guaranteed to be
 * loaded from a Hibernate object being returned by a method.
 * 
 * In order for it to work you either have to enable AspectJ support by
 * including the following element in your Spring configuration files:
 * 
 * <pre>
 * {@code
 * <aop:aspectj-autoproxy/>
 * }
 * </pre>
 * 
 * or by using the legacy xml configuration:
 * 
 * <pre class="code">
 * <code class="xml">
 * {@literal
 * <aop:config>
 * 	<aop:aspect ref="evalHandler" order="1">
 * 		<aop:around method="handle" pointcut="execution(@org.mortar.annotation.PopulateResponse * *(..))" />
 * 	</aop:aspect>
 * </aop:config>
 * }</code>
 * </pre>
 * 
 * The default evaluation engine is Spring Expression Language (Spel) which
 * requires no further dependencies. You can plug in your own evaluation engine
 * by implementing the {@link org.mortar.annotation.Evaluator Evaluator}
 * interface and registering it as the default evaluator:
 * 
 * <pre class="code">
 * <code class="xml">
 * {@literal
 * <bean class="org.mortar.annotation.OgnlEvaluator">
 * 	<qualifier value="default" />
 * </bean>
 * }</code>
 * </pre>
 * 
 * @author Pablo Krause
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Documented
public @interface PopulateResponse {
	public String[] value() default {};
}
