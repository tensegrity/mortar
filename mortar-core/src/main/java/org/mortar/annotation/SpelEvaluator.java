package org.mortar.annotation;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

@Component
@Qualifier("spel")
public class SpelEvaluator implements Evaluator {
	private ConcurrentMap<PopulateResponse, Expression[]> expCache = new ConcurrentHashMap<PopulateResponse, Expression[]>();
	private ExpressionParser parser = new SpelExpressionParser();

	public Object evaluate(Object root, PopulateResponse annotation) {
		if (null == root) {
			return null;
		}
		Expression[] exp;
		Object lastEval = null;
		exp = expCache.get(annotation);
		int i = 0;
		if (null == exp) {
			exp = new Expression[annotation.value().length];
			for (String e : annotation.value()) {
				exp[i++] = parser.parseExpression(e);
			}
			exp = exp == null ? new Expression[0] : exp;
			expCache.put(annotation, exp);
		}
		for (Expression e : exp) {
			lastEval = e.getValue(root);
		}
		return lastEval;
	}

}
