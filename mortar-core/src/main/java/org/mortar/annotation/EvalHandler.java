package org.mortar.annotation;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EvalHandler {

	@Autowired
	@Qualifier("spel")
	private Evaluator defaultEvaluator;

	@Autowired(required = false)
	@Qualifier("default")
	private Evaluator evaluator;

	@Around("execution(@org.mortar.annotation.PopulateResponse * *(..))")
	public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
		Object response = null;
		response = joinPoint.proceed();
		if (null != response) {
			Method m = ((MethodSignature) joinPoint.getSignature()).getMethod();
			m = joinPoint.getTarget().getClass().getMethod(m.getName(), m.getParameterTypes());
			PopulateResponse annotation = m.getAnnotation(PopulateResponse.class);
			if (annotation.value().length > 0) {
				getEvaluator().evaluate(response, annotation);
			} else {
				// This forces loading of a possible lazy object.  Could throw an ObjectNotFoundException.
				response.toString();
			}
		}
		return response;
	}

	public Evaluator getEvaluator() {
		return evaluator == null ? defaultEvaluator : evaluator;
	}

	public void setEvaluator(Evaluator evaluator) {
		this.evaluator = evaluator;
	}

}
