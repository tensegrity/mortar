package org.mortar.annotation;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import ognl.Ognl;
import ognl.OgnlException;

import org.mortar.util.ExceptionUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("ognl")
public class OgnlEvaluator implements Evaluator {
	private ConcurrentMap<PopulateResponse, Object[]> expCache = new ConcurrentHashMap<PopulateResponse, Object[]>();

	public Object evaluate(Object root, PopulateResponse annotation) {
		if (null == root) {
			return null;
		}
		Object[] exp;
		Object lastEval = null;
		exp = expCache.get(annotation);
		int i = 0;
		try {
			if (null == exp) {
				exp = new Object[annotation.value().length];
				for (String e : annotation.value()) {
					exp[i++] = Ognl.parseExpression(e);
				}
				exp = exp == null ? new Object[0] : exp;
				expCache.put(annotation, exp);
			}
			for (Object e : exp) {
				lastEval = Ognl.getValue(e, root);
			}
		} catch (OgnlException x) {
			throw ExceptionUtils.throwUnchecked(x);
		}
		return lastEval;
	}

}
