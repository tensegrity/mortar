package org.mortar.jawr;

import java.io.Reader;
import java.io.StringReader;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import net.jawr.web.resource.bundle.generator.GeneratorContext;
import net.jawr.web.resource.bundle.generator.ResourceGenerator;

import org.mortar.spring.beans.factory.config.WebAppPropertyPlaceholderConfigurer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MortarGenerator implements ResourceGenerator {
	private String script = null;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private synchronized String createScript(ServletContext context) {
		if (null != this.script) {
			logger.warn("Script was already created!");
			return this.script;
		}
		StringBuilder sb = new StringBuilder();
		String contextRoot = context.getContextPath();
		
		// TODO we are using the DEFAULT_KEY and not the actual key because we would need to get hold
		// of the actual instance of the WebAppPropertyPlaceholderConfigurer.  This means that if 
		// it will not work if someone uses a key other than the default. Unlikely, but...
		Map<String, Object> appProps = ((Map) context.getAttribute(WebAppPropertyPlaceholderConfigurer.DEFAULT_KEY));

		sb.append("Ext.ns('Mortar.app.props');\n");
		sb.append("Mortar.app.props = {\n");
		String separator = "\t";
		if (null != appProps) {
			for (Entry prop : appProps.entrySet()) {
				String key = ((String) prop.getKey()).replaceAll("\\.", "_").toUpperCase();
				sb.append(separator).append("'").append(key).append("':'").append(String.valueOf(prop.getValue())).append("'\n");
				separator = "\t,";
			}
		} else {
			logger.debug("There are no properties to expose to javascript.");
		}
		sb.append(separator).append("CONTEXT_ROOT:'").append(contextRoot).append("'\n");
		sb.append("};\n");

		this.script = sb.toString();
		logger.debug("Generated script:\n {}", this.script);
		return this.script;
	}

	public Reader createResource(GeneratorContext context) {
		if (null == this.script) {
			createScript(context.getServletContext());
		}
		return new StringReader(this.script);
	}

	public String getMappingPrefix() {
		return "mortar";
	}

	public String getDebugModeRequestPath() {
		return ResourceGenerator.JAVASCRIPT_DEBUGPATH;
	}
}