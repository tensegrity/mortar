package org.mortar.web.impl;

import org.directwebremoting.annotations.RemoteProxy;
import org.mortar.web.RemoteConsole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RemoteProxy(name = "RemoteConsole")
public class RemoteConsoleImpl implements RemoteConsole {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void error(String str, String trace) {
		if (null != trace) {
			logger.error("{} \n {}", str, trace);
		} else {
			logger.error("{}", str);
		}
	}

	@Override
	public void warn(String str, String trace) {
		if (null != trace) {
			logger.warn("{} \n {}", str, trace);
		} else {
			logger.warn("{}", str);
		}
	}

	@Override
	public void info(String str, String trace) {
		if (null != trace) {
			logger.info("{} \n {}", str, trace);
		} else {
			logger.info("{}", str);
		}
	}

	@Override
	public void debug(String str, String trace) {
		if (null != trace) {
			logger.debug("{} \n {}", str, trace);
		} else {
			logger.debug("{}", str);
		}
	}

}
