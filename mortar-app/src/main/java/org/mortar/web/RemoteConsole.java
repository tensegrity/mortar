package org.mortar.web;

public interface RemoteConsole {

	public void error(String str, String trace);
	public void warn(String str, String trace);
	public void info(String str, String trace);
	public void debug(String str, String trace);
	
}
