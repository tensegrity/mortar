/* Copyright GUCOBA Systems */

package org.mortar.app.util;

import java.util.List;

import org.directwebremoting.annotations.RemoteProxy;
import org.mortar.persistence.util.ComboHelperDAO;
import org.mortar.service.AbstractApplicationService;
import org.mortar.util.EntityQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementación de la interfase ComboHelperService que facilita el llenado de combo boxes para el
 * front end.
 * 
 * @author Pablo Krause
 * 
 */
@RemoteProxy(name="ComboHelperService")
@Service
public class ComboHelperServiceImpl extends AbstractApplicationService implements
		ComboHelperService {

	@Autowired
	private ComboHelperDAO comboHelperDAO;

	/**
	 * Esta clase requiere de un ComboHelperDAO para funcionar.
	 * 
	 * @param comboHelperDAO
	 */
	public void setComboHelperDAO(ComboHelperDAO comboHelperDAO) {
		this.comboHelperDAO = comboHelperDAO;
	}
	
	// Implementacion de interfase ComboHelperService:

	/** {@inheritDoc} */
	public List<?> load(EntityQuery query) {
		return comboHelperDAO.load(query);
	}
	
}
