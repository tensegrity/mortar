/**
 * 
 */
package org.mortar.application.async.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * Objeto que contiene los datos del proceso.
 * 
 * @author Alfredo Lopez powered by GUCOBA Systems S.C.
 * @version 1.0
 */
public class Process implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long processId;
	private String className;
	private String userName;
	private Integer state;
	private Date initialDate;
	private Date finalDate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esiglo.plf.hibernate.persistence.IEntity#getIdentity()
	 */
	public Serializable getIdentity() {
		return getProcessId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esiglo.plf.hibernate.persistence.IEntity#setIdentity(java.io.Serializable)
	 */
	public void setIdentity(Serializable serializable) {
		setProcessId((Long) serializable);
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Process Id : ").append(processId);
		buffer.append(" Class Name : ").append(className);
		buffer.append(" User Name : ").append(userName);
		buffer.append(" State : ").append(state);
		return buffer.toString();
	}

	public Long getProcessId() {
		return processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

}
