/* Copyright GUCOBA Systems */

package org.mortar.persistence.util;

import java.util.List;

import org.mortar.util.EntityQuery;
import org.mortar.util.Page;


/**
 * Define métodos que facilitan la consulta de datos para el llenado de
 * combo boxes para el front end.
 * 
 * @author Pablo Krause
 * 
 */
public interface ComboHelperDAO {
	
	List<?> load(EntityQuery query);
	
	Page<?> loadPaged(EntityQuery query);
	
}
