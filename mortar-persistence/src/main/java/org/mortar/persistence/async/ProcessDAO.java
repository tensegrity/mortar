package org.mortar.persistence.async;

import org.mortar.async.FindProcessRequest;
import org.mortar.persistence.EntityDAO;
import org.mortar.util.Page;

/**
 * DAO que realiza las operaciones de persistencia para la entidad Process.
 * 
 * @author Alfredo López powered by GUCOBA Systems S.C.
 * @version 1.0
 */
public interface ProcessDAO extends EntityDAO<Process, Long> {
	/**
	 * método que realiza la busqueda del o los procesos mediante los criterios contenidos dentro
	 * del request
	 * 
	 * @param request
	 *            Contiene los criterios bajo los cuales se realiza la busqueda
	 * @return Pagina que contiene los procesos que cumplieron con los criterios de busqueda
	 */
	Page<Process> findBySimilar(FindProcessRequest request);
}
