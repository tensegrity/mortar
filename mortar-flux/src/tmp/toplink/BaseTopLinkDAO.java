/* Copyright GUCOBA Systems */

package org.jnf.persistence.toplink;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.toplink.SessionFactory;
import org.springframework.orm.toplink.support.TopLinkDaoSupport;

/**
 * Esta clase sirve como base para los dos tipos de DAO del sistema, los DAO de entidad y los DAO de
 * repositorio. Los DAO de entidad tienen una relaci�n uno a uno con una entidad persistente y
 * llevan a cabo las operaciones sobre la entidad dada. Los DAO de repositorio operan sobre un
 * conjunto de entidades a la vez como por ejemplo para obtener informaci&oacute;n de varias
 * entidades. Especializada para la implementaci�n con TopLink
 * 
 * @author Juvenal Guzman, powered by GUCOBA Systems S.C.
 * 
 */
public abstract class BaseTopLinkDAO extends TopLinkDaoSupport{
	/** Log disponible para subclases */
	protected final static Log LOG = LogFactory.getLog(BaseTopLinkDAO.class);
	protected SessionFactory sessionFactory;
	

	public BaseTopLinkDAO() {
		super();
	}
	
//	/**
//	 * Devuelve la sesi&oacute;n activa. Si no hay sesi�n activa este m&eacute;todo No crea una
//	 * nueva sesi&oacute;n. Aseg&uacute;rese de qu� est&eacute; bien configurado su ambiente para
//	 * que exista una sesi&oacute;n activa al momento de invocar &eacute;ste m&eacute;todo.
//	 * 
//	 * @return la sesi�n activa
//	 */
//	public Session getSession() {
//		return SessionFactoryUtils.getSession(getSessionFactory(), false);
//	}
//	
//	// Accessors
//
//	public SessionFactory getSessionFactory() {
//		return sessionFactory;
//	}
//
//	public void setSessionFactory(SessionFactory sessionFactory) {
//		this.sessionFactory = sessionFactory;
//	}	
}
