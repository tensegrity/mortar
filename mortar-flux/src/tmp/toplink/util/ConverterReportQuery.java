package org.jnf.persistence.toplink.util;

import java.util.List;
import java.util.Vector;

import oracle.toplink.exceptions.DatabaseException;
import oracle.toplink.expressions.ExpressionBuilder;
import oracle.toplink.publicinterface.DatabaseRow;
import oracle.toplink.publicinterface.Session;
import oracle.toplink.queryframework.ReportQuery;
import oracle.toplink.queryframework.ReportQueryResult;

/**
 * Example extension to the ReportQuery to allow the resulting collection of ReportQueryresults to
 * be converted into a collection of user specified view objects based on a constructor. The intent
 * is to provide EJB 3.0 style constructor returned from projection queries.
 * 
 * @author Doug Clarke, TopLink PM
 */

//TODO: Revisar por una implementación mejor sin que se usen componentes deprecated.
public class ConverterReportQuery extends ReportQuery {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Class<?> resultClass;

	public ConverterReportQuery(Class<?> refClass, ExpressionBuilder eb, Class<?> resultClass) {
		super(refClass, eb);
		this.resultClass = resultClass;
	}

	public ConverterReportQuery(Class<?> refClass, Class<?> resultClass) {
		this(refClass, new ExpressionBuilder(refClass), resultClass);
	}

	public ConverterReportQuery(Class<?> refClass) {
		this(refClass, new ExpressionBuilder(refClass), null);
	}
	
	public ConverterReportQuery(Class<?> refClass, ExpressionBuilder eb) {
		this(refClass, eb, null);
	}	

	public Class<?> getResultClass() {
		return resultClass;
	}

	/**
	 * INTERNAL: Additional fields can be added to a query. This is used in m-m bacth reading to
	 * bring back the key from the join table.
	 */
	public Vector<?> getAdditionalFields() {
		return super.getAdditionalFields();
	}

	public Object execute(Session session, DatabaseRow row) throws DatabaseException {
		List<ReportQueryResult> results = (List) super.execute(session, row);

		ReportQueryResultConverter converter = new ReportQueryResultConverter(this,
				getResultClass());

		return converter.convert(results);
	}
}
