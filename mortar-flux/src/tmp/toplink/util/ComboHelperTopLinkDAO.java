package org.jnf.persistence.toplink.util;

import java.lang.reflect.Field;
import java.util.List;

import oracle.toplink.expressions.Expression;
import oracle.toplink.expressions.ExpressionBuilder;
import oracle.toplink.queryframework.ReadAllQuery;
import oracle.toplink.queryframework.ReportQuery;

import org.apache.commons.lang.StringUtils;
import org.jnf.application.RuntimeExceptionWrapper;
import org.jnf.application.util.EntityQuery;
import org.jnf.persistence.Page;
import org.jnf.persistence.toplink.BaseTopLinkDAO;
import org.jnf.persistence.util.ComboHelperDAO;

public class ComboHelperTopLinkDAO extends BaseTopLinkDAO implements ComboHelperDAO {

	@SuppressWarnings("unchecked")
	private Field getFieldDefinition(Class entityClass, String fieldName){
		Field field = null;
		Class currentClass = entityClass;
		if (fieldName.indexOf(".") > 0) {
			String[] fields = fieldName.split("\\.");
			
			for (int i = 0; i != fields.length - 1; i++) {
				currentClass = this.getSession().getClassDescriptorForAlias(StringUtils.capitalize(fields[i])).getJavaClass();					
			}
			try {
				field = currentClass.getDeclaredField(fields[fields.length - 1]);
			} catch (Exception e) {
				throw new RuntimeExceptionWrapper(e);
			} 
		} else {
			try {
				field = currentClass.getDeclaredField(fieldName);
			} catch (Exception e) {
				throw new RuntimeExceptionWrapper(e);
			} 
		}
		return field;
	}
	
	@SuppressWarnings("unchecked")
	private List<?> buildCriteriaAndLoad(EntityQuery query) {
		Class entityClass = this.getSession().getClassDescriptorForAlias(query.getEntity())
				.getJavaClass();
		ExpressionBuilder eb = new ExpressionBuilder(entityClass);
		Expression exp = null;
		if (query.hasFilters()) {
			for (EntityQuery.Filter filter : query.getFilters()) {
				Field field = getFieldDefinition(entityClass, filter.getField());
				Class<?> paramClass = field.getType();
				if (paramClass == Boolean.TYPE || paramClass == Boolean.class) {
					exp = getEqualExpression(eb, exp, filter, Boolean.getBoolean(filter.getValue()));
				} else if (paramClass == Integer.class || paramClass == Integer.TYPE) {
					exp = getEqualExpression(eb, exp, filter, Integer.valueOf(filter.getValue()));
				} else if (paramClass == Long.class || paramClass == Long.TYPE) {
					exp = getEqualExpression(eb, exp, filter, Long.valueOf(filter.getValue()));
				} else if (paramClass == Double.class || paramClass == Double.TYPE) {
					exp = getEqualExpression(eb, exp, filter, Double.valueOf(filter.getValue()));
				} else {
					exp = getLikeExpression(eb, exp, filter, "%" + filter.getValue() + "%");
				}
			}
		}

		List<?> result = null;

		if (query.hasProjections()) {
			ReportQuery rq = new ConverterReportQuery(entityClass, eb);

			for (String projection : query.getProjections()) {
				rq.addAttribute(projection);
			}
			rq.setSelectionCriteria(exp);
			result = (List<?>) this.getTopLinkTemplate().executeQuery(rq);

		} else {
			ReadAllQuery qr = new ReadAllQuery(entityClass, eb);

			if (query.hasOrderings()) {
				for (EntityQuery.Order order : query.getOrder()) {
					if (EntityQuery.Order.Dir.DESC == order.getDirection()) {
						qr.addAscendingOrdering(order.getField());
					} else {
						qr.addDescendingOrdering(order.getField());
					}
				}
			}
			qr.setSelectionCriteria(exp);
			result = (List<?>) this.getTopLinkTemplate().executeQuery(qr);
		}

		return result;
	}

	private Expression getEqualExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, Object value) {

		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).equal(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).equal(value));
		}
		return exp;
	}

	private Expression getLikeExpression(ExpressionBuilder eb, Expression exp,
			EntityQuery.Filter filter, String value) {
		if (null == exp) {
			exp = getNestedField(eb, filter.getField()).likeIgnoreCase(value);
		} else {
			exp = exp.and(getNestedField(eb, filter.getField()).likeIgnoreCase(value));
		}
		return exp;
	}

	private Expression getNestedField(ExpressionBuilder eb, String fieldName) {
		Expression exp = null;
		String[] fields = fieldName.split("\\.");
		for (String field : fields) {
			if (null == exp) {
				exp = eb.get(field);
			} else {
				exp = exp.get(field);
			}
		}
		return exp;
	}

	public List<?> load(EntityQuery query) {
		return buildCriteriaAndLoad(query);
	}

	public Page<?> loadPaged(EntityQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

}
