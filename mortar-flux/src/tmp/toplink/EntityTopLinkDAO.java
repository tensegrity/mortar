package org.jnf.persistence.toplink;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import oracle.toplink.descriptors.ClassDescriptor;
import oracle.toplink.expressions.ExpressionBuilder;
import oracle.toplink.queryframework.ReadAllQuery;
import oracle.toplink.queryframework.ReportQuery;
import oracle.toplink.queryframework.ReportQueryResult;

import org.jnf.persistence.EntityDAO;
import org.jnf.persistence.Page;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class EntityTopLinkDAO<T, ID extends Serializable> extends BaseTopLinkDAO implements
		EntityDAO<T, ID> {
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public EntityTopLinkDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	protected ClassDescriptor getClassDescriptor() {
		return getSession().getClassDescriptor(getPersistentClass());
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T entity) {
		getTopLinkTemplate().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return getTopLinkTemplate().readAll(getPersistentClass());
	}

	public Page<T> findAll(int firstRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	public Page<T> findAll(long firstRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public Page<T> findAll(int firstRecord, int pageSize) {
		int totalElements = -1;
		ExpressionBuilder emp = new ExpressionBuilder();
		ReportQuery queryCount = new ReportQuery(getPersistentClass(), emp);
		queryCount.addCount();
		List<ReportQueryResult> totalElementsResult = (List) getTopLinkTemplate().executeQuery(queryCount);
		if (null != totalElementsResult && totalElementsResult.size() > 0) {
			totalElements = ((Long)totalElementsResult.get(0).getResults().get(0)).intValue();
		}
		ReadAllQuery query = new ReadAllQuery(getPersistentClass());
		query.setFirstResult(firstRecord);
		query.setMaxRows(firstRecord + pageSize);
		query.prepareForExecution();
		List<T> result = (List) getTopLinkTemplate().executeQuery(query);		
		Page<T> page = new Page(result, firstRecord, pageSize, totalElements);
		return page;
	}

	@Deprecated
	public Page<T> findAll(long firstRecord, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Es probable modificar esta firma ya que ahora en TopLink se puede manejar: Defining a
	 * QueryByExamplePolicy TopLink support for query-by-example includes a query-by-example policy.
	 * You can edit the policy to modify query-by-example default behavior. You can modify the
	 * policy to do the following: - Use LIKE or other operations to compare attributes. By default,
	 * query-by-example allows only EQUALS. - Modify the set of values query-by-example ignores (the
	 * IGNORE set). The default ignored values are zero (0), empty strings, and FALSE. - Force
	 * query-by-example to consider attribute values, even if the value is in the IGNORE set. - Use
	 * isNull or notNull for attribute values.
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance, String... excludeProperty) {
		ReadAllQuery query = new ReadAllQuery(getPersistentClass());
		query.setExampleObject(exampleInstance);
		// if (excludeProperty.length > 0) {
		// QueryByExamplePolicy policy = new QueryByExamplePolicy();
		// query.setQueryByExamplePolicy(policy);
		// }
		return (List<T>) getTopLinkTemplate().executeQuery(query);
	}

	public Page<T> findByExample(int firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	public Page<T> findByExample(long firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		T entity = null;
		try {
			entity = (T) getTopLinkTemplate().readAndCopy(getPersistentClass(), id);
		} catch (ObjectRetrievalFailureException e) {
			logger.debug(getPersistentClass() + " with id: " + id + "is not found.");
		}
		return entity;
	}

	public T findById(ID id, boolean lock) {
		// getSession().getClassDescriptor(arg0)
		return null;
	}

	public T getById(ID id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public T saveOrUpdate(T entity) {
		getTopLinkTemplate().register(entity);
		return entity;
	}

	public void clear() {
		// TODO Auto-generated method stub

	}

	public void flush() {
		// TODO Auto-generated method stub
	}

}
