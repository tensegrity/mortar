package org.jnf.application.services.upload;

public class FileProcessMultipartException extends Exception {

	public FileProcessMultipartException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileProcessMultipartException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
