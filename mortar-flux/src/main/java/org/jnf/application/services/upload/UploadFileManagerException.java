package org.jnf.application.services.upload;

/**
 * @author Juvenal Guzm�n
 *
 */
public class UploadFileManagerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public UploadFileManagerException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UploadFileManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UploadFileManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UploadFileManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
