package org.jnf.application.services;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.BaseCommandController;

/**
 * Servlet implementation class for Servlet: LibraryLoader
 *
 */
public class LibraryLoaderController extends BaseCommandController {

	private String libraryPath = "/js/app/lib/";
	
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		InputStream fis = this.getServletContext().getResourceAsStream(libraryPath + request.getParameter("fileName"));
		if (fis != null) {
			BufferedInputStream bis = new BufferedInputStream(fis);
			byte[] bytes = new byte[bis.available()];
			response.setContentType("text/javascript");
        	response.setHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        	response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
			bis.read(bytes);
			bis.close();
			fis.close();
			OutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
			os.close();
		}
		
		return null;
	}

	public String getLibraryPath() {
		return libraryPath;
	}

	public void setLibraryPath(String libraryPath) {
		this.libraryPath = libraryPath;
	}
	
}
