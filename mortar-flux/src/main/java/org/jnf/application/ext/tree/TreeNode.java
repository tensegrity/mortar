/* Copyright GUCOBA Systems */

package org.jnf.application.ext.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Juvenal Guzm�n powered by GUCOBA Systems S.C.
 * 
 */
public class TreeNode implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String text;
	private boolean childrenLoaded = false;
	private boolean hasChildren = false;
	private boolean expanded = false;
	private List<TreeNode> children;

	public TreeNode(String id, String text) {
		this(id, text, false, false, false);
	}

	public TreeNode(String id, String text, boolean hasChildren) {
		this(id, text, hasChildren, false, false);
	}

	/**
	 * @param id
	 * @param text
	 * @param hasChildren
	 * @param childrenLoaded
	 * @param expanded
	 */
	public TreeNode(String id, String text, boolean hasChildren, boolean childrenLoaded,
			boolean expanded) {
		this.childrenLoaded = childrenLoaded;
		this.hasChildren = hasChildren;
		this.expanded = expanded;
		this.text = text;
		this.id = id;
	}

	public boolean isChildrenLoaded() {
		return childrenLoaded;
	}

	public boolean isHasChildren() {
		return hasChildren;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
		this.hasChildren = true;
		this.childrenLoaded = true;
	}

	public void addChild(TreeNode child) {
		if (!isHasChildren()) {
			setChildren(new ArrayList<TreeNode>());
		}
		getChildren().add(child);
	}

}
