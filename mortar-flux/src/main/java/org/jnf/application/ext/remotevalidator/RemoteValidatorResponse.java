/* Copyright GUCOBA Systems */
package org.jnf.application.ext.remotevalidator;

import java.io.Serializable;

/**
 * @author Juvenal Guzm�n powered by GUCOBA Systems S.C.
 * 
 */
public class RemoteValidatorResponse implements Serializable {

	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean success = true;
	private boolean valid;
	private String reason;

	public RemoteValidatorResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemoteValidatorResponse(boolean valid, String reason) {
		super();
		this.valid = valid;
		this.reason = reason;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
