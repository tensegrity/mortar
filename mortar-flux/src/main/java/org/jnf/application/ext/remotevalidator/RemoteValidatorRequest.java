/* Copyright GUCOBA Systems */
package org.jnf.application.ext.remotevalidator;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Juvenal Guzm�n powered by GUCOBA Systems S.C.
 * 
 */
public class RemoteValidatorRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String form;
	private String field;
	private String value;
	private Map<String, String> args;

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Map<String, String> getArgs() {
		return args;
	}

	public void setArgs(Map<String, String> args) {
		this.args = args;
	}
}
