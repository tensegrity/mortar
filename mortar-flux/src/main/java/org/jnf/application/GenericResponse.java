/* Copyright GUCOBA Systems */

package org.jnf.application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase sirve como base para dar respuesta a peticiones del front end.
 * 
 * @author Pablo Krause
 * 
 */
public class GenericResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean success = true;
	private List<FieldMessage> errors = new ArrayList<FieldMessage>();
	private List<String> messages = new ArrayList<String>();
	private Object data;

	/**
	 * Constructor por defecto.
	 */
	public GenericResponse() {
	}

	/**
	 * Constructor primario.
	 * 
	 * @param success
	 *            true si la respuesta es exitosa, false de lo contrario
	 * @param messages
	 *            cero o m�s mensajes que se quieren agregar a la respuesta
	 */
	public GenericResponse(boolean success, Object data, String... messages) {
		setSuccess(success);
		setData(data);
		if (null != messages) {
			for (String message : messages) {
				addMessage(message);
			}
		}
	}

	/**
	 * Constructor para respuestas no exitosas, lo cual se indica mediante el argumento
	 * success=false.  Para success=true es preferible utilizar el constructor simple
	 * que s�lo recibe una lista de mensajes.
	 * 
	 * @param success
	 *            true si la respuesta es exitosa, false de lo contrario
	 * @param messages
	 *            cero o m�s mensajes que se quieren agregar a la respuesta
	 */
	public GenericResponse(boolean success, String... messages) {
		this(success, null, messages);
	}

	/**
	 * Constructor simple que asume que la respuesta es exitosa.
	 * 
	 * @param messages
	 *            cero o m�s mensajes que se quieren agregar a la respuesta
	 */
	public GenericResponse(String... messages) {
		this(true, null, messages);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<FieldMessage> getErrors() {
		return errors;
	}

	public List<String> getMessages() {
		return messages;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * Agrega un mensaje informativo a la respuesta.
	 * 
	 * @param msg
	 */
	public void addMessage(String msg) {
		this.messages.add(msg);
	}

	/**
	 * Agrega un mensaje (de error) para un campo de la forma especificado por id.
	 * 
	 * @param id
	 * @param msg
	 */
	public void addFieldMessage(String id, String msg) {
		this.errors.add(new FieldMessage(id, msg));
		// Para que la forma tome estos errores es necesario
		// que success sea false, por lo que lo fijamos por defecto.
		setSuccess(false);
	}

	/**
	 * Esta inner class sirve para representar mensajes asociados a un campo particular de una
	 * forma.
	 * 
	 * @author Pablo Krause
	 * 
	 */
	public static class FieldMessage implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String id;
		private String msg;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public FieldMessage(String id, String msg) {
			super();
			this.id = id;
			this.msg = msg;
		}

	}

}
