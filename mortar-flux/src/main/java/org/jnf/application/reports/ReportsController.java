/* Copyright GUCOBA Systems */

package org.jnf.application.reports;

import org.springframework.web.servlet.mvc.Controller;

/**
 * @author Juvenal Guzm�n powered by GUCOBA Systems S.C.
 * 
 */
public interface ReportsController extends Controller {
}
