/* Copyright GUCOBA Systems */

package org.jnf.application.reports;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * @author Juvenal Guzm�n powered by GUCOBA Systems S.C.
 * 
 */
public abstract class ReportsControllerImpl extends MultiActionController implements
		ReportsController {
	private static final String DEFAULT_REPORT_FORMAT = "pdf";

	private Log log = LogFactory.getLog(this.getClass());

	protected String getReportFormat(HttpServletRequest request) {
		String uri = request.getRequestURI();
		String format = DEFAULT_REPORT_FORMAT;
		try {
			format = uri.substring(uri.lastIndexOf(".") + 1);
		} catch (IndexOutOfBoundsException e) {
			// ignore - pdf format will be used
			log.warn("No se especific� formato del reporte; se utiliza formato PDF por defecto.");
		}
		log.debug("using format: " + format);

		return format;
	}
}
