/**
 * 
 */
package org.jnf.application.async;

import java.io.Serializable;
import java.util.Map;

/**
 * Objeto que contiene los parametros que puedan ser requeridos durante la ejecucion del metodo
 * executeService de manera asincrona.
 * 
 * @author Alfredo Lopez powered by GUCOBA Systems S.C.
 * @version 1.0
 */
public class AsyncServiceParameters implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String usuario;
	private Serializable parametros;
	private Map<String, Serializable> attributes;

	public AsyncServiceParameters() {
		this.usuario = "";
		this.parametros = null;
	}

	public Map<String, Serializable> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Serializable> requestAttributes) {
		this.attributes = requestAttributes;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Usuario --> ").append(usuario);
		buffer.append(" Parametros --> ").append(parametros);
		return buffer.toString();
	}

	public Object getParametros() {
		return parametros;
	}

	public void setParametros(Serializable parametros) {
		this.parametros = parametros;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
