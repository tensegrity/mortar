/**
 * 
 */
package org.jnf.application.async.persistence;

import static org.apache.commons.lang.StringUtils.isNotEmpty;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.ilike;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.jnf.persistence.Page;
import org.jnf.persistence.hibernate.EntityHibernateDAO;

/**
 * Implementacion de ProcessDAO basada en Hibernate.
 * 
 * @author Alfredo Lopez powered by GUCOBA Systems S.C.
 * @version 1.0
 * 
 */
public class ProcessDAOHibernate extends EntityHibernateDAO<Process, Long> implements ProcessDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jnf.application.async.ProcesoDAO
	 *      #findByUsuario(org.jnf.application.async.FindProcesoRequest)
	 */
	public Page<Process> findBySimilar(FindProcessRequest request) {
		Criteria criteria = getSession().createCriteria(getPersistentClass());
		if (isNotEmpty(request.getUserName())) {
			criteria.add(ilike("userName", request.getUserName(), MatchMode.ANYWHERE));
		}

		if (isNotEmpty(request.getClassName())) {
			criteria.add(ilike("className", request.getClassName(), MatchMode.ANYWHERE));
		}

		if (null != request.getState()) {
			criteria.add(eq("state", request.getState()));
		}

		if (null != request.getInitialDate()) {
			criteria.add(eq("initialDate", request.getInitialDate()));
		}

		if (null != request.getFinalDate()) {
			criteria.add(eq("finalDate", request.getFinalDate()));
		}

		if (request.isAscendingOrder()) {
			criteria.addOrder(Order.asc("initialDate"));
		} else {
			criteria.addOrder(Order.desc("initialDate"));
		}

		@SuppressWarnings("unchecked")
		Page<Process> page = (Page<Process>) findPage(criteria, request.getStart(), request
				.getLimit());
		return page;
	}

}
