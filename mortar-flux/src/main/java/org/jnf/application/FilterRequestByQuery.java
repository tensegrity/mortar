package org.jnf.application;

import java.io.Serializable;
import java.util.Map;

import org.jnf.persistence.PagedRequest;



public class FilterRequestByQuery extends PagedRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, ?> extraParams;
	private Long identity;
	private String query;
	private String attribute;
	private String extraQuery;
	private String criteria;
	private String operator;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getExtraQuery() {
		return extraQuery;
	}

	public void setExtraQuery(String extraQuery) {
		this.extraQuery = extraQuery;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Map<String, ?> getExtraParams() {
		return extraParams;
	}

	public void setExtraParams(Map<String, ?> extraParams) {
		this.extraParams = extraParams;
	}

	public Long getIdentity() {
		return identity;
	}

	public void setIdentity(Long identity) {
		this.identity = identity;
	}

}
