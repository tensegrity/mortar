/**
 * Contiene las interfases y clases para la implementación de la capa de
 * presentación, responsable de la interacción e intercambio de datos
 * con el front end.
 */
package org.jnf.application;
