/* Copyright GUCOBA Systems */

package org.jnf.application;

/**
 * Excepci�n que nos permite envolver excepciones de tipo checked (i.e. de tipo Exception) dentro de
 * esta envoltura que es de tipo unchecked (i.e. RuntimeException) y as� evitar que el c�digo se
 * llene de try/catch que de todas formas no son recuperables. Esta clase no genera un nuevo
 * stacktrace (ver m�todo fillInSTackTrace) sino que adopta el stacktrace de la excepci�n original.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class RuntimeExceptionWrapper extends RuntimeException {
	private static final long serialVersionUID = -8469145996050478913L;
	private String folio;

	/**
	 * 
	 * @param cause
	 */
	public RuntimeExceptionWrapper(Throwable cause) {
		super(cause.getMessage(), cause);
		setStackTrace(cause.getStackTrace());
		this.folio = new StringBuffer().append(System.currentTimeMillis()).append("-").append(
				cause.hashCode()).toString();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		// Sobreescribimos el m�todo para evitar generar un stacktrace innecesario, ya que
		// vamos a utilizar el stacktrace de la excepci�n origien.
		return this;
	}

	@Override
	public String toString() {
		String s = this.getClass().getName() + "( " + getCause().getClass().getName();
		String message = getLocalizedMessage();
		return (message != null) ? (s + ": " + message + " )") : s;
	}

	public String getFolio() {
		return folio;
	}

}
