/* Copyright GUCOBA Systems */

package org.jnf.application;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jnf.business.BusinessException;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class ExceptionLogger {
	private static final Log LOG = LogFactory.getLog(ExceptionLogger.class);

	/**
	 * Este metodo se engarga de registrar en la bitacora la exepci&oacute;n.
	 * 
	 * @param throwable
	 *            La excepci&oacute;n a manejar
	 */
	public void handleException(Throwable throwable) {
		RuntimeExceptionWrapper exceptionWrapper;
		if (throwable instanceof BusinessException) {
			LOG.info(throwable.getMessage(), throwable);
			return;
		}

		if (throwable instanceof RuntimeExceptionWrapper) {
			exceptionWrapper = (RuntimeExceptionWrapper) throwable;
		} else {
			// Esto es nada m�s para generar el folio.
			exceptionWrapper = new RuntimeExceptionWrapper(throwable);
		}
		LOG.error("ERROR FOLIO #:" + exceptionWrapper.getFolio(), throwable);
		throw exceptionWrapper;
	}
}
