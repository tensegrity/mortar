/* Copyright GUCOBA Systems */

package org.jnf.application;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Implementación de una sesión.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class SessionImpl extends HashMap<String, Object> implements Session, Serializable {
	private static final long serialVersionUID = -2792589512128747476L;

	/**
	 * Constructor básico.
	 */
	public SessionImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public Object getAttribute(String key) {
		return get(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public Object setAttribute(String key, Object value) {
		return put(key, value);
	}

	/**
	 * {@inheritDoc}
	 */
	public Object removeAttribute(String key) {
		return remove(key);
	}

}
