/* Copyright GUCOBA Systems */

package org.jnf.application.util;

import java.util.List;

import org.jnf.application.ApplicationService;


/**
 * Servicio que facilita el llenado de combo boxes para el front end.
 * 
 * @author Pablo Krause
 * 
 */
public interface ComboHelperService extends ApplicationService {

	/**
	 * Obtiene los elementos de la entidad solicitada para llenar un combo.
	 * 
	 * @param query
	 * @return
	 */
	List<?> load(EntityQuery query);
	
}
