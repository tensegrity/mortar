package org.jnf.application.util;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jnf.application.util.EntityQuery.Order.Dir;

public class EntityQuery {

	public enum Conditions {
		LIKE, NOT_LIKE, 
		EQUAL, NOT_EQUAL, 
		IN, NOT_IN, 
		GT, NOT_GT, 
		LT, NOT_LT, 
		GE, NOT_GE, 
		LE, NOT_LE, 
		BETWEEN, NOT_BETWEEN
	}

	private String entity;
	private String sort; // filter field
	private String query; // filter value
	private Dir dir; // sort direction
	// private Conditions condition;
	private List<Filter> filters;
	private List<Order> order;
	private List<String> projections;

	public static class Filter {
		private String field;
		private String value;
		private Conditions condition;

		public Filter() {
		}

		public Filter(String field, String value) {
			this.field = field;
			this.value = value;
		}

		public Filter(String field, String value, Conditions condition) {
			this.field = field;
			this.value = value;
			this.condition = condition;
		}

		public String getField() {
			return field;
		}

		public void setField(String filterField) {
			this.field = filterField;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String filterValue) {
			this.value = filterValue;
		}

		public Conditions getCondition() {
			return condition;
		}

		public void setCondition(Conditions condition) {
			this.condition = condition;
		}
	}

	public static class Order {
		private String field;
		private Dir direction;

		public static enum Dir {
			ASC, DESC
		};

		public Order() {
			super();
		}

		public Order(String field, Dir order) {
			this.field = field;
			this.direction = order;
		}

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

		public Dir getDirection() {
			return direction;
		}

		public void setDirection(Dir direction) {
			this.direction = direction;
		}

		public void setDir(String dir) {
			this.direction = Dir.valueOf(dir.toUpperCase());
		}
	}

	public EntityQuery() {
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public List<String> getProjections() {
		return projections;
	}

	public void setProjections(List<String> projections) {
		this.projections = projections;
	}

	public List<Filter> getFilters() {
		List<Filter> allFilters;
		if (isNotEmpty(sort) && isNotEmpty(this.query)) {
			allFilters = new ArrayList<Filter>();
			allFilters.add(new Filter(this.sort, this.query, Conditions.LIKE));
			if (this.filters != null) {
				allFilters.addAll(this.filters);
			}
		} else {
			allFilters = this.filters;
		}
		return allFilters;
	}

	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}

	public void setSort(String filterField) {
		this.sort = filterField;
	}

	public List<Order> getOrder() {
		List<Order> allOrderings = new ArrayList<Order>();
		if (null != order && order.size() > 0) {
			allOrderings.addAll(this.order);
		} else if (StringUtils.isNotEmpty(this.sort)) {
			allOrderings.add(0, new Order(this.sort, this.dir == null ? Dir.ASC : this.dir));
		}
		return allOrderings;
	}

	public boolean hasFilters() {
		return (this.filters != null && this.filters.size() > 0)
				|| (isNotEmpty(this.sort) && isNotEmpty(this.query));
	}

	public boolean hasOrderings() {
		return (this.order != null && this.order.size() > 0) || (isNotEmpty(this.sort));
	}

	public boolean hasProjections() {
		return this.projections != null && this.projections.size() > 0;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setOrder(List<Order> ordering) {
		this.order = ordering;
	}

	public void setDir(Dir dir) {
		this.dir = dir;
	}

	// public String getCondition() {
	// return condition;
	// }
	//
	// public void setCondition(String condition) {
	// this.condition = condition;
	// }
}
