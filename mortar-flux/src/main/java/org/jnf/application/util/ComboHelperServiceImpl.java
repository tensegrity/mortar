/* Copyright GUCOBA Systems */

package org.jnf.application.util;

import java.util.List;

import org.jnf.application.AbstractApplicationService;
import org.jnf.persistence.util.ComboHelperDAO;

/**
 * Implementación de la interfase ComboHelperService que facilita el llenado de combo boxes para el
 * front end.
 * 
 * @author Pablo Krause
 * 
 */
public class ComboHelperServiceImpl extends AbstractApplicationService implements
		ComboHelperService {
	// Dependencias:
	private ComboHelperDAO comboHelperDAO;

	/**
	 * Esta clase requiere de un ComboHelperDAO para funcionar.
	 * 
	 * @param comboHelperDAO
	 */
	public void setComboHelperDAO(ComboHelperDAO comboHelperDAO) {
		this.comboHelperDAO = comboHelperDAO;
	}
	
	// Implementacion de interfase ComboHelperService:

	/** {@inheritDoc} */
	public List<?> load(EntityQuery query) {
		return comboHelperDAO.load(query);
	}
	
}
