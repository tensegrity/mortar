/* Copyright GUCOBA Systems */

package org.jnf.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ognl.Ognl;
import ognl.OgnlException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;

/**
 * Esta clase se utiliza para definir rutas de navegaci�n para entidades obtenidas mediante
 * Hibernate y que necesitan ser recorridas para garantizar que las relaciones de tipo lazy que
 * requiere el front-end sean cargadas de la base de datos.
 * 
 * Uso:
 * 
 * <pre>
 * private static final GraphDefinition CUENTA_DETALLE = new GraphDefinition(Cuenta.class,
 * 		&quot;cuentaProductoEje.tipoCuentaActual.detalleTipoCuenta&quot;, &quot;persona&quot;, &quot;cuentaAsociada&quot;);
 * </pre>
 * 
 * y posteriormente dentro del ApplicationService:
 * 
 * <pre>
 * loadEntityGraph(cuenta, CUENTA_DETALLE);
 *</pre>
 * 
 * donde cuenta es una instancia de una entidad obtenida mediante Hibernate.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class GraphDefinition {
	private List<String> pathList;
	private List<Object> compiledPathList = new ArrayList<Object>();
	private final static Log LOG = LogFactory.getLog(GraphDefinition.class);

	// Constructors

	// No queremos un constructor sin argumentos, ya que queremos forzar la
	// validaci�n de las rutas especificadas para la entidad al instanciar el
	// objeto. De no ser as� el programador podr�a olvidar invocar al m�todo
	// despues de instanciarlo.
	@SuppressWarnings("unused")
	private GraphDefinition() {
	}

	/**
	 * 
	 * Constructor principal
	 * 
	 * @param entityClass
	 *            la clase de la entidad cuyas rutas de navegaci�n se quiere cargar
	 * @param path
	 *            rutas de navegaci�n que requieren ser cargadas de la entidad correspondiente
	 */
	public GraphDefinition(String... path) {
		this.pathList = Arrays.asList(path);
		String currentPath = null;
		try {
			for (String p : path) {
				currentPath = p;
				Object expr = Ognl.parseExpression(p);
				compiledPathList.add(expr);
			}
		} catch (OgnlException e) {
			throw new RuntimeException("La expresion '" + currentPath
					+ "' no es una ruta de navegaci�n v�lida.", e);
		}
	}

	// Methods

	/**
	 * 
	 * Carga las relaciones o rutas de navegaci�n definidas en la instancia del GraphDefinition para
	 * la entidad solicitada.
	 * 
	 * Es importante que este m�todo se invoque en el contexto de una sesi�n de Hibernate, de lo
	 * contrario se producir� un error.
	 * 
	 * @param entity
	 *            la instancia de la entidad que se desea hidratar
	 * @return devuelve la misma entidad que se pas� como par�metro pero ya hidratada
	 */
	public Object load(Object entity) {
		if (null == entity) {
			return null;
		}
		for (Object path : compiledPathList) {
			try {
				Object value = Ognl.getValue(path, entity);
				// Forzamos la carga de la base de datos:
				Hibernate.initialize(value);
			} catch (OgnlException e) {
				if (e.getMessage() != null && e.getMessage().startsWith("source is null")) {
					// Si alguna propiedad es nula, la evaluaci�n de OGNL falla
					// sin embargo no hay problema, simplemente significa que no hay m�s
					// objetos que cargar en ese path.
					if (LOG.isDebugEnabled()) {
						LOG
								.debug("Propiedad nula al tratar de hidratar entidad: "
										+ e.getMessage());
					}
					continue;
				}
				// Cualquier otro error de evaluaci�n genera una excepci�n ya que significa
				// un error en la codificaci�n o definici�n de alg�n GraphDefinition.
				throw new RuntimeExceptionWrapper(e);
			}
		}
		return entity;
	}

	// Accesors

	public List<String> getPathList() {
		return pathList;
	}

}
