/* Copyright GUCOBA Systems */

package org.jnf.web;

import javax.servlet.ServletContextEvent;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jnf.util.Banner;
import org.springframework.context.ApplicationContextException;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.util.Log4jWebConfigurer;

/**
 * ContextLoaderListener que carga configuraci�n de Log4J delegando a la clase
 * org.springframework.web.util.Log4jWebConfigurer) y posteriormente carga el contexto de la
 * aplicaci�n, delegando a la clase org.springframework.web.context.ContextLoaderListener.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class AppContextLoaderListener extends ContextLoaderListener {
	protected static final Log LOG = LogFactory.getLog(AppContextLoaderListener.class);

	/**
	 * Si no se especifica (mediante el parametro de contexto 'profileKey' en el archivo web.xml, se
	 * asume que el perfil de arranque de la aplicaci�n est� dada por el par�metro o variable de
	 * entorno con el nombre definido por DEFAULT_PROFILE_KEY, es decir APP_PROFILE.
	 */
	protected static final String DEFAULT_PROFILE_KEY = "APP_PROFILE";
	
	/**
	 * Si no se desea usar APP_PROFILE como nombre para especificar el perfil de arranque se puede
	 * cambiar fijando un par�metro de context en web.xml, por ejemplo:
	 * 
	 * <pre>
	 *  &lt;context-param&gt;
	 *    &lt;param-name&gt;profileKey&lt;/param-name&gt;
	 *     &lt;para m-value&gt;perfil_arranque&lt;/param-value&gt;
	 *  &lt;/context-param&gt;
	 * </pre>
	 */
	protected static final String PROFILE_KEY_PARAM = "profileKey";


	/**
	 * Para cambiar el valor del perfil de arranque por defecto (i.e. cuando no se
	 * especifica mediante la variable de entorno o bien la propiedad de sistema).
	 * 
	 * <pre>
	 *  &lt;context-param&gt;
	 *    &lt;param-name&gt;defaultProfile&lt;/param-name&gt;
	 *     &lt;para m-value&gt;development&lt;/param-value&gt;
	 *  &lt;/context-param&gt;
	 * </pre>
	 */
	protected static final String PROFILE_VALUE_PARAM = "defaultProfile";
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		String profileKey = event.getServletContext().getInitParameter(PROFILE_KEY_PARAM);
		if (profileKey == null) {
			profileKey = DEFAULT_PROFILE_KEY;
		}

		String paramValue = System.getProperty(profileKey);
		String envValue = System.getenv(profileKey);
		String profileValue = event.getServletContext().getInitParameter(PROFILE_VALUE_PARAM);

		if (null == paramValue) {
			if (null == envValue) {
				if (null != profileValue) {
					LOG.warn("No se especific� el valor del perfil de arranque mediante " 
							+ profileKey + ", se asume el perfil por defecto \"" + profileValue + "\"");
					System.setProperty(profileKey, profileValue);
				} else {
					LOG.error("\n" + Banner.generateBanner("ERROR"));
					throw new ApplicationContextException("No existe la variable " + profileKey
							+ " ni como par�metro de sistema ni como variable de entorno y tampoco"
							+ " se especific� un perfil de arranque por defecto en web.xml "
							+ " mediante un <context-param> con nombre \"" + profileKey + "\".");
				}
			} else {
				profileValue = envValue;
				// Fijamos como propiedad de sistema el valor de la variable de entorno.
				System.setProperty(profileKey, profileValue);
				LOG.info("No se enctontr� el par�metro " + profileKey
						+ ", usando variable de entorno con valor '" + envValue + "'");
			}
		} else if (null == envValue) {
			profileValue = paramValue;
			LOG.info("Se encontr� " + profileKey + " como parametro de sistema con valor '"
					+ paramValue + "'");
		} else {
			profileValue = paramValue;
			// Si tanto el par�metro como la varialbe de entorno est�n definidas
			// generamos una advertencia.
			LOG.warn(profileKey + " se encuentra definido tanto como par�metro de sistema"
					+ " como variable de entorno; " + "Utilizando par�metro con valor '"
					+ paramValue + "'");
		}

		// Lets set the profile value as a web application-wide attribute
		event.getServletContext().setAttribute( profileKey, profileValue );
		
		try {
			// Primero hay que inicializar log4j
			Log4jWebConfigurer.initLogging(event.getServletContext());
			// Ahora si, inicializamos el contexto de la aplicacion
			super.contextInitialized(event);
		} catch (RuntimeException e) {
			LOG.error("\n" + Banner.generateBanner("ERROR"));
			throw e;
		}
		LOG.info("Perfil de arranque:\n" + Banner.generateBanner(profileValue));
		// A checkstyle no le gusta que imprimamos directamente a consola
		// pero queremos garantizar que se vea el perfil de arranque en
		// la consola, aun cuando log4j no se encuntre configurado para eso.
		System.out.println("Perfil de arranque:\n" + Banner.generateBanner(profileValue));
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);
		Log4jWebConfigurer.shutdownLogging(event.getServletContext());
	}

}
