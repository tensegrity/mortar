/* Copyright GUCOBA Systems */

package org.jnf.persistence;

import java.util.List;

/**
 * @author Pablo Krause
 * @deprecated Se debe usar la clase {@link Page} en lugar de PageImpl. 
 *             Esta clase se deja para no romper el c�digo preexistente.
 * @see org.jnf.persistence.Page
 * @param <T>
 *            tipo de dato que contendr� la p�gina
 */
@Deprecated
public class PageImpl<T> extends Page<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4049391415903451437L;

	/**
	 * Crea una nueva instancia utilizando el tama�o de p�gina predeterminado.
	 * 
	 * @deprecated Utilizar el m�todo {@link Page#Page(List, int, int)}
	 * @param elements
	 *            los elementos de la p�gina
	 * @param firstRecord
	 *            el �ndice del primer elemento de la p�gina
	 * @param totalElements
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, int firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * 
	 * @deprecated Utilizar el m�todo {@link Page#Page(List, int, int)}
	 * @param elements
	 *            los elementos de la p�gina
	 * @param firstRecord
	 *            el �ndice del primer elemento de la p�gina
	 * @param totalElements
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, long firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * @deprecated Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see Page#Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la p�gina
	 * @param first
	 *            el �ndice del primer elemento de la p�gina
	 * @param aPageSize
	 *            el n�mero de elementos por p�gina
	 * @param total
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, long first, int aPageSize, long total) {
		this(elements, (int) first, aPageSize, (int) total);
	}

	/**
	 * @deprecated Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see Page#Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la p�gina
	 * @param first
	 *            el �ndice del primer elemento de la p�gina
	 * @param aPageSize
	 *            el n�mero de elementos por p�gina
	 * @param total
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public PageImpl(List<T> elements, int first, int aPageSize, int total) {
		super(elements, first, aPageSize, total);
	}

}
