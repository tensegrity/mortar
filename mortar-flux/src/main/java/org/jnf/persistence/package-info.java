/**
 * Clases e interfases para la implementación de la capa de persistencia (DAO)
 * e integración con Hibernate.
 */
package org.jnf.persistence;
