/* Copyright GUCOBA Systems */

package org.jnf.persistence.util;

import java.util.List;

import org.jnf.application.util.EntityQuery;
import org.jnf.persistence.Page;


/**
 * Define m�todos que facilitan la consulta de datos para el llenado de
 * combo boxes para el front end.
 * 
 * @author Pablo Krause
 * 
 */
public interface ComboHelperDAO {
	
	List<?> load(EntityQuery query);
	
	Page<?> loadPaged(EntityQuery query);
	
}
