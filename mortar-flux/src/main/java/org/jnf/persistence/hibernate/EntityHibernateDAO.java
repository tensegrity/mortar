/* Copyright GUCOBA Systems */

package org.jnf.persistence.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.metadata.ClassMetadata;
import org.jnf.persistence.EntityDAO;
import org.jnf.persistence.Page;
import org.jnf.persistence.PagedRequest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Toda endidad persistente requiere de un DAO para llevar a cabo operaciones de persistencia que
 * deber&aacute; extender de esta clase, la cual provee funcionalidad b&aacute;sica CRUD.
 * 
 * @author Pablo Krause, powered by GUCOBA Systems S.C.
 * 
 * @param <T>
 *            El tipo de entidad que sera manejado por la instancia del DAO
 * @param <ID>
 *            El tipo de la llave primaria de la entidad
 */
public abstract class EntityHibernateDAO<T, ID extends Serializable> extends BaseHibernateDAO
		implements EntityDAO<T, ID> {
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public EntityHibernateDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}
	
	/**
	 * Devuelve el ClassMetadata de Hibernate para la entidad que es manejada por la instancia del
	 * DAO.
	 * 
	 * @return el ClassMetadata de Hibernate para la entidad que es manejada por la instancia del
	 *         DAO
	 */
	protected ClassMetadata getClassMetadata() {
		return getSessionFactory().getClassMetadata(getPersistentClass());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public T findById(ID id) {
		return findById(id, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getSession().load(getPersistentClass(), id, LockMode.UPGRADE);
		} else {
			entity = (T) getSession().load(getPersistentClass(), id);
		}
		return entity;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public T getById(ID id) {
		return (T) getSession().get(getPersistentClass(), id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> findAll() {
		return findByCriteria();
	}

	/**
	 * {@inheritDoc}
	 */
	public Page<T> findAll(int firstRecord) {
		return findAll(firstRecord, -1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	public Page<T> findAll(long firstRecord) {
		return findAll(firstRecord, -1);
	}

	/**
	 * {@inheritDoc}
	 */
	public Page<T> findAll(int firstRecord, int pageSize) {
		return findByCriteria(firstRecord, pageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	public Page<T> findAll(long firstRecord, int pageSize) {
		return findByCriteria(firstRecord, pageSize);
	}

	/**
	 * Devuelve la lista completa de resultados en base a un Criterion, ordenada por su campo
	 * identidad.
	 * 
	 * @param criterion
	 *            lista opcional de restricciones
	 * @return List de resultados completa (i.e. sin paginar)
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria criteria = createCriteria();
		for (Criterion c : criterion) {
			if (null != c) {
				criteria.add(c);
			}
		}
		return criteria.list();
	}

	/**
	 * Devuelve una p�gina de resultados para el Criterion dado. Utiliza tama�o de p�gina
	 * predeterminado.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una p�gina de resultados para el Criterion dado
	 */
	protected Page<T> findByCriteria(int firstRecord, Criterion... criterion) {
		return findByCriteria(firstRecord, -1, criterion);
	}

	/**
	 * Depreciado, utilizar el metodo que recibe int.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una p�gina de resultados para el Criterion dado
	 */
	@Deprecated
	protected Page<T> findByCriteria(long firstRecord, Criterion... criterion) {
		return findByCriteria(firstRecord, -1, criterion);
	}

	/**
	 * Devuelve una p�gina de resultados para el Criterion dado. El resultado est� ordenado en base
	 * al campo identidad o llave primaria de la entidad.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param pageSize
	 *            El tama�o de la p�gina
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una p�gina de resultados para el Criterion dado
	 */
	protected Page<T> findByCriteria(int firstRecord, int pageSize, Criterion... criterion) {
		List<Order> order = new ArrayList<Order>(1);
		// Agregamos ordenamiento por defecto en base a la llave primaria
		// para que al menos haya consistencia en cuanto a la generacion de paginas
		String identityProperty = getClassMetadata().getIdentifierPropertyName();
		order.add(Order.asc(identityProperty));
		return findByCriteria(firstRecord, pageSize, order, criterion);
	}

	/**
	 * Devuelve una p�gina de resultados para el Criterion dado. Este m�tod es el m�s completo de la
	 * familia de m�todos con el mismo nombre ya que permite especificar, adem�s de la informaci�n
	 * de paginaci�n y criterios de b�squeda, el ordenamiento del resultado.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param pageSize
	 *            El tama�o de la p�gina
	 * @param order
	 *            una lista con los criterios de ordenamiento requeridos
	 * @param criterion
	 *            criterio de b�squeda
	 * @return una p�gina de resultados para el Criterion y ordenamiento dados
	 */
	@SuppressWarnings("unchecked")
	protected Page<T> findByCriteria(int firstRecord, int pageSize, List<Order> order,
			Criterion... criterion) {
		Criteria criteria = createCriteria();
		for (Criterion c : criterion) {
			if (null != c) {
				criteria.add(c);
			}
		}
		for (Order o : order) {
			if (null != o) {
				criteria.addOrder(o);
			}
		}
		return (Page<T>) findPage(criteria, firstRecord, pageSize);
	}

	/**
	 * Depreciado, utilizar metodo que recibe int.
	 * 
	 * @param firstRecord
	 *            El primer registro a partir del cual se requiere el resultado
	 * @param pageSize
	 *            El tama�o de la p�gina
	 * @param criterion
	 *            El Criterion a ejecutar
	 * @return una p�gina de resultados para el Criterion dado
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	protected Page<T> findByCriteria(long firstRecord, int pageSize, Criterion... criterion) {
		Criteria criteria = createCriteria();
		for (Criterion c : criterion) {
			if (null != c) {
				criteria.add(c);
			}
		}
		return (Page<T>) findPage(criteria, firstRecord, pageSize);
	}

	/**
	 * Crea un objeto Order de Hibernate en base a la informaci�n contenida en el PagedRequest.
	 * 
	 * @param request
	 *            un PagedRequest con la informaci�n de ordenamiento
	 * @return un objeto Order para ser utilizado en queries de Hibernate.
	 */
	protected Order createOrder(PagedRequest request) {
		Order order;
		if (request.isAscendingOrder()) {
			order = Order.asc(request.getSort());
		} else {
			order = Order.desc(request.getSort());
		}
		return order;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance, String... excludeProperty) {
		Criteria criteria = createCriteria();
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		criteria.add(example);
		return criteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public Page<T> findByExample(int firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		Criteria criteria = createCriteria();
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		criteria.add(example);
		return (Page<T>) findPage(criteria, firstRecord, pageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public Page<T> findByExample(long firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		Criteria criteria = createCriteria();
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		criteria.add(example);
		return (Page<T>) findPage(criteria, firstRecord, pageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public T saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T entity) {
		getSession().delete(entity);
	}

	/**
	 * 
	 * @return un objeto de tipo Criteria de Hibernate de la entidad manejada por este DAO
	 */
	public Criteria createCriteria() {
		return getSession().createCriteria(getPersistentClass());
	}

	// Los siguientes m�todos se sobreescriben para que ya devuelvan el tipo de dato correcto.

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Query aQuery, int firstRecord, int aPageSize, int totalElements) {
		return (Page<T>) super.findPage(aQuery, firstRecord, aPageSize, totalElements);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Query aQuery, int firstRecord, int aPageSize) {
		return (Page<T>) super.findPage(aQuery, firstRecord, aPageSize);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Page<T> findPage(Criteria aCriteria, Order order, int firstRecord, int aPageSize) {
		return (Page<T>) super.findPage(aCriteria, order, firstRecord, aPageSize);
	}

}
