/* Copyright GUCOBA Systems */

package org.jnf.persistence.hibernate.util;

import java.lang.reflect.Field;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.type.Type;
import org.jnf.application.RuntimeExceptionWrapper;
import org.jnf.application.util.EntityQuery;
import org.jnf.persistence.Page;
import org.jnf.persistence.hibernate.BaseHibernateDAO;
import org.jnf.persistence.util.ComboHelperDAO;

/**
 * Implementación en Hibernate para la interfase ComboHelperDAO, la cual define métodos que
 * facilitan la consulta de datos para el llenado de combo boxes para el front end.
 * 
 * @author Pablo Krause
 * 
 */
public class ComboHelperHibernateDAO extends BaseHibernateDAO implements ComboHelperDAO {
	/**
	 * Crea un objeto Criteria para la entidad dada por entityName
	 * 
	 * @param query
	 * @return
	 */
	private Criteria buildCriteria(EntityQuery query) {
		Type type = ((SessionFactoryImpl) getSessionFactory()).getReturnTypes("from "
				+ query.getEntity())[0];
		Criteria criteria = getSession().createCriteria(type.getName());

		if (query.hasOrderings()) {
			for (EntityQuery.Order order : query.getOrder()) {
				if (EntityQuery.Order.Dir.DESC == order.getDirection()) {
					criteria.addOrder(Order.desc(order.getField()));
				} else {
					criteria.addOrder(Order.asc(order.getField()));
				}
			}
		}

		if (query.hasFilters()) {
			for (EntityQuery.Filter filter : query.getFilters()) {
				Class<?> paramClass;
				try {
					Field field = type.getReturnedClass().getDeclaredField(filter.getField());
					paramClass = field.getType();
				} catch (SecurityException e) {
					throw new RuntimeExceptionWrapper(e);
				} catch (NoSuchFieldException e) {
					throw new RuntimeExceptionWrapper(e);
				}
				if (paramClass == Boolean.TYPE || paramClass == Boolean.class) {
					criteria.add(Restrictions.eq(filter.getField(), Boolean.getBoolean(filter.getValue())));
				} else if (paramClass == Integer.class || paramClass == Integer.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Integer.valueOf(filter.getValue())));
				} else if (paramClass == Long.class || paramClass == Long.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Long.valueOf(filter.getValue())));
				} else if (paramClass == Double.class || paramClass == Double.TYPE) {
					criteria.add(Restrictions.eq(filter.getField(), Double.valueOf(filter.getValue())));
				} else {
					criteria.add(Restrictions.ilike(filter.getField(), filter.getValue()));
				}
			}
		}

		if (query.hasProjections()) {
			ProjectionList plist = Projections.projectionList();
			for (String projection : query.getProjections()) {
				plist.add(Projections.property(projection));
			}
			criteria.setProjection(plist);
		}
		
		// TODO: pending implementation of conditions

		return criteria;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<?> load(EntityQuery query) {
		Criteria criteria = buildCriteria(query);
		return criteria.list();
	}

	public Page<?> loadPaged(EntityQuery query) {
		// Criteria criteria = buildCriteria(query);
		// return super.findPage(criteria, firstRecord, aPageSize);
		return null;
	}

}
