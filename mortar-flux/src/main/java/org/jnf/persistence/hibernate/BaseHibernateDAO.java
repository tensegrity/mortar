/* Copyright GUCOBA Systems */

package org.jnf.persistence.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.impl.CriteriaImpl;
import org.jnf.persistence.GenericDAO;
import org.jnf.persistence.Page;

/**
 * Esta clase sirve como base para los dos tipos de DAO del sistema, los DAO de entidad y los DAO de
 * repositorio. Los DAO de entidad tienen una relaci�n uno a uno con una entidad persistente y
 * llevan a cabo las operaciones sobre la entidad dada. Los DAO de repositorio operan sobre un
 * conjunto de entidades a la vez como por ejemplo para obtener informaci&oacute;n de varias
 * entidades.
 * 
 * @author Pablo Krause, powered by GUCOBA Systems S.C.
 * 
 */
public abstract class BaseHibernateDAO implements GenericDAO {
	/** Log disponible para subclases */
	protected final static Log LOG = LogFactory.getLog(BaseHibernateDAO.class);
	protected SessionFactory sessionFactory;

	public BaseHibernateDAO() {
		super();
	}

	/**
	 * Devuelve un objeto Query a partir del nombre de un query, el cual debe de estar definido en
	 * los archivos de hibernate.
	 * 
	 * @param queryName
	 *            el nombre del query
	 * @return el objeto Query asociado
	 */
	protected Query getNamedQuery(String queryName) {
		return getSession().getNamedQuery(queryName);
	}

	/**
	 * {@inheritDoc}
	 */
	public void clear() {
		getSession().clear();
	}

	/**
	 * {@inheritDoc}
	 */
	public void flush() {
		getSession().flush();
	}

	/**
	 * Devuelve la sesi&oacute;n activa. Si no hay sesi�n activa este m&eacute;todo No crea una
	 * nueva sesi&oacute;n. Aseg&uacute;rese de qu� est&eacute; bien configurado su ambiente para
	 * que exista una sesi&oacute;n activa al momento de invocar &eacute;ste m&eacute;todo.
	 * 
	 * @return la sesi�n activa
	 */
	public Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	// Accessors

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Utilizar el m&eacute;todo con el mismo nombre pero que recibe puros int en lugar de longs.
	 * 
	 * @param aQuery
	 *            deprecated
	 * @param firstRecord
	 *            deprecated
	 * @param aPageSize
	 *            deprecated
	 * @param totalElements
	 *            deprecated
	 * @return deprecated
	 * @throws HibernateException
	 *             deprecated
	 */
	@Deprecated
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize, long totalElements)
			throws HibernateException {
		return findPage(aQuery, firstRecord, aPageSize, (int) totalElements);
	}

	/**
	 * Obtiene una p�gina de informaci�n a partir de un Query de b�squeda especificado por el objeto
	 * Query. A diferencia el m�todo del mismo nombre que funciona con objetos de tipo Criteria,
	 * �ste m�todo no es capaz de determinar el total de registros autom�ticamente. En caso de
	 * requerir el total es necesario que se pase como par�metro, de lo contrario �ste m�todo
	 * devolver� un valor falso del total de registros igual a al n�mero del �ltimo registro de la
	 * p�gina m�s uno (lo que permite que las paginaciones funcionen correctamente, aunque el n�mero
	 * total reportado no sea correcto). En caso de que se llegara a la �ltima p�gina de resultados,
	 * este m�todo si se da cuenta y s�lo entonces devuelve el n�mero correcto del total de
	 * registros.
	 * 
	 * @param aQuery
	 *            el Query que se desea ejecutar y paginar
	 * @param firstRecord
	 *            el n�mero del primer registro que se quiere traer (empezando en 0)
	 * @param aPageSize
	 *            el n�mero de registros a traer
	 * @param totalElements
	 *            El n�mero total de elementos que devuelve el query, o -1 si no se necesita
	 * @return un objeto Page con los registros encontrados
	 * @throws HibernateException
	 *             si algo falla
	 */
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize, int totalElements)
			throws HibernateException {
		int pageSize = aPageSize;
		int total = totalElements;
		if (pageSize <= 0) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug(aQuery.getQueryString());
		}
		Query query = aQuery.setFirstResult(firstRecord);
		// No nos confiamos del total y siempre pedimos 1 registro mas
		// para saber si hay otra p�gina de datos.
		query = query.setMaxResults(pageSize + 1);
		List<?> elements = query.list();
		boolean hasNextPage = false;
		if (elements.size() > pageSize) {
			// Eliminamos el �ltimo registro, que utilizamos
			// para saber si hab�a otra p�gina.
			elements.remove(pageSize);
			hasNextPage = true;
		}
		if (!hasNextPage) {
			// Si ya no hay mas paginas, ya sabemos el total de registros.
			total = firstRecord + elements.size();
		}
		if (total < 0) {
			// Si no sabemos el total, ponemos un total falso de 1 elemento mas
			// que la
			// pagina actual
			// para que el paginador funcione y permita solicitar la siguiente
			// pagina
			total = firstRecord + pageSize + 1;
		}
		@SuppressWarnings("unchecked")
		Page<?> page = new Page(elements, firstRecord, pageSize, total);
		page.setHasNextPage(hasNextPage);
		return page;
	}

	/**
	 * Obtiene una p�gina de informaci�n dado un Query en casos donde no se necesita saber el total
	 * de elementos. Este m�todo manda a llamar a {@link #findPage(Criteria, Order, int, int)} sin
	 * especificar el n�mero total de elementos.
	 * 
	 * @param aQuery
	 *            el Query que se desea ejecutar y paginar
	 * @param firstRecord
	 *            el n�mero del primer registro que se quiere traer (empezando en 0)
	 * @param aPageSize
	 *            el n�mero de registros a traer
	 * @return un objeto Page con los registros encontrados
	 * @throws HibernateException
	 */
	protected Page<?> findPage(Query aQuery, int firstRecord, int aPageSize) {
		return this.findPage(aQuery, firstRecord, aPageSize, -1);
	}

	/**
	 * Este metodo se depreci&oacute; en favor del m&eacute;todo con el mismo nombre pero que recibe
	 * int en lugar de long para firstRecord.
	 * 
	 * @param aCriteria
	 *            deprecated
	 * @param firstRecord
	 *            deprecated
	 * @param aPageSize
	 *            deprecated
	 * @return deprecated
	 * @throws HibernateException
	 *             deprecated
	 */
	@Deprecated
	protected Page<?> findPage(Criteria aCriteria, long firstRecord, int aPageSize)
			throws HibernateException {
		return findPage(aCriteria, (int) firstRecord, aPageSize);
	}

	/**
	 * 
	 * Este m�todo se encarga de obtener una p�gina de resultados a partir del criterio de b�squeda
	 * especificado por el objeto criteria. Este m�todo se encarga de obtener el n�mero total de
	 * registros que arrojar�a el criteria.
	 * 
	 * ADVERTENCIA: El argumento Criteria es modificado por este m�todo y no debe vovler a
	 * utilizarse. Esto con el fin de poder obtener el n�mero total de registros.
	 *  
	 * @param aCriteria
	 *            El criterio original que se quiere paginar.
	 * @param firstRecord
	 *            El primer registro requerido (comenzando en 0).
	 * @param aPageSize
	 *            El n�mero de elementos requerido.
	 * @return una p�gina de resultados
	 * @throws HibernateException
	 *             en caso de error
	 */
	protected Page<?> findPage(Criteria aCriteria, int firstRecord, int aPageSize)
			throws HibernateException {
		return findPage(aCriteria, null, firstRecord, aPageSize);
	}

	/**
	 * Este m�todo se encarga de obtener una p�gina de resultados a partir del criterio de b�squeda
	 * especificado por el objeto criteria. Este m�todo se encarga de obtener el n�mero total de
	 * registros que arrojar�a el criteria.
	 * 
	 * ADVERTENCIA: El argumento Criteria es modificado por este m�todo y no debe vovler a
	 * utilizarse. Esto con el fin de poder obtener el n�mero total de registros.
	 * 
	 * @param aCriteria
	 *            El criterio original que se quiere paginar.
	 * @param order
	 *            el orden en el que se quieren los resultados
	 * @param firstRecord
	 *            El primer registro requerido (comenzando en 0).
	 * @param aPageSize
	 *            El n�mero de elementos requerido.
	 * @return una p�gina de resultados
	 * @throws HibernateException
	 *             en caso de error
	 */
	protected Page<?> findPage(Criteria aCriteria, Order order, int firstRecord, int aPageSize)
			throws HibernateException {
		// NOTA: No se utiliza scrollable result set porque Oracle 9i no lo soporta
		// y lo que hace el driver es emularlo, cargando TODOS los resultados en memoria!
		// http://download-west.oracle.com/docs/cd/B10501_01/java.920/a96654/resltset.htm#1017914
		int totalElements = -1;
		boolean hasNextPage = false;
		int pageSize = aPageSize;

		if (pageSize <= 0) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
			LOG.debug("No se especific� tama�o de la p�gina... usando por default: " + pageSize);
		}

		if (null != order) {
			// Le ponemos el orden que nos pasaron.
			aCriteria.addOrder(order);
		}

		// Modificamos el criteria para obtener unicamente los registros
		// de la pagina solicitada:
		Criteria criteria = aCriteria.setFirstResult((int) firstRecord);
		// No nos confiamos del total y siempre pedimos 1 registro mas
		// para saber si hay otra p�gina de datos.
		criteria = criteria.setMaxResults(pageSize + 1);

		List<?> elements = criteria.list();

		if (elements.size() > pageSize) {
			// Eliminamos el �ltimo registro, que utilizamos
			// para saber si hab�a otra p�gina.
			elements.remove(pageSize);
			hasNextPage = true;
		}
		if (hasNextPage) {
			// Modificamos el Criteria original para obtener el numero total de elementos.
			// Necesitamos hacer cast a CriteriaImpl para tener acceso al Projection y
			// ResultTransformer originales.
			CriteriaImpl cImpl = (CriteriaImpl) aCriteria;
			// Antes necesitabamos guardar el projection y resultTransformer
			// originales, pero ya no. Ver comentario algunas l�neas m�s abajo.
			// Projection originalProjection = cImpl.getProjection();
			// ResultTransformer originalResultTransformer = cImpl.getResultTransformer();
			cImpl.setFirstResult(0);
			// Contamos primero el n�mero total de registros que arrojar�a el criteria:
			totalElements = (Integer) aCriteria.setProjection(Projections.rowCount())
					.uniqueResult();
			// Ahora, regresamos el objeto criteria a su estado original:
			// (ya no hace falta regresarlo a su estado original... ademas nunca va a quedar igual,
			// pues no hay forma de volverle a poner null al firstResult y al maxResult)
			// aCriteria.setProjection(originalProjection); // Haya sido null o no, hay que
			// reestablecerlo
			// aCriteria.setResultTransformer(originalResultTransformer);
		} else {
			// Si ya no hay mas paginas, ya sabemos el total de registros.
			totalElements = firstRecord + elements.size();
		}
		@SuppressWarnings("unchecked")
		Page<?> page = new Page(elements, firstRecord, pageSize, totalElements);
		page.setHasNextPage(hasNextPage);
		return page;
	}

}
