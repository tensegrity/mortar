/* Copyright GUCOBA Systems */

package org.jnf.persistence;

import java.io.Serializable;
import java.util.List;

/**
 * Interfase gen&eacute;rica que debe implementar todo DAO, la cual define las operaciones
 * b&aacute;sicas de persistencia. Basado en el c&oacute;digo del libro Java Persistence with
 * Hibernate de Christian Bauer y Gavin King.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 * @param <T>
 *            El tipo de clase persistente sobre la cual actua el DAO.
 * @param <ID>
 *            El tipo de dato de la llave primaria de la clase persistente.
 */
public interface EntityDAO<T, ID extends Serializable> extends GenericDAO {

	/**
	 * Devuelve la entidad sobre la que act&uacute;a la instancia del DAO.
	 * 
	 * @return el tipo de entidad (class) que maneja la instancia del DAO.
	 */
	Class<T> getPersistentClass();

	/**
	 * Busca una entidad por su id o llave primaria. Este m�todo no bloquea el regitro.
	 * 
	 * @param id
	 *            el identificador de la entidad que se quiere encontrar
	 * @return devuelve una instancia de la entidad buscada
	 */
	T findById(ID id);

	/**
	 * Regresa una entidad por su id de manera inmediata, i.e. va a la base de datos a diferencia
	 * del m&eacute;todo findById el cual es lazy.
	 * 
	 * @param id
	 * @return
	 */
	T getById(ID id);

	/**
	 * Busca una entidad por su id o llave primaria y bloquea o no el registro dependiendo del valor
	 * de lock.
	 * 
	 * @param id
	 *            el identificador de la entidad que se quiere encontrar
	 * @param lock
	 *            indica si se quiere bloquear o no la entidad
	 * @return devuelve una instancia de la entidad buscada
	 */
	T findById(ID id, boolean lock);

	/**
	 * Devuelve una lista de todas las entidades del tipo que maneja el DAO, ordenadas por su campo
	 * identidad. Considerar utilizar el m�todo {@link EntityDAO#findAll(int, int)} que devuelve los
	 * resultados una p�gina a la vez.
	 * 
	 * @return lista completa de entidades
	 */
	List<T> findAll();

	/**
	 * Devuelva una p�gina de todos las entidades del tipo que maneja el DAO ordenadas por su campo
	 * identidad. Utiliza el n&uacute;mero de p&aacute;gina predefinida.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @return una p�gina de entidades
	 */
	Page<T> findAll(int firstRecord);

	/**
	 * Depreciado, utilizar el m&eacute;todo que recibe un int.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @return una p�gina de entidades
	 */
	@Deprecated
	Page<T> findAll(long firstRecord);

	/**
	 * Devuelve una p�gina de todas las entidades del tipo que maneja el DAO, ordenadas por su campo
	 * identidad.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @param pageSize
	 *            el n�mero de elementos por p�gina
	 * @return una p�gina de entidades
	 */
	Page<T> findAll(int firstRecord, int pageSize);

	/**
	 * Depreciado, utilizar el m&eacute;todo que recibe un ints.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @param pageSize
	 *            el n�mero de elementos por p�gina
	 * @return una p�gina de entidades
	 */
	@Deprecated
	Page<T> findAll(long firstRecord, int pageSize);

	/**
	 * Dada una instancia (de ejemplo) de la Entidad que maneja el DAO, este m�todo busca instancias
	 * cuyos campos coincidan con el del ejemplo. Este m�todo devuelve la totalidad de los
	 * resultados, considerar el uso de {@link EntityDAO#findByExample(int, int, Object, String...)}
	 * que devuelve una p�gina a la vez.
	 * 
	 * @param exampleInstance
	 *            instancia de ejemplo
	 * @param excludeProperty
	 *            propiedades de la instancia que se desean excluir
	 * @return una lista de entidades
	 */
	List<T> findByExample(T exampleInstance, String... excludeProperty);

	/**
	 * Dada una instancia (de ejemplo) de la Entidad que maneja el DAO, este m�todo busca instancias
	 * cuyos campos coincidan con el del ejemplo. Este m�todo devuelve la totalidad de los
	 * resultados, considerar el uso de {@link EntityDAO#findByExample(int, int, Object, String...)}
	 * que devuelve una p�gina a la vez.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @param pageSize
	 *            el n�mero de elementos por p�gina
	 * @param exampleInstance
	 *            instancia de ejemplo
	 * @param excludeProperty
	 *            propiedades de la instancia que se desean excluir
	 * @return una p�gina de entidades
	 */
	Page<T> findByExample(int firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty);

	/**
	 * Depreciado, utilizar el m&eacute;todo que recibe un ints.
	 * 
	 * @param firstRecord
	 *            el �ndice del primer elemento a partir del cual obtener una p�gina
	 * @param pageSize
	 *            el n�mero de elementos por p�gina
	 * @param exampleInstance
	 *            instancia de ejemplo
	 * @param excludeProperty
	 *            propiedades de la instancia que se desean excluir
	 * @return una p�gina de entidades
	 */
	@Deprecated
	Page<T> findByExample(long firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty);

	/**
	 * Si la entidad es nueva la guarda, o si ya existe la actualiza. Si se afecta a una entidad que
	 * ya est� dentro del contexto de persistencia no es necesario invocar este m�todo, Hibernate se
	 * encarga de persistir los cambios autom�ticamente.
	 * 
	 * @param entity
	 *            una instancia de la entidad del tipo manejado por el DAO
	 * @return la misma entidad
	 */
	T saveOrUpdate(T entity);

	/**
	 * Elimina la entidad dada.
	 * 
	 * @param entity
	 *            instancia de la entidad que se quiere eliminar
	 */
	void delete(T entity);

}
