/* Copyright GUCOBA Systems */

package org.jnf.persistence;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;


/**
 * 
 * @param <T>
 *            tipo de dato que contendr� la p�gina
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 */
public class Page<T> /*extends GenericResponse*/ implements Serializable {
	public static final int DEFAULT_PAGE_SIZE = 15;
	private static final long serialVersionUID = 1L;
	private List<T> elements;
	private int pageSize = DEFAULT_PAGE_SIZE;
	private int pageNumber;
	private int pageFirstElementNumber;
	private int totalElements = -1;
	private int numberOfPages;
	private boolean hasNextPage = false;

	/**
	 * Crea una nueva instancia utilizando el tama�o de p�gina predeterminado.
	 * 
	 * @param elements
	 *            los elementos de la p�gina
	 * @param firstRecord
	 *            el �ndice del primer elemento de la p�gina
	 * @param totalElements
	 *            el n�mero total de elementos
	 */
	public Page(List<T> elements, int firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * 
	 * @deprecated Utilizar el m�todo {@link #Page(List, int, int)}
	 * @param elements
	 *            los elementos de la p�gina
	 * @param firstRecord
	 *            el �ndice del primer elemento de la p�gina
	 * @param totalElements
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public Page(List<T> elements, long firstRecord, int totalElements) {
		this(elements, firstRecord, DEFAULT_PAGE_SIZE, totalElements);
	}

	/**
	 * Constructor depreciado, utilizar el constructor que recibe int en lugar de long:
	 * 
	 * @see #Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la p�gina
	 * @param first
	 *            el �ndice del primer elemento de la p�gina
	 * @param aPageSize
	 *            el n�mero de elementos por p�gina
	 * @param total
	 *            el n�mero total de elementos
	 */
	@Deprecated
	public Page(List<T> elements, long first, int aPageSize, long total) {
		this(elements, (int) first, aPageSize, (int) total);
	}

	/**
	 * Constructor principal.
	 * 
	 * @see #Page(List, int, int, int)
	 * 
	 * @param elements
	 *            los elementos de la p�gina
	 * @param first
	 *            el �ndice del primer elemento de la p�gina
	 * @param aPageSize
	 *            el n�mero de elementos por p�gina
	 * @param total
	 *            el n�mero total de elementos
	 */
	public Page(List<T> elements, int first, int aPageSize, int total) {
		this.pageFirstElementNumber = first;
		this.pageSize = aPageSize;
		this.totalElements = total;
		if (null == elements) {
			// this.elements = Collections.EMPTY_LIST;
			this.elements = Collections.<T> emptyList();
		} else {
			this.elements = elements;
		}

		if (pageFirstElementNumber < 0) {
			// En lugar de error lo fijamos en la primera p�gina
			pageFirstElementNumber = 0;
		}
		if (pageSize < 0) {
			// En lugar de dar error, lo fijamos al default
			pageSize = DEFAULT_PAGE_SIZE;
		}

		// Validamos que los valores de las variables de instancia hagan sentido.
		sanityCheck();

		if (pageFirstElementNumber == 0) {
			this.pageNumber = 0;
		} else {
			this.pageNumber = (int) (pageFirstElementNumber / pageSize);
		}
		if (totalElements < 0) {
			numberOfPages = -1;
		} else {
			numberOfPages = totalElements / pageSize;
			// La �ltima p�guina puede tener menos elementos que pageSize:
			numberOfPages += totalElements % pageSize == 0 ? 0 : 1;
		}
	}

	/**
	 * Determina si existen m�s p�ginas o se trata de la �ltima p�gina.
	 * 
	 * @return true si existen mas p�ginas, false si es la �ltima p�gina
	 */
	public boolean isHasNextPage() {
		if (getTotal() < 0) {
			return this.hasNextPage;
		} else {
			return !(getPageNumber() >= getNumberOfPages() - 1);
		}
	}

	public void setHasNextPage(boolean flag) {
		this.hasNextPage = true;
	}

	public boolean isHasPreviousPage() {
		return getPageNumber() > 0;
	}

	/**
	 * Devuelve el contenido de la p�gina.
	 * 
	 * @return los elementos de la p�gina.
	 */
	public List<T> getElements() {
		return elements;
	}

	/**
	 * Devuelve el n�mero total de elementos.
	 * 
	 * @return n�mero total de elementos
	 */
	public int getTotal() {
		return totalElements;
	}

	/**
	 * @see #getTotal()
	 * 
	 * @deprecated utilizar getTotal() en lugar de este m&eacute;todo.
	 * @return n�mero total de elementos
	 */
	@Deprecated
	public long getTotalNumberOfElements() {
		return totalElements;
	}

	public int getFirstElementIndex() {
		return pageFirstElementNumber;
	}

	/**
	 * @see #getFirstElementIndex()
	 * 
	 * @deprecated utilizar {@link #getFirstElementIndex()} en lugar de este m&eacute;todo.
	 * @return el �ndice del primer elemento de la p�gina
	 */
	@Deprecated
	public long getPageFirstElementNumber() {
		return pageFirstElementNumber;
	}

	/**
	 * Devuelve el �ndice del �ltimo elemento de la p�gina.
	 * 
	 * @return el �ndice del �ltimo elemento de la p�gina
	 */
	public int getLastElementIndex() {
		return getFirstElementIndex() + getElements().size() - 1;
	}

	/**
	 * @deprecated utilizar {@link #getLastElementIndex()} en lugar de este m&eacute;todo.
	 * @return el �ndice del �ltimo elemento de la p�gina
	 */
	@Deprecated
	public long getPageLastElementNumber() {
		return getPageFirstElementNumber() + getElements().size() - 1;
	}

	/**
	 * Devuelve el n�mero de elementos que contiene cada p�gina.
	 * 
	 * @return el tama�o de la p�gina
	 */
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Devuelve el n�mero de p�gina que representa esta p�gina. Depende del n�mero total de
	 * registros y del tama�o de la p�gina.
	 * 
	 * @return el n&uacute;mero de p�gina
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * Devuelve la cantidad de de p&aacute;ginas totales.
	 * 
	 * @return n�mero de p�ginas
	 */
	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 * Validamos que las variables de instancia hagan sentido.
	 */
	private void sanityCheck() {
		if (totalElements == 0 && null != elements && elements.size() != 0) {
			throw new RuntimeException(
					"The total number of elements cannot be 0 if the page contains elements");
		}
		if (elements == null & totalElements > 0) {
			throw new RuntimeException(
					"There cannot be an empty page if total number of elements is not 0");
		}
		if (elements != null && totalElements > 0 && totalElements < elements.size()) {
			throw new RuntimeException("Total number of elements cannot be less "
					+ "than the number of elements in the page.");
		}
		if (elements != null && pageSize < elements.size()) {
			throw new RuntimeException(
					"The page size cannot be less than the number of elements of the page!");
		}
		if (elements != null && elements.size() == 0 && totalElements > 0) {
			throw new RuntimeException(
					"There cannot be an empty page if total number of elements > 0");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Total elements=").append(getTotal() >= 0 ? getTotal() : "Unkown");
		sb.append("\nPage number=").append(getPageNumber());
		sb.append("\nPage size =").append(getPageSize());
		sb.append("\nActual page size=").append(getElements().size());
		sb.append("\nFirst element on page =").append(getFirstElementIndex());
		sb.append("\nLast element on page =").append(getLastElementIndex());
		sb.append("\nNumber of pages=").append(
				getNumberOfPages() >= 0 ? getNumberOfPages() : "Unkown");
		sb.append("\nHas next page=").append(this.isHasNextPage());
		return sb.toString();
	}

}
