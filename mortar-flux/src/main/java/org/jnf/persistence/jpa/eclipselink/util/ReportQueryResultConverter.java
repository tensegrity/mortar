package org.jnf.persistence.jpa.eclipselink.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.eclipse.persistence.internal.helper.ConversionManager;
import org.eclipse.persistence.internal.queries.ReportItem;
import org.eclipse.persistence.queries.ReportQuery;
import org.eclipse.persistence.queries.ReportQueryResult;

/**
 * Example TopLink Extension: Converts the result of a report query (List<ReportQueryResult>) into a
 * list of user defined non-persistence managed objects. This is done using the only constructor on
 * the class and mapping its required arguments to those selected in the ReportQuery.
 * 
 * This class does not handle all of the cases possible with multiple constructors or control over
 * mapping partial or re-ordered results onto a constructor.
 * 
 * @author Doug Clarke, TopLink PM
 */
public class ReportQueryResultConverter<T> {
	private ConversionManager conversionManager = ConversionManager.getDefaultManager();
	private Constructor<T> constructor;
	private ReportQuery reportQuery;
	private String[] constructorArgNames;
	private String[] beanProperties;
	//private String[] keys;
	private boolean useConstructor = true;
	private boolean isObjectArray = false;

	public ReportQueryResultConverter(ReportQuery reportQuery) {
		this(reportQuery, null, null);
	}

	public ReportQueryResultConverter(ReportQuery reportQuery, Class<T> resultClass) {
		this(reportQuery, resultClass, null);
	}
	
	public ReportQueryResultConverter(ReportQuery reportQuery, String[] argNames) {
		this(reportQuery, null, argNames);
	}	

	public ReportQueryResultConverter(ReportQuery reportQuery, Class<T> resultClass,
			String[] argNames) {
		this.reportQuery = reportQuery;
		this.constructorArgNames = argNames;

		if (null == resultClass) {
			this.isObjectArray = true;
			configBeanProperties(argNames);
		} else {
			configConstructor(resultClass);
			if (this.useConstructor) {
				configConstructorArgs(argNames);
			} else {
				configBeanProperties(argNames);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> convert(List<ReportQueryResult> results) {
		List<T> convertedResults = new ArrayList<T>();

		for (ReportQueryResult result : results) {
			if (this.isObjectArray) {
				convertedResults.add((T) convertToObjectArray(result));
			} else {
				if (this.useConstructor) {
					convertedResults.add((T) convert(result));
				} else {
					convertedResults.add((T) convertWithBeanProperties(result));
				}
			}
		}

		return convertedResults;
	}

	@SuppressWarnings("unchecked")
	private void configConstructor(Class javaClass) {
		Constructor[] constructors = javaClass.getDeclaredConstructors();

		List<Constructor> legalConsutructors = new ArrayList<Constructor>();

		for (int index = 0; index < constructors.length; index++) {
			Constructor con = constructors[index];

			if (Modifier.isPublic(con.getModifiers()) && con.getParameterTypes().length > 0) {
				legalConsutructors.add(con);
			} else {
				this.constructor = con;
			}
		}

		if (legalConsutructors.isEmpty()) {
			// throw new IllegalArgumentException("No accesible constructors found on: " +
			// javaClass);
			this.useConstructor = false;
		}
		if (legalConsutructors.size() > 1) {
			// throw new IllegalArgumentException("Multiple accesible constructors found on: " +
			// javaClass);
			this.useConstructor = false;
		}
		if (this.useConstructor) {
			this.constructor = legalConsutructors.get(0);
		}
	}

	private String[] getAttributesFromReportQuery(String[] argNames) {
		if (null != argNames && argNames.length > 0) {
			return argNames;
		} else {
			List<String> names = new ArrayList<String>();
			for (ReportItem item: getReportQuery().getItems()){
				names.add(item.getName());
			}
			return names.toArray(new String[]{});		
		}
	}

	private void configBeanProperties(String[] argNames) {
		this.beanProperties = getAttributesFromReportQuery(argNames);
	}

//	private void configKeys(String[] argNames) {
//		this.keys = getAttributesFromReportQuery(argNames);
//	}

	private void configConstructorArgs(String[] argNames) {
		if (argNames != null) {
			if (argNames.length != getConstructor().getParameterTypes().length) {
				throw new IllegalArgumentException(
						"Provided argNames length != constructor parameters");
			}
			// TODO: Check for other arg mismatches?

			this.constructorArgNames = argNames;
		} else {
			if (getReportQuery().getItems().size() != getConstructor().getParameterTypes().length) {
				throw new IllegalArgumentException(
						"Provided query arguments length != constructor parameters");
			}
			// TODO: Check RQ parameter types
			// TODO: Handle the constructor taking a subset of args

			this.constructorArgNames = getAttributesFromReportQuery(argNames);
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T convert(ReportQueryResult result) {
		Object[] values = new Object[getConstructorArgNames().length];

		for (int index = 0; index < getConstructorArgNames().length; index++) {
			String argName = getConstructorArgNames()[index];
			Class javaClass = getConstructor().getParameterTypes()[index];
			Object value = result.get(argName);

			values[index] = getConversionManager().convertObject(value, javaClass);
		}

		T view = null;

		try {
			view = (T) getConstructor().newInstance(values);
		} catch (Exception e) {
			throw new RuntimeException(
					"Error al tratar de instanciar una nueva clase para el converter de TopLink en el ReportQuery.",
					e);
		}
		return view;
	}

	@SuppressWarnings("unchecked")
	private <T> T convertWithBeanProperties(ReportQueryResult result) {
		T view = null;

		try {
			view = (T) getConstructor().newInstance();
			for (String property : this.beanProperties) {
				Object value = result.get(property);
				PropertyUtils.setSimpleProperty(view, property, value);
			}

		} catch (Exception e) {
			throw new RuntimeException(
					"Error al tratar de instanciar una nueva clase para el converter de TopLink en el ReportQuery.",
					e);
		}
		return view;
	}

	private Object[] convertToObjectArray(ReportQueryResult result) {
		List<Object> view = new ArrayList<Object>();

		try {
			for (String property : this.beanProperties) {
				Object value = result.get(property);
				view.add(value);
			}

		} catch (Exception e) {
			throw new RuntimeException(
					"Error al tratar de instanciar una nueva clase para el converter de TopLink en el ReportQuery.",
					e);
		}
		return view.toArray(new Object[] {});
	}

	public ConversionManager getConversionManager() {
		return conversionManager;
	}

	protected Constructor<T> getConstructor() {
		return constructor;
	}

	public ReportQuery getReportQuery() {
		return reportQuery;
	}

	public String[] getConstructorArgNames() {
		return constructorArgNames;
	}
}
