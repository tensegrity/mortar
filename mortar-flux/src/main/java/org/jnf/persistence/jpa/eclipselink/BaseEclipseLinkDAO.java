/* Copyright GUCOBA Systems */

package org.jnf.persistence.jpa.eclipselink;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.sessions.Session;
import org.springframework.dao.support.DaoSupport;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseEclipseLinkDAO extends DaoSupport {
	/** Log disponible para subclases */
	protected final static Log LOG = LogFactory.getLog(BaseEclipseLinkDAO.class);

	private JpaTemplate jpaTemplate;
	private EntityManager entityManager;	

	public BaseEclipseLinkDAO() {
		super();
	}

	/**
	 * Set the JpaTemplate for this DAO explicitly,
	 * as an alternative to specifying a EntityManagerFactory.
	 * @see #setEntityManagerFactory
	 */
	public final void setJpaTemplate(JpaTemplate jpaTemplate) {
		this.jpaTemplate = jpaTemplate;
	}

	/**
	 * Return the JpaTemplate for this DAO, pre-initialized
	 * with the EntityManagerFactory or set explicitly.
	 */
	public final JpaTemplate getJpaTemplate() {
	  return jpaTemplate;
	}
	
	protected JpaTemplate createJpaTemplate(EntityManager entityManager) {
		return new JpaTemplate(entityManager);
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
		this.jpaTemplate = this.createJpaTemplate(this.entityManager);
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}
	
	public Session getSession() {
		return ((JpaEntityManager) this.entityManager.getDelegate()).getActiveSession();
	}

	protected final void checkDaoConfig() {
		if (this.entityManager == null) {
			throw new IllegalArgumentException("entityManager or jpaTemplate is required");
		}		
		if (this.jpaTemplate == null) {
			throw new IllegalArgumentException("entityManager or jpaTemplate is required");
		}
	}
	
}
