package org.jnf.persistence.jpa.eclipselink;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.LockModeType;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.queries.ReportQuery;
import org.eclipse.persistence.queries.ReportQueryResult;
import org.jnf.persistence.EntityDAO;
import org.jnf.persistence.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class EntityEclipseLinkDAO<T, ID extends Serializable> extends BaseEclipseLinkDAO implements
		EntityDAO<T, ID> {

	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public EntityEclipseLinkDAO() {
		super();
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	protected ClassDescriptor getClassDescriptor() {
		return getSession().getClassDescriptor(getPersistentClass());
	}	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T entity) {
		this.getEntityManager().remove(entity);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll() {
		ReadAllQuery qr = new ReadAllQuery(getPersistentClass());
		return (List<T>)this.getSession().executeQuery(qr);
	}

	public Page<T> findAll(int firstRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	public Page<T> findAll(long firstRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)	
	public Page<T> findAll(int firstRecord, int pageSize) {
		int totalElements = -1;
		ExpressionBuilder emp = new ExpressionBuilder();
		ReportQuery queryCount = new ReportQuery(getPersistentClass(), emp);
		queryCount.addCount();
		List<ReportQueryResult> totalElementsResult = (List) this.getSession().executeQuery(queryCount);
		if (null != totalElementsResult && totalElementsResult.size() > 0) {
			totalElements = ((Long)totalElementsResult.get(0).getResults().get(0)).intValue();
		}
		ReadAllQuery query = new ReadAllQuery(getPersistentClass());
		query.setFirstResult(firstRecord);
		query.setMaxRows(firstRecord + pageSize);
		query.prepareForExecution();
		List<T> result = (List) this.getSession().executeQuery(query);		
		Page<T> page = new Page(result, firstRecord, pageSize, totalElements);
		return page;
	}

	public Page<T> findAll(long firstRecord, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)	
	public List<T> findByExample(T exampleInstance, String... excludeProperty) {
		ReadAllQuery query = new ReadAllQuery(getPersistentClass());
		query.setExampleObject(exampleInstance);
		// if (excludeProperty.length > 0) {
		// QueryByExamplePolicy policy = new QueryByExamplePolicy();
		// query.setQueryByExamplePolicy(policy);
		// }
		return (List<T>)this.getSession().executeQuery(query);
	}

	public Page<T> findByExample(int firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		// TODO Auto-generated method stub
		return null;
	}

	public Page<T> findByExample(long firstRecord, int pageSize, T exampleInstance,
			String... excludeProperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(readOnly = true)
	public T findById(ID id) {
		return this.getEntityManager().find(getPersistentClass(), id);
	}

	@Transactional(readOnly = true)
	public T findById(ID id, boolean lock) {
		T entity = this.findById(id);
		if (lock) {
			this.getEntityManager().lock(entity, LockModeType.PESSIMISTIC_READ);
		}
		return entity;
	}

	@Transactional(readOnly = true)
	public T getById(ID id) {
		return this.findById(id);
	}

	public Class<T> getPersistentClass() {
		return this.persistentClass;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public T saveOrUpdate(T entity) {
		//TODO: Para el caso del update se debe de obtener el id y si no esta en null realizar el update, de lo contrario es un save
		this.getEntityManager().persist(entity);
		return entity;
	}

	public void clear() {
		// TODO Auto-generated method stub

	}

	public void flush() {
		// TODO Auto-generated method stub

	}

}
