package org.jnf.system;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContextException;

/**
 * 
 * @author Pablo Krause
 * 
 */
public final class SystemUtils {
	private final static Log LOG = LogFactory.getLog(SystemUtils.class);

	private SystemUtils() {
		// No se debe de instanciar esta clase.
	}

	/**
	 * Devuelve el valor de una propiedad de sistema o, en caso de no existir como propiedad el
	 * valor de la variable de entorno del mismo nombre o null si no existe ninguna de las dos.
	 * 
	 * @param name
	 *            nombre del par�metro o variable de entorno
	 * @return valor del par�metro o variable de entorno, o null si no existe ninguno
	 */
	public static String getProperty(String name) {
		String paramValue = System.getProperty(name);
		String envValue = System.getenv(name);
		String value = null;

		if (null == paramValue) {
			if (null == envValue) {
				throw new ApplicationContextException("No existe la variable " + name
						+ " ni como par�metro de sistema ni como variable de entorno.");
			} else {
				value = envValue;
				System.setProperty(name, value);
				LOG.info("No se enctontr� el par�metro " + name
						+ ", usando variable de entorno con valor '" + envValue + "'");
			}
		} else if (null == envValue) {
			value = paramValue;
			LOG.info("Se encontr� " + name + " como parametro de sistema con valor '" + paramValue
					+ "'");
		} else {
			value = paramValue;
			// Si tanto el par�metro como la varialbe de entorno est�n definidas
			// generamos una advertencia.
			LOG.warn(name + " se encuentra definido tanto como par�metro de sistema"
					+ " como variable de entorno; " + "Utilizando par�metro con valor '"
					+ paramValue + "'");
		}
		return value;
	}
}
