/* Copyright GUCOBA Systems */

package org.jnf.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author Juvenal Guzm�n
 *
 */
public class AbstractBusinessService implements BusinessService {
	/** Log disponible para subclases */
	protected final Log log = LogFactory.getLog(getClass());
	/* Este loger no es static para que cada subclase tenga su propio logger */

}
