/* Copyright GUCOBA Systems */

package org.jnf.business;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class BusinessException extends Exception {
	private static final long serialVersionUID = 1L;
	private String codigo;

	public BusinessException(String elCodigo) {
		super();
		this.codigo = elCodigo;
	}

	public String getCodigo() {
		return codigo;
	}

}
