/* Copyright GUCOBA Systems */

package org.jnf.legacy.webservices;

import java.net.URL;
import java.util.Properties;

/**
 * Clase base para implementar el consumo de web services legados (RPC/encoded)
 * de manera programática, i.e. mediante el API de Axis.
 * El método preferido para consumir WS legados es mediante los stubs generados
 * por Axis en lugar de hacerlo proramaticamente.  Para este caso utilizar
 * la clase {@link AbstractWebServiceDelegate}
 * 
 * @author Pablo Krause
 *
 */
public abstract class BaseWebServiceDelegate {

	private Properties userProperties;
	private URL endpoint;
	private String username;

	public BaseWebServiceDelegate() {
		super();
	}

	public URL getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(URL endpoint) {
		this.endpoint = endpoint;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return userProperties.getProperty(getUsername());
	}

	public void setUserProperties(Properties userProperties) {
		this.userProperties = userProperties;
	}

}
