package org.jnf.security.web;

import org.acegisecurity.Authentication;
import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class SecurityRenderService implements InitializingBean {
	private static final Log LOG = LogFactory.getLog(SecurityRenderService.class);

	private SecureObjectsInspector objectInspector;

	public boolean renderWidget(String form, String widgetId) {
		LOG.debug("Check for security widget render rules [form=" + form + ", widgetId=" + widgetId
				+ "]");
		boolean doRender = true;
		if (widgetId != null && !StringUtils.isEmpty(widgetId)) {
			if ((SecurityContextHolder.getContext() != null)
					|| (SecurityContextHolder.getContext() instanceof SecurityContext)
					|| (((SecurityContext) SecurityContextHolder.getContext()).getAuthentication() != null)) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				GrantedAuthority[] auths = auth.getAuthorities();
				LOG.debug("Checking authorities for user [" + auth.getName() + "]...");
				LOG.debug("Authorities found for user " + auth.getName() + ", [" + auths + "]");
				String objectId = widgetId;
				if (form != null && !StringUtils.isEmpty(form)) {
					objectId = form + "." + widgetId;
				}
				doRender = objectInspector.isObjectSecure(objectId, auths);
			} else {
				LOG.error("No security context holder was found, or authentation holder is null.");
				return false;
			}
		}

		return doRender;
	}

	public void setObjectInspector(SecureObjectsInspector objectInspector) {
		this.objectInspector = objectInspector;
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.objectInspector,
				"An secure object inspector needs to define for SecurityRenderService.");

	}

}
