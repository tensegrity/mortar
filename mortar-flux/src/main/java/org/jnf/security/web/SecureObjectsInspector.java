package org.jnf.security.web;

import org.acegisecurity.GrantedAuthority;

public interface SecureObjectsInspector {
	
	boolean isObjectSecure(String objectId, GrantedAuthority[] auths);

}
