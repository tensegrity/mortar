/* Copyright GUCOBA Systems */

package org.jnf.security;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Esta clase se utiliza para pasar informaci�n del contexto de la invocaci�n al motor de reglas
 * responsable de determinar la acci�n a tomar en base, entre otras cosas, a la clase y m�todo que
 * se est� pretendiendo invocar as� como de los argumentos utilizados en dicha invocaci�n (e.g. para
 * poder tomar acciones en base a datos).
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class InvocationInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String className;
	private String methodName;
	private Object[] args;

	public void setArguments(Object... arguments) {
		this.args = arguments;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
