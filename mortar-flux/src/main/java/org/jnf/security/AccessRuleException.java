/* Copyright GUCOBA Systems */

package org.jnf.security;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class AccessRuleException extends Exception {
	private static final long serialVersionUID = 1L;
	public final static String PREFIX = "iid";
	private String invocationId;
	private RuleAction ruleAction;
	private String message;

	// Constructors:

	/**
	 * Constructor
	 * 
	 * @param aRuleAction
	 *            la accion por la cual se genera la excepci�n
	 */
	public AccessRuleException(RuleAction aRuleAction) {
		this.ruleAction = aRuleAction;
		this.invocationId = generateInvocationId();
		generateMessage();
	}

	/**
	 * Este constructor sirve para instanciar la excepci�n para una invocaci�n preexistente para la
	 * cual ya se tiene un identificador.
	 * 
	 * @param anInvocationId
	 *            el identificador de la invocaci�n
	 * @param aRuleAction
	 *            la acci�n que genera la excepci�n
	 */
	public AccessRuleException(String anInvocationId, RuleAction aRuleAction) {
		this.ruleAction = aRuleAction;
		this.invocationId = anInvocationId;
		generateMessage();
	}

	/**
	 * 
	 */
	private void generateMessage() {
		StringBuffer msg = new StringBuffer();
		switch (ruleAction) {
		case NO_ACTION:
			// Nunca deber�a caer aqu�, pues no es una excepci�n...
			msg.append("Operaci�n '").append(invocationId).append("' autorizada.");
			break;

		case REQUIRES_AUTHORIZATION:
			msg.append("La operaci�n '").append(invocationId).append("' requiere autorizaci�n.");
			break;

		case WARNING:
			msg.append("La operaci�n '").append(invocationId).append("' requiere confirmaci�n.");
			break;

		case EXCEPTION:
			msg.append("No tiene permisos para ejecutar la operaci�n '").append(invocationId)
					.append("'.");
			break;

		default:
			throw new UnsupportedOperationException();
		}
		message = msg.toString();
	}

	// Methods:

	@Override
	public String getMessage() {
		// Sobrecargamos este m�todo ya que para fijar el mensaje s�lo es posible hacerlo
		// en la primera l�nea del constructor, sin embargo el mensaje requiere que generemos
		// el invocationId antes.
		return message;
	}

	/**
	 * Genera un identificador �nico
	 * 
	 * @return un String con un identificador �nico
	 */
	private String generateInvocationId() {
		StringBuffer id = new StringBuffer();
		id.append(PREFIX).append(ruleAction.ordinal()).append("-").append(
				System.currentTimeMillis()).append("-").append(this.hashCode());
		return id.toString();
	}

	// Accessor methods:

	public RuleAction getRuleAction() {
		return ruleAction;
	}

	public String getInvocationId() {
		return invocationId;
	}

}
