/* Copyright GUCOBA Systems */

package org.jnf.security;

import javax.security.auth.login.LoginException;

/**
 * 
 * Servicio para la autorizaci�n, confirmaci�n o cancelaci�n de invocaciones.
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public interface AuthorizationService {

	/**
	 * Solicita la autorizaci�n de la ejecuci�n de una invocaci�n.
	 * 
	 * @param invocationId
	 *            el identificador de la invocaci�n
	 * @param username
	 *            clave del usuario que autoriza
	 * @param password
	 *            password del usuario que autoriza
	 * @return regresa lo que sea que la operaci�n que se pretende ejecutar regrese
	 * 
	 * @throws AccessRuleException
	 *             si aplica alguna regla de acceso
	 * @throws LoginException
	 *             si falla la autenticaci�n del usuario que pretende autorizar
	 */
	Object authorize(String invocationId, String username, String password)
			throws AccessRuleException, LoginException;

	/**
	 * Este m�todo cancela la invocaci�n en curso lo que significa que se elimina la invocaci�n de
	 * la sesi�n.
	 * 
	 * @param invocationId
	 *            el identificador de la invocaci�n a cancelar
	 */
	void cancelInvocation(String invocationId);

	/**
	 * Este m�todo se invoca cuando el usuario confirma que si quiere realizar la operaci�n cuyo
	 * identificador se env�a.
	 * 
	 * @param invocationId
	 *            invocationId el identificador de la invocaci�n a confirmar
	 * @return el resultado de la invocaci�n que qued� pendiente de confirmar
	 */
	Object confirmInvocation(String invocationId);

	/**
	 * Este m�todo sirve para solicitar autorizaci�n para ejecutar una operaci�n mediante la
	 * evaluaci�n de las reglas de acceso para dicha operaci�n. El m�todo preferido NO es usar este
	 * m�todo sino que a trav�s de un aspecto que intercepte la invocaci�n del servicio (operaci�n)
	 * que se desea proteger mediante reglas. Este m�todo es para aquellos casos en los que el
	 * front-end requiere llevar a cabo expl�citamente la evaluaci�n para, por ejemplo, mostrar o no
	 * ciertas partes de la pantalla.
	 * 
	 * @deprecated Se prefiere el uso de control de acceso medianta aspectos, es decir mediante un
	 *             interceptor que atrape la invocaci�n y mande llamar las reglas de acceso. Este
	 *             m�todo se incluye para soportar funcionalidad que ya estaba construida.
	 * 
	 * @param objecto
	 *            el nombre del objeto (e.g. pantalla)
	 * @param operacion
	 *            el nombre de la operaci�n que se pretende ejecutar
	 * @param args
	 *            posibles argumentos que requiera la operaci�n
	 * @throws AccessRuleException
	 *             si aplica alguna regla de acceso
	 */
	@Deprecated
	void evaluateAccessRules(String objecto, String operacion, Object... args)
			throws AccessRuleException;

	/**
	 * Invoca la ejecuci�n de las reglas de acceso. Este metodo es consumido por el
	 * AccessRuleInterceptor y no est� pensado para ser invocado directamente.
	 * 
	 * @param invocationInfo
	 *            la informaci�n de invocaci�n para la evaluaci�n de la regla de acceso
	 * @param userInfo
	 *            informaci�n del usuario con el cual se debe evaluar la regla de acceso
	 * @return un enum de tipo RuleAction con el resultado de la evaluaci�n de las reglas.
	 */
	RuleAction invokeAccessRule(InvocationInfo invocationInfo, UserInfo userInfo);

}
