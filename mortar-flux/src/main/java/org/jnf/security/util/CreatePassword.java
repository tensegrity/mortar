/* Copyright GUCOBA Systems */

package org.jnf.security.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import static java.lang.System.out;

/**
 * Clase para cifrar Strings, en particular pensado para cifrar los passwords de los clientes de los
 * web services.
 * 
 * @author Pablo Krause
 * 
 */
public final class CreatePassword {
	// Llave de cifrado por defecto.
	private final static char[] DEFAULT_KEY 
		= { 51, 112, 51, 115, 89, 100, 112, 98, 66, 114, 51, 116, 77 };
	private static final String DEFAULT_ALGORITHM = "PBEWithMD5AndDES";

	/**
	 * Ocultamos constructor.
	 */
	private CreatePassword() {
	}

	/**
	 * Cifra un String con una llave de encripci�n dada.
	 * 
	 * @param args
	 *            el primer argumento debe de ser el String a cifrar, el segundo argumento debe de
	 *            ser la llave utilizada para cifrar (la cual debe ser la misma al descifrar). Si no
	 *            se especifica esta �ltima se utiliza una por defecto. El tercer argumento
	 *            especifica el algoritmo a utilizar, si no se especifica se utiliza
	 *            PBEWithMD5AndDES
	 */
	public static void main(String[] args) {
		String key = new String(DEFAULT_KEY);
		String algorithm = DEFAULT_ALGORITHM;
		if (null == args || args.length <= 0) {
			out.println(
					"uso: pwd <password> [<llave de encripcion>] [<algoritmo de encripcion>]");
			return;
		}
		String clearpwd = args[0];
		if (args.length > 1) {
			key = args[1];
		}
		if (args.length > 2) {
			algorithm = args[2];
		}
		String encpwd;
		StandardPBEStringEncryptor e = new StandardPBEStringEncryptor();
		e.setAlgorithm(algorithm);
		e.setPassword(String.valueOf(key));
		encpwd = e.encrypt(clearpwd);
		out.println(encpwd);
	}

}
