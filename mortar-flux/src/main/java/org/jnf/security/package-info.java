/**
 * Clases e interfases para el manejo de la seguridad, incluida la integración
 * con el motor de reglas Open Rules.
 */
package org.jnf.security;
