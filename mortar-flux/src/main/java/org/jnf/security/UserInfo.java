package org.jnf.security;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author Pablo Krause powered by GUCOBA Systems S.C.
 * 
 */
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private String userName;
	private String userRoles;
	private String ipAddress;
	private String company;
	private String application;
	private String codigoOficina;

	public UserInfo() {

	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String companyName) {
		this.company = companyName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String userRoles) {
		this.userRoles = userRoles;
	}

	public String getCodigoOficina() {
		return codigoOficina;
	}

	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
